package com.windstream.cu.TestContactUs;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.windstream.ctctus.config.Constants;
import com.windstream.ctctus.dao.XrefDAOImpl;

public class XrefDAOTest {

	private Logger logger = Logger.getLogger(XrefDAOTest.class);
	private static XrefDAOImpl xrefDao = null;

	@Test
	public void testConstructor() {
		if(xrefDao == null) {
			try {
				logger.debug(new File(".").getCanonicalPath());
//				File file = new File(Constants.TEST_PROP_FILE_NAME);
				//check if the properties file was loaded.
//				Assert.assertTrue(file.exists());
				//instantiate object now that properties are loaded.
				xrefDao = new XrefDAOImpl(Constants.TEST_PROPERTIES_BUNDLE);
//				xrefDao = new XrefDAOImpl(Constants.TEST_PROP_FILE_NAME);
			} catch (FileNotFoundException e) {
				fail("File not found Exception thrown:" + e.getMessage());
			} catch (IOException e) {
				fail("IOException thrown:" + e.getMessage());
			}
		} else {
			fail("xrefDao is not null. It should be null.");
		}
		Assert.assertNotNull(xrefDao);
	}
	
	@Test
	public void testGetEAN() {
		try {
			xrefDao = new XrefDAOImpl();
		} catch (FileNotFoundException e) {
			fail("File not found Exception thrown:" + e.getMessage());
		} catch (IOException e) {
			fail("IOException thrown:" + e.getMessage());
		}
		int ean = xrefDao.getEAN(Constants.TEST_ACCT_NUM,Constants.TEST_DSS_ID);
		Assert.assertFalse(0==ean);
		Assert.assertTrue(ean==2431493);
	}

}
