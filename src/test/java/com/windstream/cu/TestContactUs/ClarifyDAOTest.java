package com.windstream.cu.TestContactUs;

import com.windstream.bus.ws.client.ClarifyClient.HgbstElement;
import com.windstream.bus.ws.client.ClarifyClient.SiteContacts;
import com.windstream.bus.ws.client.ClarifyClient.StandardResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.windstream.ctctus.config.Constants;
import com.windstream.ctctus.dao.ClarifyDAOImpl;
import com.windstream.ctctus.exceptions.ClarifyWSException;
import com.windstream.ctctus.properties.Properties;

public class ClarifyDAOTest {

	private static Logger logger = Logger.getLogger(ClarifyDAOTest.class);
	private static ClarifyDAOImpl clarifyDAO;
	
	@Test
	public final void testClarifyDAOImpl() {
		if(clarifyDAO == null) {
			try {
				//display the file path
				logger.debug(new File(".").getCanonicalPath());
				//this will cause the DAO to use the test.properties file instead
				clarifyDAO = new ClarifyDAOImpl(Constants.TEST_PROPERTIES_BUNDLE);
//				clarifyDAO = new ClarifyDAOImpl(Constants.TEST_PROP_FILE_NAME);
				//properties should be loaded and not null at this point
				Assert.assertTrue(Properties.getMyResources()!=null);
//				logger.debug(Properties.getBundleName()+"="+Constants.TEST_PROP_FILE_NAME);
			} catch (FileNotFoundException e) {
				Assert.fail("File not found Exception thrown:" + e.getMessage());
			} catch (IOException e) {
				Assert.fail("IOException thrown:" + e.getMessage());
			}
		} else {
			Assert.fail("clarifyDAO is supposed to be null at this point.");
		}
	}

	@Test
	public final void testGetSiteContacts() {
		List<SiteContacts> contacts = null;
		try {
			contacts = clarifyDAO.getSiteContacts(Constants.TEST_ACCT_NUM);
		} catch (ClarifyWSException e) {
			e.printStackTrace();
			Assert.fail(e.getTheError());
		}
		Assert.assertNotNull(contacts);
		Assert.assertTrue("Fail!", Integer.toString(Constants.TEST_ACCT_NUM).equalsIgnoreCase(contacts.get(0).getSiteID()));
		Assert.assertEquals("Fail!", "Aurora Nurses Professional Registry", contacts.get(0).getSiteName());
	}

	@Test
	public final void testGetLevel2Dropdown() {
		List<HgbstElement> level2Dropdown = null;
		try {
			level2Dropdown = clarifyDAO.getLevel2Dropdown(Constants.TEST_LEVEL1);
		} catch (ClarifyWSException e) {
			e.printStackTrace();
			Assert.fail(e.getTheError());
		}
		Assert.assertEquals("Fail!", 0, level2Dropdown.get(0).getRank());
		Assert.assertTrue("Fail!", "Disputes".equalsIgnoreCase(level2Dropdown.get(0).getTitle()));
	}

	@Test
	public final void testGetLevel3Dropdown() {
		List<HgbstElement> level3Dropdown = null;
		try {
			level3Dropdown = clarifyDAO.getLevel3Dropdown(Constants.TEST_LEVEL1,Constants.TEST_LEVEL2);
		} catch (ClarifyWSException e) {
			e.printStackTrace();
			Assert.fail(e.getTheError());
		}
		HgbstElement firstElement = level3Dropdown.get(0);
		Assert.assertEquals("Success!", 0, firstElement.getRank());
		Assert.assertEquals("Success!", "1st Invoice", firstElement.getTitle());
	}

	@Test
	public final void testCreateCase() {
		StandardResponse response = null;
		try {
			response = clarifyDAO.createCase(
					Integer.toString(Constants.TEST_ACCT_NUM),	//site ID
					Constants.TEST_CONTACT_FNAME,				//contact first name
					Constants.TEST_CONTACT_LNAME,				//contact last name
					Constants.TEST_CONTACT_PHONE,				//contact phone
					Constants.TEST_LEVEL1,						//trouble type
					Constants.TEST_LEVEL2,						//trouble sub type
					Constants.TEST_LEVEL3,						//trouble subtype category
					Constants.TEST_PRIORITY,					//priority
					Constants.TEST_PHONE_LOG);					//phoneLog
		} catch (ClarifyWSException e) {
			e.printStackTrace();
			Assert.fail(e.getTheError());
		}					
		Assert.assertNotNull("Null response when trying to create a case.", response);
		
		String specialExtraLongErrorMessage = "Fail! The response code is not 'Success'. It is "+
			response.getReturnCode()+":"+response.getReturnMessage(); 
		Assert.assertTrue(specialExtraLongErrorMessage, response.getReturnCode().equalsIgnoreCase("Success"));
		
		//"0" indicates success
		Assert.assertTrue("Fail! The response message is not 0.", response.getReturnMessage().equalsIgnoreCase("0"));
		Assert.assertNotNull("Fail! The reference value is null.", response.getReturnReferenceValue());
	}

}
