import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import org.apache.log4j.Logger;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class support_email extends HttpServlet {
	
	private static final long serialVersionUID = 70569063129946193L;
	private static final Logger logger = Logger.getLogger(support_email.class);
	/* 01.19.2011-bcarson: I chose NOT to make this 'static'. This means properties file can be
	 * modified on the server and the app does not have to be bounced for the change to be picked
	 * up. 
	 */
	private final Properties propFile = new Properties();
	
	/**
	 * Constructor of the object.
	 */
	public support_email() {
		super();
		try {
			propFile.load(new FileInputStream("app.properties"));
		} catch (FileNotFoundException e) {
			logger.error("ERROR: FileNotFoundException has occurred.");
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("ERROR: IOException has occurred.");
			e.printStackTrace();
		}
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.debug("*** in support_email.doGet***");

		response.setContentType("text/html");
		/*PrintWriter out = response.getWriter();
		out
				.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the GET method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();*/
		support_email method = new support_email();
		method.doPost(request,response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.debug("*** in support_email.doPost***");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		/*
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
		*/
		try {
			//Create some properties and get a default Session.
			Properties props = new Properties();
			props.put("mail.smtp.host","smtp.nuvox.net");
						
			Session mailSession = Session.getInstance(props, null);
			//get parameters from the request
			String nv_accountnum = request.getParameter("nv_accountnum");
			String aicEscDisplayName = request.getParameter("aicEscDisplayName");
			String phone = request.getParameter("phone");
			String aicEscContact = request.getParameter("aicEscContact");
			String nv_ticketnum = request.getParameter("nv_ticketnum");
			String nv_tickettype= request.getParameter("nv_tickettype");
			String nv_psrnum = request.getParameter("nv_psrnum");
			String nv_icareordernum = request.getParameter("nv_icareordernum");
			String aicEscQuestion = request.getParameter("aicEscQuestion");
			
			String Msg = "Support Contact Results" + "\n\n\n\n [nv_accountnum=" + nv_accountnum + "]\n [nv_ticketnum=" + nv_ticketnum + "] \n [nv_tickettype=" + nv_tickettype +
				"] \n [nv_psrnum=" + nv_psrnum + "] \n [nv_icareordernum=" + nv_icareordernum + "] \n\n\n\n Contact Name: " + aicEscDisplayName + "\n Contact Email: " + aicEscContact + "\n Contact Phone No.: " + phone + "\n Need assistance with: " + aicEscQuestion;
			
			//Instantiate a new MineMessage and fill it with the required information.
			//create a message
			MimeMessage message = new MimeMessage(mailSession);
			//set the from address
			message.setFrom(new InternetAddress(aicEscContact));
			//set to address
			//01.19.2011-bcarson: choose inbox based on user input
			String nv_isacustomer = request.getParameter("nv_isacustomer");
			InternetAddress to;
			if(nv_isacustomer.equalsIgnoreCase("true")) {
				to = new InternetAddress(propFile.getProperty("customer_inbox"));
			} else {
				to = new InternetAddress(propFile.getProperty("acct_man_inbox"));
			}
			message.addRecipient(Message.RecipientType.TO, to);
			//set rest of the message
			message.setSubject("Contact Inquiry");
			message.setText(Msg);
			
			//send message
			Transport.send(message);
			logger.debug("*** after email sent in support_email***");
			//response.setContentType("text/html");
			//response.sendRedirect(/support_form.jsp);
			out.println("Thank You. Your email has been sent.");
		} catch(Exception ex) {
			System.out.println("ERROR...."+ex);
		}
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
