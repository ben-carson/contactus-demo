
import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


public class support_link extends HttpServlet {
	
	private static final long serialVersionUID = -5064435798043157278L;
	private static final Logger logger = Logger.getLogger(support_link.class);

	/**
	 * Constructor of the object.
	 */
	public support_link() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.debug("*** in support_link.doGet***");

		//response.setContentType("text/html");
							
		StringBuilder sb=new 
		StringBuilder("https://customerchat.nuvox.com/website/public/account.jsp?");
		
		sb.append("aicAuthAction=login");
		sb.append("&aicTenant=DefaultTenant");
		sb.append("&aicLanguage=en");
		sb.append("&aicAuthLevel=guest");
		sb.append("&aicEscAction=escalate");
		sb.append("&aicEscRequestedMedia=chat");
		sb.append("&aicEscDisplayName=");
		sb.append(request.getParameter(URLEncoder.encode("aicEscDisplayName","UTF-8")));
		sb.append("&aicEscTranscriptRequest=off");
		sb.append("&aicEscContact=");
		sb.append(request.getParameter(URLEncoder.encode("aicEscContact","UTF-8")));		
		sb.append("&aicEscQuestion=");
		sb.append(request.getParameter(URLEncoder.encode("aicEscQuestion","UTF-8")));
		sb.append("&aicEscStartURL=");
		sb.append(URLEncoder.encode("http://www.nuvox.com","UTF-8"));
		sb.append("&edu.username=");
		sb.append(request.getParameter(URLEncoder.encode("aicEscDisplayName","UTF-8")));
		sb.append("&edu.nv_accountnum=");
		sb.append(request.getParameter(URLEncoder.encode("nv_accountnum","UTF-8")));
		sb.append("&nv_ticketype=");
		sb.append(request.getParameter(URLEncoder.encode("nv_ticketype","UTF-8")));
		sb.append("&phone=");
		sb.append(request.getParameter(URLEncoder.encode("phone","UTF-8")));
		
		
		
		//URLEncoder.encode(sb.toString(),"UTF-8");
		
		try {
			//String encodedurl = URLEncoder.encode(sb.toString(),"UTF-8");
			response.sendRedirect(sb.toString());
			//response.sendRedirect(encodedurl);
		} catch (Exception e){
			System.err.println("Error Sending Redirect: "+e.getMessage());
			e.printStackTrace();
		} finally {
			System.out.println(sb);
		}
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out
				.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
		*/
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
