package com.windstream.ctctus.properties;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.windstream.ctctus.config.Constants;

public class Properties {
	
	private static Logger logger = Logger.getLogger(Properties.class);
	private static String bundleName = Constants.APP_PROPERTIES_BUNDLE;
	private static ResourceBundle myResources;
	
	public static String getProperty(String key) {
		logger.debug("Getting property for key " + key);
		if(myResources == null) {
			logger.debug("myResources is null. Setting its bundle to " + getBundleName());
			setMyResources(getBundleName());
		}
		String value = "";
		value = myResources.getString(key);
		logger.debug("Property returned is "+key+"="+value);
		if (value == null)
			return "";
		else
			return value;
	}

	public static String getBundleName() {
		return bundleName;
	}
	public static void setBundleName(String bundleName) {
		Properties.bundleName = bundleName;
	}

	public static ResourceBundle getMyResources() {
		if(myResources == null) {
			myResources = ResourceBundle.getBundle(bundleName);
		}
		return myResources;
	}
	public static void setMyResources(ResourceBundle myResources) {
		Properties.myResources = myResources;
	}
	//use this to specify the resource bundle. Used for testing.
	public static void setMyResources(String bundleName) {
		setBundleName(bundleName);
		myResources = ResourceBundle.getBundle(getBundleName());
	}
}
