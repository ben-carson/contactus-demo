package com.windstream.ctctus.dao;

import com.windstream.bus.ws.client.ClarifyClient.ClarifyServiceManager;
import com.windstream.bus.ws.client.ClarifyClient.ClarifyServiceManagerSoap;
import com.windstream.bus.ws.client.ClarifyClient.HgbstElement;
import com.windstream.bus.ws.client.ClarifyClient.HgbstResp;
import com.windstream.bus.ws.client.ClarifyClient.SiteContacts;
import com.windstream.bus.ws.client.ClarifyClient.SiteContactsResp;
import com.windstream.bus.ws.client.ClarifyClient.SiteSearchField;
import com.windstream.bus.ws.client.ClarifyClient.StandardResponse;
import com.windstream.bus.ws.client.ClarifyClient.TreatmentElement;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.windstream.ctctus.config.Constants;
import com.windstream.ctctus.exceptions.ClarifyWSException;
import com.windstream.ctctus.properties.Properties;

public class ClarifyDAOImpl implements ClarifyDAO {
	
	private static Logger logger = Logger.getLogger(ClarifyDAOImpl.class);
	private static String clarifyWSDL;
	private static ClarifyServiceManager clarManager = null;
	private static ClarifyServiceManagerSoap clarifyClient = null;
	
	public ClarifyDAOImpl() throws FileNotFoundException, IOException {
		try {
			if(clarManager == null) {
logger.debug("DAO: CLARMANAGER IS NULL.");
				clarifyWSDL = Properties.getProperty(Constants.CLARIFY_WSDL_PROPERTY_NAME);
				URL wsdlURL = new URL(clarifyWSDL);
logger.debug("DAO: ABOUT TO CREATE CLARIFYSERVICEMANAGER.");
				clarManager = new ClarifyServiceManager(wsdlURL);
logger.debug("DAO: CLARIFY SERVICE MANAGER CREATED.");
			}
			if(clarifyClient == null) {
logger.debug("DAO: ABOUT TO CREATE CLARIFY CLIENT");
				clarifyClient = clarManager.getClarifyServiceManagerSoap();
logger.debug("DAO: CLARIFY CLIENT CREATED.");
			}
		} catch (IOException ioe) {
			System.err.println("ERROR: IOExeception encountered in default constructor.");
			ioe.printStackTrace();
			throw ioe;
		}
	}
	
	//if the user needs to change the properties file, use this constructor
	public ClarifyDAOImpl(String props) throws FileNotFoundException, IOException {
		try {
			//change the properties file
			logger.debug("Changing Clarify ResourceBundle to " + props);
			Properties.setMyResources(props);
			//then construct the object
			new ClarifyDAOImpl();
		} catch (FileNotFoundException e) {
			logger.error("ERROR: Could not locate properties file.");
			throw e;
		} catch (IOException ioe) {
			logger.error("ERROR: IOExeception encountered in constructor with argument.");
			throw ioe;
		} finally {
			if(Properties.getMyResources()!=null) {
				logger.debug("Properties file being used is " + Properties.getBundleName());
			} else {
				logger.error("Failure to load " + props + " file.");
			}
		}
	}
	
	public List<SiteContacts> getSiteContacts(int ean)
			throws ClarifyWSException {
logger.debug("inside getSiteContacts.");
		SiteContactsResp response = clarifyClient.getSiteContacts(SiteSearchField.SITE_ID, Integer.toString(ean));
logger.debug("web service successfully called.");
		List<SiteContacts> contacts = null;

		if(response == null) {
			throw new ClarifyWSException("The response object returned from the web service is null!");
		} else if(response.getContacts() == null) {

			//check if an error was returned
			if(response.getStandardResponse() != null &&
				response.getStandardResponse().getReturnCode().equals(Constants.ERROR_RETURN_CODE)) {
				//an error was returned, was it an empty result or something else?
				StandardResponse clarifyResponse = response.getStandardResponse();
				if(clarifyResponse.getReturnMessage().equals(Constants.NO_RECORDS_MESSAGE)) {
					contacts = new ArrayList<SiteContacts>();
				} else {
					throw new ClarifyWSException(clarifyResponse.getReturnCode()+": "+clarifyResponse.getReturnMessage());
				}
			}
			
		} else if(response.getContacts().getSiteContacts() != null &&
				!response.getContacts().getSiteContacts().isEmpty()) {
			contacts = response.getContacts().getSiteContacts();
		}
		
		return contacts;
	}

	public List<HgbstElement> getLevel2Dropdown(String level1) throws ClarifyWSException {
		HgbstResp response = clarifyClient.getHgbstList(Constants.DROPDOWN_LIST_TYPE, level1, null, null, null);
		List<HgbstElement> dropdownList = null;
		if(response == null) {
			throw new ClarifyWSException("The response object returned from the web service is null!");
		} else if(response.getHgbstElements() == null) {
			//FIXME: determine if this is an OK situation or not. Is it possible for this to be null because there were no records found?
			//response.getStandardResponse().getReturnCode()
			throw new ClarifyWSException("The ArrayOfHgbstElement object returned from the web service is null!");
		} else if(response.getHgbstElements().getHgbstElement() != null &&
				!response.getHgbstElements().getHgbstElement().isEmpty()) {
			dropdownList = response.getHgbstElements().getHgbstElement();
		}
		return dropdownList;
	}

	public List<HgbstElement> getLevel3Dropdown(String level1, String level2) throws ClarifyWSException {
		HgbstResp response = clarifyClient.getHgbstList(Constants.DROPDOWN_LIST_TYPE, level1, level2, null, null);
		List<HgbstElement> dropdownList = new ArrayList<HgbstElement>();
		if(response == null) {
			logger.warn("The response object returned from the web service is null!");
		} else if(response.getHgbstElements() == null) {
			logger.warn("The ArrayOfHgbstElement object returned from the web service is null!");
		} else if(response.getHgbstElements().getHgbstElement() != null &&
				!response.getHgbstElements().getHgbstElement().isEmpty()) {
			dropdownList = response.getHgbstElements().getHgbstElement();
		}
		return dropdownList;
	}

	public StandardResponse createCase(String siteId, String contactFName,
			String contactLName, String contactPhone, String troubleType,
			String troubleSubType, String troubleSubTypeCat, String priority,
			String phoneLog)
			throws ClarifyWSException {
		//if this is set to 'noOpt', then nothing was returned from the server for lvl3 dropdown
		//no need to send to the server.
		if(troubleSubTypeCat.equalsIgnoreCase("noOpt")) {
			troubleSubTypeCat = "";
		}
		StandardResponse response = clarifyClient.createCase(
					siteId,				//arg0 = site id
					contactFName,		//arg1 = contact first name
					contactLName,		//arg2 = contact last name
					contactPhone,		//arg3 = contact phone
					null,				//arg4 = title
					troubleType,		//arg5 = trouble type, aka hgbst - level1
					troubleSubType,		//arg6 = trouble subtype, aka hgbst - level2
					troubleSubTypeCat,	//arg7 = trouble subtype category, aka hgbst - level3
					null,				//arg8 = fsMarket
					priority,			//arg9 = priority
					null,				//arg10 = Severity
					null,				//arg11 = Status
					null,				//arg12 = CaseType
					phoneLog,			//arg13 = phone log
					null,				//arg14 = Queue
					null,				//arg15 = creator login
					null,				//arg16 = Lock Title (Boolean)
					null,				//arg17 = Allow Email Survey (Boolean)
					null,				//arg18 = Survey Email
					null,				//arg19 = Allow Email status change (Boolean)
					null,				//arg20 = Status Change Email
					null);				/*arg21 = TreatmentElement:
													-Balance 30 to 60
													-Balance 61 to 90
													-Balance over 90
													-Invoice Amount
													-Credit class
													-Invoice Cycle */
		if(response == null) {
			throw new ClarifyWSException("The response object returned from the create case web service is null!");
		} else {
			return response;
		}
	}

	//overloaded version of createCase that allows input of ALL fields
	public StandardResponse createCase(String siteId, String contactFName,
			String contactLName, String contactPhone, String title, String troubleType,
			String troubleSubType, String troubleSubTypeCat, String fsMarket, String priority,
			String severity, String status, String caseType, String phoneLog, String queue,
			String creatorLogin, Boolean lockTitle, Boolean allowEmailSurvey, String surveyEmail,
			Boolean allowEmailStatusChange, String statusChangeEmail, TreatmentElement treatmentInfo)
			throws ClarifyWSException {
		StandardResponse response = clarifyClient.createCase(
					siteId,				//arg0 = site id
					contactFName,		//arg1 = contact first name
					contactLName,		//arg2 = contact last name
					contactPhone,		//arg3 = contact phone
					title,				//arg4 = title
					troubleType,		//arg5 = trouble type, aka hgbst - level1
					troubleSubType,		//arg6 = trouble subtype, aka hgbst - level2
					troubleSubTypeCat,	//arg7 = trouble subtype category, aka hgbst - level3
					fsMarket,			//arg8 = fsMarket
					priority,			//arg9 = priority
					severity,			//arg10 = Severity
					status,				//arg11 = Status
					caseType,			//arg12 = CaseType
					phoneLog,			//arg13 = phone log
					queue,				//arg14 = Queue
					creatorLogin,		//arg15 = creator login
					lockTitle,			//arg16 = Lock Title (Boolean)
					allowEmailSurvey,	//arg17 = Allow Email Survey (Boolean)
					surveyEmail,		//arg18 = Survey Email
					allowEmailStatusChange,//arg19 = Allow Email status change (Boolean)
					statusChangeEmail,	//arg20 = Status Change Email
					treatmentInfo);		/*arg21 = TreatmentElement:
											-Balance 30 to 60
											-Balance 61 to 90
											-Balance over 90
											-Invoice Amount
											-Credit class
											-Invoice Cycle */
		if(response == null) {
			throw new ClarifyWSException("The response object returned from the create case web service is null!");
		} else {
			return response;
		}
	}
	
	public static String getClarifyWSDL() {
		return clarifyWSDL;
	}
	public static void setClarifyWSDL(String clarifyWSDL) {
		ClarifyDAOImpl.clarifyWSDL = clarifyWSDL;
	}

}
