package com.windstream.ctctus.dao;

public interface XrefDAO {
	
	public abstract int getEAN(int acctId, int dssId);

}
