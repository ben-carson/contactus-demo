package com.windstream.ctctus.dao;

import java.util.List;

import com.windstream.bus.ws.client.ClarifyClient.HgbstElement;
import com.windstream.bus.ws.client.ClarifyClient.SiteContacts;
import com.windstream.bus.ws.client.ClarifyClient.StandardResponse;
import com.windstream.bus.ws.client.ClarifyClient.TreatmentElement;
import com.windstream.ctctus.exceptions.ClarifyWSException;

public interface ClarifyDAO {
	
	public List<SiteContacts> getSiteContacts(int ean)
		throws ClarifyWSException;
	
	//no need for level1 dropdown, as it should only be Billing or MACD
	public List<HgbstElement> getLevel2Dropdown(String level1) throws ClarifyWSException;
	public List<HgbstElement> getLevel3Dropdown(String level1, String level2) throws ClarifyWSException;
	
	public StandardResponse createCase(String siteId, String contactFName, String contactLName,
			String contactPhone, String troubleType, String troubleSubType, String troubleSubTypeCat,
			String priority, String phoneLog) throws ClarifyWSException;

	public StandardResponse createCase(String siteId, String contactFName,
			String contactLName, String contactPhone, String title, String troubleType,
			String troubleSubType, String troubleSubTypeCat, String fsMarket, String priority,
			String severity, String status, String caseType, String phoneLog, String queue,
			String creatorLogin, Boolean lockTitle, Boolean allowEmailSurvey, String surveyEmail,
			Boolean allowEmailStatusChange, String statusChangeEmail, TreatmentElement treatmentInfo)
		throws ClarifyWSException;
}
