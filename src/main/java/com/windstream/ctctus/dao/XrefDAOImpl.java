package com.windstream.ctctus.dao;

import com.windstream.bus.ws.client.XrefClient.XrefAccess;
import com.windstream.bus.ws.client.XrefClient.XrefAccessPortType;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

import org.apache.log4j.Logger;

import com.windstream.ctctus.config.Constants;
import com.windstream.ctctus.properties.Properties;

public class XrefDAOImpl implements XrefDAO {

	private Logger logger = Logger.getLogger(XrefDAOImpl.class);
	private static String xrefWSDL;
	private static XrefAccess xrefAccess = null;
	private static XrefAccessPortType xrefClient = null;
	
	public XrefDAOImpl() throws FileNotFoundException, IOException {
		try {
			if(xrefAccess == null) {
				setXrefWSDL(Properties.getProperty(Constants.XREF_WSDL_PROPERTY_NAME));
				URL wsdlURL = new URL(getXrefWSDL());
				xrefAccess = new XrefAccess(wsdlURL);
			}
			if(xrefClient == null) {
				xrefClient = xrefAccess.getXrefAccessHttpPort();
			}
		} catch (IOException ioe) {
			System.err.println("ERROR: IOExeception encountered.");
			ioe.printStackTrace();
			throw ioe;
		}
		
	}
	//if the user needs to change the properties file, use this constructor
	public XrefDAOImpl(String props) throws FileNotFoundException, IOException {
		try {
			//change the properties file
			logger.debug("Changing XREF ResourceBundle to " + props);
			Properties.setMyResources(props);
			//now create the object
			new XrefDAOImpl();
		} finally {
			if(Properties.getMyResources()!=null) {
				logger.debug("Properties file being used is " + Properties.getBundleName());
				logger.debug("Xref WSDL is " + Properties.getMyResources().getString(Constants.XREF_WSDL_PROPERTY_NAME));
			} else {
				logger.error("Failure to load "+props+" file.");
			}
		}
	}
	
	public int getEAN(int acctId, int dssId) {
		// DSS IDs
		// 3 = Revchain5,5 = Billplex,10 = MSS,11 = Clarify,16 = Revchain7
		// 50 = Enterprise Account Number
		int tgtDssID = 50;
		int ean = xrefClient.getAccountNumberFromXref(acctId, dssId, tgtDssID);
		return ean;
	}

	public static void setXrefWSDL(String xrefWSDL) {
		XrefDAOImpl.xrefWSDL = xrefWSDL;
	}
	public static String getXrefWSDL() {
		return xrefWSDL;
	}
}
