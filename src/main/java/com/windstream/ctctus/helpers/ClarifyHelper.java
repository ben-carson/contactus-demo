package com.windstream.ctctus.helpers;

import com.windstream.bus.ws.client.ClarifyClient.HgbstElement;
import com.windstream.bus.ws.client.ClarifyClient.SiteContacts;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ClarifyHelper {

	public static List<String> convertHgbstList2StringList(List<HgbstElement> level2HgbstList) {
		List<String> targetList = new ArrayList<String>();
		Iterator<HgbstElement> itr = level2HgbstList.iterator();
		while(itr.hasNext()) {
			HgbstElement currElement = itr.next();
			String currString = currElement.getTitle();
			targetList.add(currString);
		}
		return targetList;
	}
	
	public static Map<String, SiteContacts> convertSiteContactsList2Map(List<SiteContacts> contactList) {
		Map<String, SiteContacts> conversionMap = new HashMap<String, SiteContacts>();
		for(SiteContacts contact: contactList) {
			conversionMap.put(((Long)contact.getContactObjid()).toString(), contact);
		}
		return conversionMap;
	}
	public static Map<String,String> convertSiteContactsList2StringMap(List<SiteContacts> contactList) {
		Map<String,String> conversionMap = new HashMap<String,String>();
		for(SiteContacts contact: contactList) {
			conversionMap.put(
					((Long)contact.getContactObjid()).toString(),
					contact.getContactFirstName() + " " + contact.getContactLastName());
		}
		return conversionMap;
	}

}
