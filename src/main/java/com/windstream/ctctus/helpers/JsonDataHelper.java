package com.windstream.ctctus.helpers;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.windstream.bus.ws.client.ClarifyClient.HgbstElement;
import com.windstream.bus.ws.client.ClarifyClient.SiteContacts;
import com.windstream.ctctus.config.Constants;

public class JsonDataHelper {

	public static JSONArray convertSiteContactsList2JSONArray(List<SiteContacts> contacts)
			throws JSONException {
		JSONArray forDisplay = new JSONArray();
		if(contacts != null && !contacts.isEmpty()) {
			for(SiteContacts contact : contacts) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put(Constants.F_NAME, contact.getContactFirstName());
				jsonObj.put(Constants.L_NAME, contact.getContactLastName());
				jsonObj.put(Constants.PHONE, contact.getContactPhone());
				forDisplay.put(jsonObj);
			}
		}
		return forDisplay;
	}

	public static JSONArray convertHgbstList2JSONArray(List<HgbstElement> hgbstList)
			throws JSONException {
		JSONArray forDisplay = new JSONArray();
		if(hgbstList != null && !hgbstList.isEmpty()) {
			for(HgbstElement element : hgbstList) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put(element.getTitle(), element.getTitle());
				forDisplay.put(element.getTitle());
			}
		}
		return forDisplay;
	}

	public static JSONObject convertContactInfoStr2ContactInfoJson(String contactInfo)
			throws JSONException, ParseException {
		JSONObject jsonObj = new JSONObject();
		JSONParser parser = new JSONParser();
		org.json.simple.JSONObject tempJSON = new org.json.simple.JSONObject();
		Object obj = parser.parse(contactInfo);
		tempJSON = (org.json.simple.JSONObject) obj;
		jsonObj.put(Constants.F_NAME, tempJSON.get("fName"));
		jsonObj.put(Constants.L_NAME, tempJSON.get("lName"));
		jsonObj.put(Constants.PHONE, tempJSON.get("phone"));
		return jsonObj;
	}
}
