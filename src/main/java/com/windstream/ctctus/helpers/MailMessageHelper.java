package com.windstream.ctctus.helpers;

import java.io.File;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.windstream.ctctus.config.Constants;
import com.windstream.ctctus.dto.CreateCaseDTO;

public class MailMessageHelper {

	public static void createHeaderInfo(MimeMessage theEmail, CreateCaseDTO caseDto)
			throws AddressException, MessagingException {
		//set FROM address
		theEmail.setFrom(new InternetAddress(Constants.NO_REPLY_EMAIL));

		//set TO address to the address provided on the form.
		//This will be confirmation of submission to the AM
		theEmail.setRecipient(Message.RecipientType.TO, new InternetAddress(caseDto.getManagerEmail()));
		theEmail.setSubject("Ticket "+caseDto.getReferenceValue()+" created by "+caseDto.getManagerName());
	}

	public static MimeBodyPart addEmailBody(String theBody)
		throws MessagingException {
		//create a mime body part for the email's BODY
		MimeBodyPart messageBody = new MimeBodyPart();
		messageBody.setText(theBody);
		return messageBody;
	}
	
	public static MimeBodyPart addAttachment(File theAttachment)
		throws Exception {
		Logger logger = Logger.getLogger(MailMessageHelper.class);
		//create a mime body part for the email's ATTACHMENT
		MimeBodyPart messageAttachment = new MimeBodyPart();
		
		logger.info("Attaching file '" + theAttachment.getName() + "'");
		
		DataSource fileDataSource = new FileDataSource(theAttachment);
		messageAttachment.setDataHandler(new DataHandler(fileDataSource));
		messageAttachment.setFileName(theAttachment.getName());
		return messageAttachment;
	}
}
