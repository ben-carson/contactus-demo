package com.windstream.ctctus.exceptions;

public class ClarifyWSException extends Exception {

	private static final long serialVersionUID = 267759530816982195L;
	String theError;
	
	//default constructor
	//initialize error message to "unknown"
	public ClarifyWSException() {
		super();					//superclass constructor
		theError = "unknown";
	}
	
	//constructor with argument
	public ClarifyWSException(String specificError) {
		super(specificError);		//superclass constructor
		theError = specificError;	//save the message
	}
	
	public String getTheError() {
		return theError;
	}
	
}
