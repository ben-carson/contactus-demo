package com.windstream.ctctus.config;


public class Constants {

	public static final String APP_PROPERTIES_BUNDLE = "app";
	public static final String TEST_PROPERTIES_BUNDLE = "test";
	
	public static final String PROP_FILE_NAME = "src/main/resources/app.properties";
//	public static final String TEST_PROP_FILE_NAME = "src/test/resources/test.properties";

	public static final String XREF_WSDL_PROPERTY_NAME = "xref.wsdl.location";
	public static final String CLARIFY_WSDL_PROPERTY_NAME = "clarify.wsdl.location";
	public static final String MAIL_HOST_NAME = "mail.smtp.host";
	//bad, I know. Should be in app.properties file, but MailSession requires a property object.
	public static final String MAIL_HOST_VALUE = "smtp.nuvox.net";
	
	//the type of list to use for the getHgbstList method
	public static final String DROPDOWN_LIST_TYPE = "x_merged_open_codes";

	public static final Object ERROR_RETURN_CODE = "Error";
	public static final Object NO_RECORDS_MESSAGE = "No record found";
	
	public static final String NO_REPLY_EMAIL = "donotreply@windstream.com";
	//DEV
	public static final String MAC_EMAIL = "bacarso@gmail.com";
//	public static final String MAC_EMAIL = "Christopher.Mccasland@windstream.com";
	//PROD
//	public static final String MAC_EMAIL = "WINDSTREAM.AM_MAC_Orders@windstream.com";
	
	//DEV
	public static final String CUST_SERV_EMAIL = "benjamin.carson@windstream.com";
	//PROD
//	public static final String CUST_SERV_EMAIL = "WCI.CEC.CustServ@windstream.com";

    //--== constants for JSON ==--
	//JSON - Contact
	public static final String F_NAME = "fName";
	public static final String L_NAME = "lName";
	public static final String PHONE = "phone";
	
	//JSON - Level 1
	public static final String CODE_JSON_VAR = "reqCode";
	public static final String TEXT_JSON_VAR = "reqDesc";
	public static final String ACCT_CHG_CODE = "MACD";
	public static final String ACCT_CHG_TEXT = "Making changes to your current account and services";
	public static final String BILL_CODE = "Billing";
	public static final String BILL_TEXT = "Billing questions regarding your account";
	public static final String TECH_SUPP_CODE = "Tech";
	public static final String TECH_SUPP_TEXT = "Technical support for your services";
	public static final String NON_CRIT_CODE = "NonCrit";
	public static final String NON_CRIT_TEXT = "Non-critical issues (Please allow 48 hours for a response)";
	
	//--== constants for JUnit ==--
	public static final int TEST_ACCT_NUM = 2431493;
	public static final int TEST_DSS_ID = 50;
	public static final String TEST_LEVEL1 = "Billing";
	public static final String TEST_LEVEL2 = "Disputes";
	public static final String TEST_LEVEL3 = "Balance Dispute";
	public static final String TEST_CONTACT_FNAME = "NORTH";
	public static final String TEST_CONTACT_LNAME = "AMERICAN TELECOM";
	public static final String TEST_CONTACT_PHONE = "9544498000";
	public static final String TEST_PRIORITY = "Low";
	public static final String TEST_PHONE_LOG = "Created with JUnit as a test case!";
}
