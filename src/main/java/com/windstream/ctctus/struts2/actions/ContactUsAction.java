package com.windstream.ctctus.struts2.actions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;

import com.opensymphony.xwork2.ActionSupport;
import com.windstream.bus.ws.client.ClarifyClient.SiteContacts;
import com.windstream.bus.ws.client.ClarifyClient.StandardResponse;
import com.windstream.ctctus.config.Constants;
import com.windstream.ctctus.dto.CreateCaseDTO;
import com.windstream.ctctus.dto.DisplayDTO;
import com.windstream.ctctus.exceptions.ClarifyWSException;
import com.windstream.ctctus.helpers.JsonDataHelper;
import com.windstream.ctctus.helpers.MailMessageHelper;
import com.windstream.ctctus.models.ContactUsModel;
import com.windstream.ctctus.models.ContactUsModelImpl;

public class ContactUsAction extends ActionSupport{

	private static final long serialVersionUID = -4891534445816414681L;
	private static final Logger logger = Logger.getLogger(ContactUsAction.class);
	private CreateCaseDTO caseDto = new CreateCaseDTO();
	private DisplayDTO displayDto = new DisplayDTO();
	private ContactUsModel contactModel = new ContactUsModelImpl();
	
	public String validateAcctNum() {
		String result = SUCCESS;
		try {
			if(caseDto.getSiteId() != null && !caseDto.getSiteId().equals("")) {
				String siteIdString = caseDto.getSiteId();
				int siteId = Integer.parseInt(siteIdString);
				int trueEAN = contactModel.findEAN(siteId);
				if(trueEAN == 0) {
					result = ERROR;
					String noAcct = "The account, "+siteId+", does not exist. Please try again.";
					addActionMessage(noAcct);
					displayDto.setDisplayMessage(noAcct);
					logger.info(noAcct);
				}
				caseDto.setSiteId(String.valueOf(trueEAN));
			} else {
				result = INPUT;
				String message = "Please provide an account number.";
				addActionError(message);
				logger.debug(message);
				displayDto.setDisplayMessage(message);
			}
		} catch (NumberFormatException nfe) {
			result = ERROR;
			String message = "An account number can only include numbers.";
			addActionError(message);
			logger.debug(message);
			caseDto.setSiteId("");	//blank out the account id so its not included in the error message displayed 
			displayDto.setDisplayMessage(message);
		}
		return result;
	}
	//site contacts
	public String getSiteContacts() {
logger.debug("ACTION: INSIDE GETSITECONTACTS ACTION");
		String result = SUCCESS;
		if(caseDto != null && caseDto.getSiteId()!=null && !caseDto.getSiteId().equals("")) {
			List<SiteContacts> contacts = null;
			try {
logger.debug("ACTION: ABOUT TO CALL CONTACTS METHOD ON MODEL");
				contacts = contactModel.findSiteContacts(Integer.parseInt(caseDto.getSiteId()));
logger.debug("ACTION: RETURNING FROM CONTACTS METHOD ON MODEL");
				displayDto.setContacts(JsonDataHelper.convertSiteContactsList2JSONArray(contacts));
			} catch (JSONException e) {
				result=ERROR;
				logger.error("JSONException in ContactUsAction: " + e.getMessage());
				addActionError(e.getMessage());
				displayDto.setDisplayMessage(e.getMessage());
				e.printStackTrace();
			} catch (NumberFormatException e) {
				result=ERROR;
				logger.error("NumberFormatException in ContactUsAction: " + e.getMessage());
				addActionError(e.getMessage());
				displayDto.setDisplayMessage(e.getMessage());
			} catch (ClarifyWSException e) {
				result=ERROR;
				logger.error("ClarifyWSException in ContactUsAction: " + e.getMessage());
				addActionError(e.getMessage());
				displayDto.setDisplayMessage(e.getMessage());
			}
		} else {
			result = INPUT;
			logger.info("There was no account number provided.");
			addActionError("Please provide an account number.");
		}
		return result;
	}
	
	//level 1 dropdown
	public String getTroubleType() {
		String result = SUCCESS;
		try {
			JSONArray troubleTypes = new JSONArray();
			troubleTypes = contactModel.populateLevel1Dropdown();
			displayDto.setRequestList(troubleTypes);
		} catch (JSONException je) {
			result = ERROR;
			logger.error("JSONException while getting level 1 dropdown.");
			logger.error(je.getMessage());
			je.printStackTrace();
		}
		return result;
	}
	//level 2
	public String getTroubleSubType() {
		String result = SUCCESS;
		try {
			JSONArray tSubTypes = new JSONArray();
			tSubTypes = contactModel.populateLevel2Dropdown(caseDto.getRequestType());
			displayDto.setRequestSubTypeList(tSubTypes);
		} catch (ClarifyWSException cE) {
			result = ERROR;
			logger.error("ERROR: ClarifyWSException while retrieving level 2 dropdown list.");
			logger.error(cE);
			cE.printStackTrace();
		} catch (FileNotFoundException e) {
			result = ERROR;
			logger.error("ERROR: properties file not found while attempting to populate level 2 dropdown.");
			logger.error(e);
			e.printStackTrace();
		} catch (IOException e) {
			result = ERROR;
			logger.error("ERROR: IOException while attempting to populate level 2 dropdown.");
			logger.error(e);
			e.printStackTrace();
		} catch (JSONException je) {
			result = ERROR;
			logger.error("JSONException while getting level 2 dropdown.");
			logger.error(je.getMessage());
			je.printStackTrace();
		}
		return result;
	}
	//level 3
	public String getTroubleSubTypeCategory() {
		String result = SUCCESS;
		try {
			JSONArray troubleSubTypeCategories = new JSONArray();
			troubleSubTypeCategories = contactModel.populateLevel3Dropdown(caseDto.getRequestType(), caseDto.getRequestSubType());
			displayDto.setReqSubTypeCatList(troubleSubTypeCategories);
		} catch (ClarifyWSException cE) {
			result = ERROR;
			logger.error("ERROR: ClarifyWSException while retrieving level 3 dropdown list.");
			logger.error(cE);
			displayDto.setDisplayMessage("Error: " + cE.getMessage());
			cE.printStackTrace();
		} catch (FileNotFoundException e) {
			result = ERROR;
			logger.error("ERROR: Properties file not found while attempting to populate level 3 dropdown.");
			logger.error(e);
			displayDto.setDisplayMessage("Error: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			result = ERROR;
			String genericError = "ERROR: IOException while attempting to populate level 3 dropdown.";
			String specificError = e.getMessage(); 
			logger.error(genericError);
			logger.error(specificError);
			displayDto.setDisplayMessage("Error: " + e.getMessage());
			e.printStackTrace();
		} catch (JSONException je) {
			result = ERROR;
			String genericError = "JSONException while getting level 2 dropdown.";
			String specificError = je.getMessage(); 
			logger.error(genericError);
			logger.error(specificError);
			displayDto.setDisplayMessage("Error: " + je.getMessage());
			addActionError(je.getMessage());
			je.printStackTrace();
		}
		return result;
	}

	public String createTicket() {
		String result = SUCCESS;
		StandardResponse response;
		
		//determine if the user was an account manager and take appropriate action if so.
		if(caseDto.isManager()) {
			StringBuilder newLog = new StringBuilder();
			newLog.append("Ticket submitted by ");
			newLog.append(caseDto.getManagerName());
			newLog.append(" ("+caseDto.getManagerEmail()+")");
			newLog.append("\n\n");
			caseDto.setPhoneLog(newLog + caseDto.getPhoneLog());
		}
		
		try {
			response = contactModel.createCase(
					caseDto.getSiteId(),
					caseDto.getContactInfo().getString(Constants.F_NAME),
					caseDto.getContactInfo().getString(Constants.L_NAME),
					caseDto.getContactInfo().getString(Constants.PHONE),
					caseDto.getRequestType(),
					caseDto.getRequestSubType(),
					caseDto.getRequestSubTypeCategory(),
					caseDto.getPriority(), 
					caseDto.getPhoneLog());
			caseDto.setReturnCode(response.getReturnCode());
			caseDto.setResponseMessage(response.getReturnMessage());
			caseDto.setReferenceName(response.getReturnReferenceName());
			caseDto.setReferenceValue(response.getReturnReferenceValue());
			
			//Display a success message in the log
			logger.info(" *************************************************************************");
			logger.info(" * Ticket "+caseDto.getReferenceValue()+" was successfully created.");
			if(caseDto.isManager()) {
				Properties props = new Properties();
				props.put(
						Constants.MAIL_HOST_NAME, 
						Constants.MAIL_HOST_VALUE);
				Session mailSession = Session.getInstance(props);
				
				MimeMessage theEmail = new MimeMessage(mailSession);
				MailMessageHelper.createHeaderInfo(theEmail, caseDto);
				
				Multipart bodyAndAttachment = new MimeMultipart();
				
				//add the message body
				MimeBodyPart theBody = MailMessageHelper.addEmailBody(caseDto.getPhoneLog());
				bodyAndAttachment.addBodyPart(theBody);
				
				//add the attachment, if needed
				if(caseDto.getAcctMngrAttachment() != null &&
						caseDto.getAcctMngrAttachment().exists()) {
					
					//if attachment exists, send it to the acct man help desk
					theEmail.setRecipient(Message.RecipientType.TO, new InternetAddress(Constants.MAC_EMAIL));
					//add the attachment to the email
					MimeBodyPart theAttachment = MailMessageHelper.addAttachment(caseDto.getAcctMngrAttachment());
					bodyAndAttachment.addBodyPart(theAttachment);
				}
				
				//add body (and attachment) to the email object
				theEmail.setContent(bodyAndAttachment);
				
				//send the message
				Transport.send(theEmail);
				
				//add to the log
				logger.info(" * Emails were sent to "+Constants.MAC_EMAIL+" & "+caseDto.getManagerEmail()+".");
				logger.info(" * "+caseDto.getAcctMngrAttachment().getName() +" was attached.");
			}
			logger.info(" *************************************************************************");
		} catch (JSONException e) {
			result = ERROR;
			logger.error("JSONException thrown: "+e.getMessage());
			logger.error("Ticket was not created.");
			addActionError("An error occured. Your ticket was not created.");
			displayDto.setDisplayMessage("An problem occurred. Your ticket was not created.");
			e.printStackTrace();
		} catch (AddressException e) {
			result = ERROR;
			logger.error("AddressException thrown: "+e.getMessage());
			logger.error("Ticket "+caseDto.getReferenceValue()+" WAS created, but emails were not sent. Attachments were lost.");
			displayDto.setDisplayMessage("Ticket "+caseDto.getReferenceValue()+" was created, but there was a problem sending emails. Any attachments have been lost.");
			e.printStackTrace();
		} catch (MessagingException e) {
			result = ERROR;
			logger.error("MessagingException thrown: "+ e.getMessage());
			logger.error("Ticket "+caseDto.getReferenceValue()+" WAS created, but emails were not sent. Attachments were lost.");
			displayDto.setDisplayMessage("Ticket "+caseDto.getReferenceValue()+" was created, but there was a problem sending emails. Any attachments have been lost.");
			e.printStackTrace();
		} catch (Exception e) {
			result = ERROR;
			logger.error("Exception thrown: "+ e.getMessage());
			logger.error("Ticket "+caseDto.getReferenceValue()+" WAS created, but emails were not sent. Attachments were lost.");
			displayDto.setDisplayMessage("Ticket "+caseDto.getReferenceValue()+" was created, but there was a problem sending emails. Any attachments have been lost.");
			e.printStackTrace();
		}
		return result;
	}
	
	public void setCaseDto(CreateCaseDTO caseDto) {
		this.caseDto = caseDto;
	}
	public CreateCaseDTO getCaseDto() {
		return caseDto;
	}
	
	public void setDisplayDto(DisplayDTO displayDto) {
		this.displayDto = displayDto;
	}
	public DisplayDTO getDisplayDto() {
		return displayDto;
	}

}
