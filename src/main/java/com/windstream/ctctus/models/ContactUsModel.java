package com.windstream.ctctus.models;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.windstream.bus.ws.client.ClarifyClient.SiteContacts;
import com.windstream.bus.ws.client.ClarifyClient.StandardResponse;
import com.windstream.ctctus.exceptions.ClarifyWSException;

public interface ContactUsModel extends Serializable {

	public JSONArray populateLevel1Dropdown()
			throws JSONException;
	public JSONArray populateLevel2Dropdown(String lvl1Selection)
			throws FileNotFoundException, ClarifyWSException, IOException,JSONException;
	public JSONArray populateLevel3Dropdown(String lvl1Selection, String lvl2Selection)
			throws FileNotFoundException, ClarifyWSException, IOException, JSONException;
	
	public int findEAN(int acctNum);
	public List<SiteContacts> findSiteContacts(int acctNum)
			throws ClarifyWSException;
	
	public StandardResponse createCase(String siteID, String contactFirstName, String contactLastName,
			String contactPhone, String troubleType, String troubleSubType, String troubleSubTypeCategory,
			String priority,String phoneLog);
	
}
