package com.windstream.ctctus.models;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.windstream.bus.ws.client.ClarifyClient.HgbstElement;
import com.windstream.bus.ws.client.ClarifyClient.SiteContacts;
import com.windstream.bus.ws.client.ClarifyClient.StandardResponse;
import com.windstream.ctctus.config.Constants;
import com.windstream.ctctus.dao.ClarifyDAO;
import com.windstream.ctctus.dao.ClarifyDAOImpl;
import com.windstream.ctctus.dao.XrefDAO;
import com.windstream.ctctus.dao.XrefDAOImpl;
import com.windstream.ctctus.exceptions.ClarifyWSException;
import com.windstream.ctctus.helpers.JsonDataHelper;

public class ContactUsModelImpl implements ContactUsModel {

	private static final long serialVersionUID = 8519001686393512066L;
	private Logger logger = Logger.getLogger(ContactUsModelImpl.class);
	//this list won't change, so make it final and static
	private static final JSONArray level1List = new JSONArray();
	private static XrefDAO xrefDao;
	private static ClarifyDAO clarifyDao;

	public int findEAN(int acctNum) {
		int dssId = 50;
		int ean = 0;
		try {
			ean = getXrefDao().getEAN(acctNum, dssId);
		} catch (FileNotFoundException e) { 
			logger.error("ERROR: properties file not found while retrieving EAN.");
			logger.error(e);
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("ERROR: IOException while retrieving EAN.");
			logger.error(e);
			e.printStackTrace();
		}
		return ean;
	}

	public List<SiteContacts> findSiteContacts(int acctNum)
			throws ClarifyWSException {
logger.debug("MODEL: INSIDE FINDSITECONTACTS METHOD IN THE MODEL.");
		List<SiteContacts> contacts = null;
		try {
logger.debug("MODEL: TRYING THE WEB SERVICE ON THE CLARIFYdao");
			contacts = getClarifyDao().getSiteContacts(acctNum);
logger.debug("MODEL: RETURNED TO THE MODEL FROM THE CLARIFYDAO");
		} catch (ClarifyWSException cE) {
			logger.error("ERROR: ClarifyWSException while retrieving site contact list.");
			logger.error(cE);
			cE.printStackTrace();
			throw cE;
		} catch (FileNotFoundException e) {
			logger.error("ERROR: Properties file not found while retrieving site contact list.");
			logger.error(e);
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("ERROR: IOException while retrieving site contact list.");
			logger.error(e);
			e.printStackTrace();
		}
		return contacts;
	}

	public JSONArray populateLevel1Dropdown()
			throws JSONException {
		//this method is all hard-coded because it won't change. The requirements are 
		//only to support MACD and Billing requests.
		if(level1List!=null && level1List.length()<=0) {
			JSONObject tempObj = new JSONObject();
			tempObj.put(Constants.CODE_JSON_VAR, Constants.ACCT_CHG_CODE);
			tempObj.put(Constants.TEXT_JSON_VAR, Constants.ACCT_CHG_TEXT);
			level1List.put(tempObj);
			
			tempObj = new JSONObject();
			tempObj.put(Constants.CODE_JSON_VAR, Constants.BILL_CODE);
			tempObj.put(Constants.TEXT_JSON_VAR, Constants.BILL_TEXT);
			level1List.put(tempObj);
			
			tempObj = new JSONObject();
			tempObj.put(Constants.CODE_JSON_VAR, Constants.TECH_SUPP_CODE);
			tempObj.put(Constants.TEXT_JSON_VAR, Constants.TECH_SUPP_TEXT);
			level1List.put(tempObj);
	
			tempObj = new JSONObject();
			tempObj.put(Constants.CODE_JSON_VAR, Constants.NON_CRIT_CODE);
			tempObj.put(Constants.TEXT_JSON_VAR, Constants.NON_CRIT_TEXT);
			level1List.put(tempObj);
		}
		
		return level1List;
	}

	public JSONArray populateLevel2Dropdown(String lvl1Selection)
		throws FileNotFoundException, ClarifyWSException, IOException, JSONException {
		JSONArray level2List = new JSONArray();
		List<HgbstElement> level2HgbstList = new ArrayList<HgbstElement>();
		level2HgbstList = getClarifyDao().getLevel2Dropdown(lvl1Selection);
		level2List = JsonDataHelper.convertHgbstList2JSONArray(level2HgbstList);
		return level2List;
	}

	public JSONArray populateLevel3Dropdown(String lvl1Selection, String lvl2Selection)
			throws FileNotFoundException, ClarifyWSException, IOException, JSONException {
		JSONArray level3List = new JSONArray();
		List<HgbstElement> level3HgbstList = new ArrayList<HgbstElement>();
		level3HgbstList = getClarifyDao().getLevel3Dropdown(lvl1Selection, lvl2Selection);
		level3List = JsonDataHelper.convertHgbstList2JSONArray(level3HgbstList);
		return level3List;
	}

	public StandardResponse createCase(String siteID, String contactFirstName,
			String contactLastName, String contactPhone, String troubleType,
			String troubleSubType, String troubleSubTypeCategory,
			String priority, String phoneLog) {
		StandardResponse response = null;
		try {
			response = clarifyDao.createCase(siteID,
					contactFirstName, contactLastName, contactPhone,
					troubleType, troubleSubType, troubleSubTypeCategory,
					priority, phoneLog);
		} catch (ClarifyWSException cE) {
			logger.error("ERROR: An exception occured while trying to create a clarify case.");
			logger.error(cE);
		}
		return response;
	}

	public void setXrefDao(XrefDAO xrefDao) {
		this.xrefDao = xrefDao;
	}
	public XrefDAO getXrefDao() throws FileNotFoundException, IOException {
		if(xrefDao == null) {
			xrefDao = new XrefDAOImpl();
		}
		return xrefDao;
	}

	public void setClarifyDao(ClarifyDAO clarifyDao) {
		this.clarifyDao = clarifyDao;
	}
	public ClarifyDAO getClarifyDao() throws FileNotFoundException, IOException {
		if(clarifyDao == null) {
			clarifyDao = new ClarifyDAOImpl();
		}
		return clarifyDao;
	}

}
