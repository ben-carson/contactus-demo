package com.windstream.ctctus.dto;

import java.io.File;
import java.io.Serializable;

import org.apache.commons.fileupload.FileItem;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;

import com.windstream.ctctus.helpers.JsonDataHelper;

public class CreateCaseDTO implements Serializable {

	private static final long serialVersionUID = -2208817658099052399L;
	private String siteId;
	private JSONObject contactInfo;
	private String requestType;
	private String requestSubType;
	private String requestSubTypeCategory;
	private String priority;
	private String phoneLog;
	
	//account Manager section
	private boolean manager;
	private String managerName;
	private String managerEmail;
	private File acctMngrAttachment;
	
	//fields for the Clarify response
	private String returnCode; //a literal of either "success" or "error"
	private String responseMessage; //status message
	private String referenceName; //the literal "case id"
	private String referenceValue; //null if "error", case id if "success"
	
	/*--== Form data accessor methods ==--*/
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public void setContactInfo(String contactInfo)
			throws JSONException, ParseException {
		this.contactInfo = JsonDataHelper.convertContactInfoStr2ContactInfoJson(contactInfo);
	}
	public JSONObject getContactInfo() {
		return contactInfo;
	}
	
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String troubleType) {
		this.requestType = troubleType;
	}
	
	public String getRequestSubType() {
		return requestSubType;
	}
	public void setRequestSubType(String requestSubType) {
		this.requestSubType = requestSubType;
	}
	
	public String getRequestSubTypeCategory() {
		return requestSubTypeCategory;
	}
	public void setRequestSubTypeCategory(String requestSubTypeCategory) {
		this.requestSubTypeCategory = requestSubTypeCategory;
	}
	
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	public String getPhoneLog() {
		return phoneLog;
	}
	public void setPhoneLog(String phoneLog) {
		this.phoneLog = phoneLog;
	}
	
	/*--== Account Manager accessor methods ==--*/
	public boolean isManager() {
		return manager;
	}
	public void setManager(boolean isManager) {
		this.manager = isManager;
	}
	
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	
	public String getManagerEmail() {
		return managerEmail;
	}
	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}
	
	public File getAcctMngrAttachment() {
		return acctMngrAttachment;
	}
	public void setAcctMngrAttachment(File formAttachment) {
		this.acctMngrAttachment = formAttachment;
	}
	
	/*--== Server response accessor methods ==--*/
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	public String getReferenceName() {
		return referenceName;
	}
	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}
	
	public String getReferenceValue() {
		return referenceValue;
	}
	public void setReferenceValue(String referenceValue) {
		this.referenceValue = referenceValue;
	}
}
