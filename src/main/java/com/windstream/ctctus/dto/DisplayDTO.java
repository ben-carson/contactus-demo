package com.windstream.ctctus.dto;

import java.io.Serializable;

import org.json.JSONArray;

import com.windstream.ctctus.config.Constants;

public class DisplayDTO implements Serializable {

	private static final long serialVersionUID = -8783731832415695428L;

	private JSONArray contacts;
	private JSONArray requestList;
	private JSONArray requestSubTypeList;
	private JSONArray reqSubTypeCatList;
	//generic field for displaying messages to the user
	private String displayMessage;

	public String getCustServEmail() {
		return Constants.CUST_SERV_EMAIL;
	}
	
	public void setContacts(JSONArray newContacts) {
		this.contacts = newContacts;
	}
	public JSONArray getContacts() {
		return contacts;
	}
	
	public JSONArray getRequestList() {
		return requestList;
	}
	public void setRequestList(JSONArray requestList) {
		this.requestList = requestList;
	}
	
	public JSONArray getRequestSubTypeList() {
		return requestSubTypeList;
	}
	public void setRequestSubTypeList(JSONArray requestSubTypeList) {
		this.requestSubTypeList = requestSubTypeList;
	}
	
	public JSONArray getReqSubTypeCatList() {
		return reqSubTypeCatList;
	}
	public void setReqSubTypeCatList(JSONArray reqSubTypeCatList) {
		this.reqSubTypeCatList = reqSubTypeCatList;
	}
	
	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}
	public String getDisplayMessage() {
		return displayMessage;
	}
	
}
