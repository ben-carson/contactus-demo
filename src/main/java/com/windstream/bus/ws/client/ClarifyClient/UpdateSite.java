
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteType" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SiteType"/>
 *         &lt;element name="Status" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SiteStatus"/>
 *         &lt;element name="FSMarket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NPANXX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="District" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IndustryType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryUse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryAddress" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}AddressElement"/>
 *         &lt;element name="BillingAddress" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}AddressElement"/>
 *         &lt;element name="ShippingAddress" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}AddressElement"/>
 *         &lt;element name="ParentSiteIDNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SupportOfficeSiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "siteID",
    "siteName",
    "siteType",
    "status",
    "fsMarket",
    "npanxx",
    "phone",
    "fax",
    "region",
    "district",
    "industryType",
    "primaryUse",
    "primaryAddress",
    "billingAddress",
    "shippingAddress",
    "parentSiteIDNum",
    "supportOfficeSiteID"
})
@XmlRootElement(name = "UpdateSite")
public class UpdateSite {

    @XmlElement(name = "SiteID")
    protected String siteID;
    @XmlElement(name = "SiteName")
    protected String siteName;
    @XmlElement(name = "SiteType", required = true, nillable = true)
    protected SiteType siteType;
    @XmlElement(name = "Status", required = true, nillable = true)
    protected SiteStatus status;
    @XmlElement(name = "FSMarket")
    protected String fsMarket;
    @XmlElement(name = "NPANXX")
    protected String npanxx;
    @XmlElement(name = "Phone")
    protected String phone;
    @XmlElement(name = "Fax")
    protected String fax;
    @XmlElement(name = "Region")
    protected String region;
    @XmlElement(name = "District")
    protected String district;
    @XmlElement(name = "IndustryType")
    protected String industryType;
    @XmlElement(name = "PrimaryUse")
    protected String primaryUse;
    @XmlElement(name = "PrimaryAddress", required = true)
    protected AddressElement primaryAddress;
    @XmlElement(name = "BillingAddress", required = true)
    protected AddressElement billingAddress;
    @XmlElement(name = "ShippingAddress", required = true)
    protected AddressElement shippingAddress;
    @XmlElement(name = "ParentSiteIDNum")
    protected String parentSiteIDNum;
    @XmlElement(name = "SupportOfficeSiteID")
    protected String supportOfficeSiteID;

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteID(String value) {
        this.siteID = value;
    }

    /**
     * Gets the value of the siteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * Sets the value of the siteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteName(String value) {
        this.siteName = value;
    }

    /**
     * Gets the value of the siteType property.
     * 
     * @return
     *     possible object is
     *     {@link SiteType }
     *     
     */
    public SiteType getSiteType() {
        return siteType;
    }

    /**
     * Sets the value of the siteType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SiteType }
     *     
     */
    public void setSiteType(SiteType value) {
        this.siteType = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link SiteStatus }
     *     
     */
    public SiteStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link SiteStatus }
     *     
     */
    public void setStatus(SiteStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the fsMarket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFSMarket() {
        return fsMarket;
    }

    /**
     * Sets the value of the fsMarket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFSMarket(String value) {
        this.fsMarket = value;
    }

    /**
     * Gets the value of the npanxx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNPANXX() {
        return npanxx;
    }

    /**
     * Sets the value of the npanxx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNPANXX(String value) {
        this.npanxx = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Gets the value of the district property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistrict() {
        return district;
    }

    /**
     * Sets the value of the district property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistrict(String value) {
        this.district = value;
    }

    /**
     * Gets the value of the industryType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustryType() {
        return industryType;
    }

    /**
     * Sets the value of the industryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustryType(String value) {
        this.industryType = value;
    }

    /**
     * Gets the value of the primaryUse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryUse() {
        return primaryUse;
    }

    /**
     * Sets the value of the primaryUse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryUse(String value) {
        this.primaryUse = value;
    }

    /**
     * Gets the value of the primaryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link AddressElement }
     *     
     */
    public AddressElement getPrimaryAddress() {
        return primaryAddress;
    }

    /**
     * Sets the value of the primaryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressElement }
     *     
     */
    public void setPrimaryAddress(AddressElement value) {
        this.primaryAddress = value;
    }

    /**
     * Gets the value of the billingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link AddressElement }
     *     
     */
    public AddressElement getBillingAddress() {
        return billingAddress;
    }

    /**
     * Sets the value of the billingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressElement }
     *     
     */
    public void setBillingAddress(AddressElement value) {
        this.billingAddress = value;
    }

    /**
     * Gets the value of the shippingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link AddressElement }
     *     
     */
    public AddressElement getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets the value of the shippingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressElement }
     *     
     */
    public void setShippingAddress(AddressElement value) {
        this.shippingAddress = value;
    }

    /**
     * Gets the value of the parentSiteIDNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentSiteIDNum() {
        return parentSiteIDNum;
    }

    /**
     * Sets the value of the parentSiteIDNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentSiteIDNum(String value) {
        this.parentSiteIDNum = value;
    }

    /**
     * Gets the value of the supportOfficeSiteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupportOfficeSiteID() {
        return supportOfficeSiteID;
    }

    /**
     * Sets the value of the supportOfficeSiteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupportOfficeSiteID(String value) {
        this.supportOfficeSiteID = value;
    }

}
