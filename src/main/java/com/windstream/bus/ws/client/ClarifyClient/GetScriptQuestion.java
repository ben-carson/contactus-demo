
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QuestionObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "questionObjid"
})
@XmlRootElement(name = "GetScriptQuestion")
public class GetScriptQuestion {

    @XmlElement(name = "QuestionObjid")
    protected long questionObjid;

    /**
     * Gets the value of the questionObjid property.
     * 
     */
    public long getQuestionObjid() {
        return questionObjid;
    }

    /**
     * Sets the value of the questionObjid property.
     * 
     */
    public void setQuestionObjid(long value) {
        this.questionObjid = value;
    }

}
