
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCaseLogResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCaseLogResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StandardResponse" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *         &lt;element name="CaseLogs" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ArrayOfCaseLog" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCaseLogResp", propOrder = {
    "standardResponse",
    "caseLogs"
})
public class GetCaseLogResp {

    @XmlElement(name = "StandardResponse", required = true)
    protected StandardResponse standardResponse;
    @XmlElement(name = "CaseLogs")
    protected ArrayOfCaseLog caseLogs;

    /**
     * Gets the value of the standardResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getStandardResponse() {
        return standardResponse;
    }

    /**
     * Sets the value of the standardResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setStandardResponse(StandardResponse value) {
        this.standardResponse = value;
    }

    /**
     * Gets the value of the caseLogs property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCaseLog }
     *     
     */
    public ArrayOfCaseLog getCaseLogs() {
        return caseLogs;
    }

    /**
     * Sets the value of the caseLogs property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCaseLog }
     *     
     */
    public void setCaseLogs(ArrayOfCaseLog value) {
        this.caseLogs = value;
    }

}
