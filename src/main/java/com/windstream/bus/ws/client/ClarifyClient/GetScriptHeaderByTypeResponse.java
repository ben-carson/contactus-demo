
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetScriptHeaderByTypeResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ScriptHeaderByTypeResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getScriptHeaderByTypeResult"
})
@XmlRootElement(name = "GetScriptHeaderByTypeResponse")
public class GetScriptHeaderByTypeResponse {

    @XmlElement(name = "GetScriptHeaderByTypeResult", required = true)
    protected ScriptHeaderByTypeResp getScriptHeaderByTypeResult;

    /**
     * Gets the value of the getScriptHeaderByTypeResult property.
     * 
     * @return
     *     possible object is
     *     {@link ScriptHeaderByTypeResp }
     *     
     */
    public ScriptHeaderByTypeResp getGetScriptHeaderByTypeResult() {
        return getScriptHeaderByTypeResult;
    }

    /**
     * Sets the value of the getScriptHeaderByTypeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScriptHeaderByTypeResp }
     *     
     */
    public void setGetScriptHeaderByTypeResult(ScriptHeaderByTypeResp value) {
        this.getScriptHeaderByTypeResult = value;
    }

}
