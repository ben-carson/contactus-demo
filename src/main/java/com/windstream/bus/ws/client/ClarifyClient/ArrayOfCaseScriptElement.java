
package com.windstream.bus.ws.client.ClarifyClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCaseScriptElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCaseScriptElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CaseScriptElement" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}CaseScriptElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCaseScriptElement", propOrder = {
    "caseScriptElement"
})
public class ArrayOfCaseScriptElement {

    @XmlElement(name = "CaseScriptElement")
    protected List<CaseScriptElement> caseScriptElement;

    /**
     * Gets the value of the caseScriptElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the caseScriptElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCaseScriptElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CaseScriptElement }
     * 
     * 
     */
    public List<CaseScriptElement> getCaseScriptElement() {
        if (caseScriptElement == null) {
            caseScriptElement = new ArrayList<CaseScriptElement>();
        }
        return this.caseScriptElement;
    }

}
