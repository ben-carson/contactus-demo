
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gwscSortBy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="gwscSortBy">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="AccountNo"/>
 *     &lt;enumeration value="AccountName"/>
 *     &lt;enumeration value="ContactFirstName"/>
 *     &lt;enumeration value="ContactLastName"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "gwscSortBy")
@XmlEnum
public enum GwscSortBy {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("AccountNo")
    ACCOUNT_NO("AccountNo"),
    @XmlEnumValue("AccountName")
    ACCOUNT_NAME("AccountName"),
    @XmlEnumValue("ContactFirstName")
    CONTACT_FIRST_NAME("ContactFirstName"),
    @XmlEnumValue("ContactLastName")
    CONTACT_LAST_NAME("ContactLastName");
    private final String value;

    GwscSortBy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GwscSortBy fromValue(String v) {
        for (GwscSortBy c: GwscSortBy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
