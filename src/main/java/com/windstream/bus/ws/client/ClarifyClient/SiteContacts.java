
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SiteContacts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SiteContacts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SiteObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactRoleObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ContactRoleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsContactPrimarySite" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ContactObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ContactFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactSalutation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactFax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactMobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactAfterHoursNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SiteContacts", propOrder = {
    "siteObjid",
    "siteID",
    "siteName",
    "siteStatus",
    "contactRoleObjid",
    "contactRoleName",
    "isContactPrimarySite",
    "contactObjid",
    "contactFirstName",
    "contactLastName",
    "contactPhone",
    "contactStatus",
    "contactEmail",
    "contactSalutation",
    "contactTitle",
    "contactFax",
    "contactMobileNumber",
    "contactAfterHoursNumber"
})
public class SiteContacts {

    @XmlElement(name = "SiteObjid")
    protected long siteObjid;
    @XmlElement(name = "SiteID")
    protected String siteID;
    @XmlElement(name = "SiteName")
    protected String siteName;
    @XmlElement(name = "SiteStatus")
    protected String siteStatus;
    @XmlElement(name = "ContactRoleObjid")
    protected long contactRoleObjid;
    @XmlElement(name = "ContactRoleName")
    protected String contactRoleName;
    @XmlElement(name = "IsContactPrimarySite")
    protected boolean isContactPrimarySite;
    @XmlElement(name = "ContactObjid")
    protected long contactObjid;
    @XmlElement(name = "ContactFirstName")
    protected String contactFirstName;
    @XmlElement(name = "ContactLastName")
    protected String contactLastName;
    @XmlElement(name = "ContactPhone")
    protected String contactPhone;
    @XmlElement(name = "ContactStatus")
    protected String contactStatus;
    @XmlElement(name = "ContactEmail")
    protected String contactEmail;
    @XmlElement(name = "ContactSalutation")
    protected String contactSalutation;
    @XmlElement(name = "ContactTitle")
    protected String contactTitle;
    @XmlElement(name = "ContactFax")
    protected String contactFax;
    @XmlElement(name = "ContactMobileNumber")
    protected String contactMobileNumber;
    @XmlElement(name = "ContactAfterHoursNumber")
    protected String contactAfterHoursNumber;

    /**
     * Gets the value of the siteObjid property.
     * 
     */
    public long getSiteObjid() {
        return siteObjid;
    }

    /**
     * Sets the value of the siteObjid property.
     * 
     */
    public void setSiteObjid(long value) {
        this.siteObjid = value;
    }

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteID(String value) {
        this.siteID = value;
    }

    /**
     * Gets the value of the siteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * Sets the value of the siteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteName(String value) {
        this.siteName = value;
    }

    /**
     * Gets the value of the siteStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteStatus() {
        return siteStatus;
    }

    /**
     * Sets the value of the siteStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteStatus(String value) {
        this.siteStatus = value;
    }

    /**
     * Gets the value of the contactRoleObjid property.
     * 
     */
    public long getContactRoleObjid() {
        return contactRoleObjid;
    }

    /**
     * Sets the value of the contactRoleObjid property.
     * 
     */
    public void setContactRoleObjid(long value) {
        this.contactRoleObjid = value;
    }

    /**
     * Gets the value of the contactRoleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactRoleName() {
        return contactRoleName;
    }

    /**
     * Sets the value of the contactRoleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactRoleName(String value) {
        this.contactRoleName = value;
    }

    /**
     * Gets the value of the isContactPrimarySite property.
     * 
     */
    public boolean isIsContactPrimarySite() {
        return isContactPrimarySite;
    }

    /**
     * Sets the value of the isContactPrimarySite property.
     * 
     */
    public void setIsContactPrimarySite(boolean value) {
        this.isContactPrimarySite = value;
    }

    /**
     * Gets the value of the contactObjid property.
     * 
     */
    public long getContactObjid() {
        return contactObjid;
    }

    /**
     * Sets the value of the contactObjid property.
     * 
     */
    public void setContactObjid(long value) {
        this.contactObjid = value;
    }

    /**
     * Gets the value of the contactFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactFirstName() {
        return contactFirstName;
    }

    /**
     * Sets the value of the contactFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactFirstName(String value) {
        this.contactFirstName = value;
    }

    /**
     * Gets the value of the contactLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactLastName() {
        return contactLastName;
    }

    /**
     * Sets the value of the contactLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactLastName(String value) {
        this.contactLastName = value;
    }

    /**
     * Gets the value of the contactPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * Sets the value of the contactPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPhone(String value) {
        this.contactPhone = value;
    }

    /**
     * Gets the value of the contactStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactStatus() {
        return contactStatus;
    }

    /**
     * Sets the value of the contactStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactStatus(String value) {
        this.contactStatus = value;
    }

    /**
     * Gets the value of the contactEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     * Sets the value of the contactEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactEmail(String value) {
        this.contactEmail = value;
    }

    /**
     * Gets the value of the contactSalutation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactSalutation() {
        return contactSalutation;
    }

    /**
     * Sets the value of the contactSalutation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactSalutation(String value) {
        this.contactSalutation = value;
    }

    /**
     * Gets the value of the contactTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactTitle() {
        return contactTitle;
    }

    /**
     * Sets the value of the contactTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactTitle(String value) {
        this.contactTitle = value;
    }

    /**
     * Gets the value of the contactFax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactFax() {
        return contactFax;
    }

    /**
     * Sets the value of the contactFax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactFax(String value) {
        this.contactFax = value;
    }

    /**
     * Gets the value of the contactMobileNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactMobileNumber() {
        return contactMobileNumber;
    }

    /**
     * Sets the value of the contactMobileNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactMobileNumber(String value) {
        this.contactMobileNumber = value;
    }

    /**
     * Gets the value of the contactAfterHoursNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactAfterHoursNumber() {
        return contactAfterHoursNumber;
    }

    /**
     * Sets the value of the contactAfterHoursNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactAfterHoursNumber(String value) {
        this.contactAfterHoursNumber = value;
    }

}
