
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurrentQuestionObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CurrentResponseObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "currentQuestionObjid",
    "currentResponseObjid"
})
@XmlRootElement(name = "GetScriptNextQuestion")
public class GetScriptNextQuestion {

    @XmlElement(name = "CurrentQuestionObjid")
    protected long currentQuestionObjid;
    @XmlElement(name = "CurrentResponseObjid")
    protected long currentResponseObjid;

    /**
     * Gets the value of the currentQuestionObjid property.
     * 
     */
    public long getCurrentQuestionObjid() {
        return currentQuestionObjid;
    }

    /**
     * Sets the value of the currentQuestionObjid property.
     * 
     */
    public void setCurrentQuestionObjid(long value) {
        this.currentQuestionObjid = value;
    }

    /**
     * Gets the value of the currentResponseObjid property.
     * 
     */
    public long getCurrentResponseObjid() {
        return currentResponseObjid;
    }

    /**
     * Sets the value of the currentResponseObjid property.
     * 
     */
    public void setCurrentResponseObjid(long value) {
        this.currentResponseObjid = value;
    }

}
