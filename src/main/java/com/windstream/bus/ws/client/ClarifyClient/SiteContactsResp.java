
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SiteContactsResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SiteContactsResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StandardResponse" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *         &lt;element name="Contacts" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ArrayOfSiteContacts" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SiteContactsResp", propOrder = {
    "standardResponse",
    "contacts"
})
public class SiteContactsResp {

    @XmlElement(name = "StandardResponse", required = true)
    protected StandardResponse standardResponse;
    @XmlElement(name = "Contacts")
    protected ArrayOfSiteContacts contacts;

    /**
     * Gets the value of the standardResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getStandardResponse() {
        return standardResponse;
    }

    /**
     * Sets the value of the standardResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setStandardResponse(StandardResponse value) {
        this.standardResponse = value;
    }

    /**
     * Gets the value of the contacts property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSiteContacts }
     *     
     */
    public ArrayOfSiteContacts getContacts() {
        return contacts;
    }

    /**
     * Sets the value of the contacts property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSiteContacts }
     *     
     */
    public void setContacts(ArrayOfSiteContacts value) {
        this.contacts = value;
    }

}
