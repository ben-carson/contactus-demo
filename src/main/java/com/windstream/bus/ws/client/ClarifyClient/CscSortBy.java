
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cscSortBy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="cscSortBy">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SubcaseID"/>
 *     &lt;enumeration value="SubcaseTitle"/>
 *     &lt;enumeration value="CreationTime"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "cscSortBy")
@XmlEnum
public enum CscSortBy {

    @XmlEnumValue("SubcaseID")
    SUBCASE_ID("SubcaseID"),
    @XmlEnumValue("SubcaseTitle")
    SUBCASE_TITLE("SubcaseTitle"),
    @XmlEnumValue("CreationTime")
    CREATION_TIME("CreationTime");
    private final String value;

    CscSortBy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CscSortBy fromValue(String v) {
        for (CscSortBy c: CscSortBy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
