
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseCount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseCount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CountService" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CountServiceData" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CountServiceVoice" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CountServiceFacility" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseCount", propOrder = {
    "countService",
    "countServiceData",
    "countServiceVoice",
    "countServiceFacility"
})
public class CaseCount {

    @XmlElement(name = "CountService")
    protected long countService;
    @XmlElement(name = "CountServiceData")
    protected long countServiceData;
    @XmlElement(name = "CountServiceVoice")
    protected long countServiceVoice;
    @XmlElement(name = "CountServiceFacility")
    protected long countServiceFacility;

    /**
     * Gets the value of the countService property.
     * 
     */
    public long getCountService() {
        return countService;
    }

    /**
     * Sets the value of the countService property.
     * 
     */
    public void setCountService(long value) {
        this.countService = value;
    }

    /**
     * Gets the value of the countServiceData property.
     * 
     */
    public long getCountServiceData() {
        return countServiceData;
    }

    /**
     * Sets the value of the countServiceData property.
     * 
     */
    public void setCountServiceData(long value) {
        this.countServiceData = value;
    }

    /**
     * Gets the value of the countServiceVoice property.
     * 
     */
    public long getCountServiceVoice() {
        return countServiceVoice;
    }

    /**
     * Sets the value of the countServiceVoice property.
     * 
     */
    public void setCountServiceVoice(long value) {
        this.countServiceVoice = value;
    }

    /**
     * Gets the value of the countServiceFacility property.
     * 
     */
    public long getCountServiceFacility() {
        return countServiceFacility;
    }

    /**
     * Sets the value of the countServiceFacility property.
     * 
     */
    public void setCountServiceFacility(long value) {
        this.countServiceFacility = value;
    }

}
