
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cbsSortBy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="cbsSortBy">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CaseID"/>
 *     &lt;enumeration value="CaseTitle"/>
 *     &lt;enumeration value="CreationTime"/>
 *     &lt;enumeration value="Market"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "cbsSortBy")
@XmlEnum
public enum CbsSortBy {

    @XmlEnumValue("CaseID")
    CASE_ID("CaseID"),
    @XmlEnumValue("CaseTitle")
    CASE_TITLE("CaseTitle"),
    @XmlEnumValue("CreationTime")
    CREATION_TIME("CreationTime"),
    @XmlEnumValue("Market")
    MARKET("Market");
    private final String value;

    CbsSortBy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CbsSortBy fromValue(String v) {
        for (CbsSortBy c: CbsSortBy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
