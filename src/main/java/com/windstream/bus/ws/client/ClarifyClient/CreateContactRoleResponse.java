
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreateContactRoleResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createContactRoleResult"
})
@XmlRootElement(name = "CreateContactRoleResponse")
public class CreateContactRoleResponse {

    @XmlElement(name = "CreateContactRoleResult", required = true)
    protected StandardResponse createContactRoleResult;

    /**
     * Gets the value of the createContactRoleResult property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getCreateContactRoleResult() {
        return createContactRoleResult;
    }

    /**
     * Sets the value of the createContactRoleResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setCreateContactRoleResult(StandardResponse value) {
        this.createContactRoleResult = value;
    }

}
