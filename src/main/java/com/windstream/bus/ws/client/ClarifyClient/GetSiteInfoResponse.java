
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSiteInfoResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}GetSiteInfoResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSiteInfoResult"
})
@XmlRootElement(name = "GetSiteInfoResponse")
public class GetSiteInfoResponse {

    @XmlElement(name = "GetSiteInfoResult", required = true)
    protected GetSiteInfoResp getSiteInfoResult;

    /**
     * Gets the value of the getSiteInfoResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetSiteInfoResp }
     *     
     */
    public GetSiteInfoResp getGetSiteInfoResult() {
        return getSiteInfoResult;
    }

    /**
     * Sets the value of the getSiteInfoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetSiteInfoResp }
     *     
     */
    public void setGetSiteInfoResult(GetSiteInfoResp value) {
        this.getSiteInfoResult = value;
    }

}
