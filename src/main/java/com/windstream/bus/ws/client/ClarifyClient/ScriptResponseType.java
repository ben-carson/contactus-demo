
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScriptResponseType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ScriptResponseType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Text"/>
 *     &lt;enumeration value="Dropdown"/>
 *     &lt;enumeration value="DateTime"/>
 *     &lt;enumeration value="RadioButton"/>
 *     &lt;enumeration value="Checkbox"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ScriptResponseType")
@XmlEnum
public enum ScriptResponseType {

    @XmlEnumValue("Text")
    TEXT("Text"),
    @XmlEnumValue("Dropdown")
    DROPDOWN("Dropdown"),
    @XmlEnumValue("DateTime")
    DATE_TIME("DateTime"),
    @XmlEnumValue("RadioButton")
    RADIO_BUTTON("RadioButton"),
    @XmlEnumValue("Checkbox")
    CHECKBOX("Checkbox");
    private final String value;

    ScriptResponseType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ScriptResponseType fromValue(String v) {
        for (ScriptResponseType c: ScriptResponseType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
