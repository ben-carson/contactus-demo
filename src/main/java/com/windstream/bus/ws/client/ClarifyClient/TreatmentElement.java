
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TreatmentElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TreatmentElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Balance30To60" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Balance61To90" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="BalanceOver90" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="InvoiceAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="CreditClass" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="InvoiceCycle" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MRC" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TreatmentElement", propOrder = {
    "balance30To60",
    "balance61To90",
    "balanceOver90",
    "invoiceAmount",
    "creditClass",
    "invoiceCycle",
    "mrc"
})
public class TreatmentElement {

    @XmlElement(name = "Balance30To60")
    protected double balance30To60;
    @XmlElement(name = "Balance61To90")
    protected double balance61To90;
    @XmlElement(name = "BalanceOver90")
    protected double balanceOver90;
    @XmlElement(name = "InvoiceAmount")
    protected double invoiceAmount;
    @XmlElement(name = "CreditClass")
    protected long creditClass;
    @XmlElement(name = "InvoiceCycle")
    protected int invoiceCycle;
    @XmlElement(name = "MRC")
    protected double mrc;

    /**
     * Gets the value of the balance30To60 property.
     * 
     */
    public double getBalance30To60() {
        return balance30To60;
    }

    /**
     * Sets the value of the balance30To60 property.
     * 
     */
    public void setBalance30To60(double value) {
        this.balance30To60 = value;
    }

    /**
     * Gets the value of the balance61To90 property.
     * 
     */
    public double getBalance61To90() {
        return balance61To90;
    }

    /**
     * Sets the value of the balance61To90 property.
     * 
     */
    public void setBalance61To90(double value) {
        this.balance61To90 = value;
    }

    /**
     * Gets the value of the balanceOver90 property.
     * 
     */
    public double getBalanceOver90() {
        return balanceOver90;
    }

    /**
     * Sets the value of the balanceOver90 property.
     * 
     */
    public void setBalanceOver90(double value) {
        this.balanceOver90 = value;
    }

    /**
     * Gets the value of the invoiceAmount property.
     * 
     */
    public double getInvoiceAmount() {
        return invoiceAmount;
    }

    /**
     * Sets the value of the invoiceAmount property.
     * 
     */
    public void setInvoiceAmount(double value) {
        this.invoiceAmount = value;
    }

    /**
     * Gets the value of the creditClass property.
     * 
     */
    public long getCreditClass() {
        return creditClass;
    }

    /**
     * Sets the value of the creditClass property.
     * 
     */
    public void setCreditClass(long value) {
        this.creditClass = value;
    }

    /**
     * Gets the value of the invoiceCycle property.
     * 
     */
    public int getInvoiceCycle() {
        return invoiceCycle;
    }

    /**
     * Sets the value of the invoiceCycle property.
     * 
     */
    public void setInvoiceCycle(int value) {
        this.invoiceCycle = value;
    }

    /**
     * Gets the value of the mrc property.
     * 
     */
    public double getMRC() {
        return mrc;
    }

    /**
     * Sets the value of the mrc property.
     * 
     */
    public void setMRC(double value) {
        this.mrc = value;
    }

}
