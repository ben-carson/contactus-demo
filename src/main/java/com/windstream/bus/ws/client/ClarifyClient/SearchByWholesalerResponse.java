
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchByWholesalerResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SearchByWholesalerResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchByWholesalerResult"
})
@XmlRootElement(name = "SearchByWholesalerResponse")
public class SearchByWholesalerResponse {

    @XmlElement(name = "SearchByWholesalerResult", required = true)
    protected SearchByWholesalerResp searchByWholesalerResult;

    /**
     * Gets the value of the searchByWholesalerResult property.
     * 
     * @return
     *     possible object is
     *     {@link SearchByWholesalerResp }
     *     
     */
    public SearchByWholesalerResp getSearchByWholesalerResult() {
        return searchByWholesalerResult;
    }

    /**
     * Sets the value of the searchByWholesalerResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchByWholesalerResp }
     *     
     */
    public void setSearchByWholesalerResult(SearchByWholesalerResp value) {
        this.searchByWholesalerResult = value;
    }

}
