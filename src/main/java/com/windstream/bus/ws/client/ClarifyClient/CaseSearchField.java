
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseSearchField.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CaseSearchField">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Objid"/>
 *     &lt;enumeration value="CaseID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CaseSearchField")
@XmlEnum
public enum CaseSearchField {

    @XmlEnumValue("Objid")
    OBJID("Objid"),
    @XmlEnumValue("CaseID")
    CASE_ID("CaseID");
    private final String value;

    CaseSearchField(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CaseSearchField fromValue(String v) {
        for (CaseSearchField c: CaseSearchField.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
