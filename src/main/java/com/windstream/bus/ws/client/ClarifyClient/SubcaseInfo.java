
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubcaseInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubcaseInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubcaseObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="SubcaseID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubcaseTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreationTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="SubcaseType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Closed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ConditionTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OwnerLogin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Queue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CommitDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CommitPriorWarning" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OriginatorLogin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EscalationSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EscalationReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EscalationIssueCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EscalationIssueDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParentCaseID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubcaseInfo", propOrder = {
    "subcaseObjid",
    "subcaseID",
    "subcaseTitle",
    "creationTime",
    "subcaseType",
    "closed",
    "conditionTitle",
    "status",
    "ownerLogin",
    "queue",
    "commitDate",
    "commitPriorWarning",
    "priority",
    "originatorLogin",
    "escalationSource",
    "escalationReason",
    "escalationIssueCode",
    "escalationIssueDesc",
    "parentCaseID"
})
public class SubcaseInfo {

    @XmlElement(name = "SubcaseObjid")
    protected long subcaseObjid;
    @XmlElement(name = "SubcaseID")
    protected String subcaseID;
    @XmlElement(name = "SubcaseTitle")
    protected String subcaseTitle;
    @XmlElement(name = "CreationTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationTime;
    @XmlElement(name = "SubcaseType")
    protected String subcaseType;
    @XmlElement(name = "Closed")
    protected boolean closed;
    @XmlElement(name = "ConditionTitle")
    protected String conditionTitle;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "OwnerLogin")
    protected String ownerLogin;
    @XmlElement(name = "Queue")
    protected String queue;
    @XmlElement(name = "CommitDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar commitDate;
    @XmlElement(name = "CommitPriorWarning", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar commitPriorWarning;
    @XmlElement(name = "Priority")
    protected String priority;
    @XmlElement(name = "OriginatorLogin")
    protected String originatorLogin;
    @XmlElement(name = "EscalationSource")
    protected String escalationSource;
    @XmlElement(name = "EscalationReason")
    protected String escalationReason;
    @XmlElement(name = "EscalationIssueCode")
    protected String escalationIssueCode;
    @XmlElement(name = "EscalationIssueDesc")
    protected String escalationIssueDesc;
    @XmlElement(name = "ParentCaseID")
    protected String parentCaseID;

    /**
     * Gets the value of the subcaseObjid property.
     * 
     */
    public long getSubcaseObjid() {
        return subcaseObjid;
    }

    /**
     * Sets the value of the subcaseObjid property.
     * 
     */
    public void setSubcaseObjid(long value) {
        this.subcaseObjid = value;
    }

    /**
     * Gets the value of the subcaseID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubcaseID() {
        return subcaseID;
    }

    /**
     * Sets the value of the subcaseID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubcaseID(String value) {
        this.subcaseID = value;
    }

    /**
     * Gets the value of the subcaseTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubcaseTitle() {
        return subcaseTitle;
    }

    /**
     * Sets the value of the subcaseTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubcaseTitle(String value) {
        this.subcaseTitle = value;
    }

    /**
     * Gets the value of the creationTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationTime() {
        return creationTime;
    }

    /**
     * Sets the value of the creationTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationTime(XMLGregorianCalendar value) {
        this.creationTime = value;
    }

    /**
     * Gets the value of the subcaseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubcaseType() {
        return subcaseType;
    }

    /**
     * Sets the value of the subcaseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubcaseType(String value) {
        this.subcaseType = value;
    }

    /**
     * Gets the value of the closed property.
     * 
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * Sets the value of the closed property.
     * 
     */
    public void setClosed(boolean value) {
        this.closed = value;
    }

    /**
     * Gets the value of the conditionTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConditionTitle() {
        return conditionTitle;
    }

    /**
     * Sets the value of the conditionTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConditionTitle(String value) {
        this.conditionTitle = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the ownerLogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerLogin() {
        return ownerLogin;
    }

    /**
     * Sets the value of the ownerLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerLogin(String value) {
        this.ownerLogin = value;
    }

    /**
     * Gets the value of the queue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueue() {
        return queue;
    }

    /**
     * Sets the value of the queue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueue(String value) {
        this.queue = value;
    }

    /**
     * Gets the value of the commitDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCommitDate() {
        return commitDate;
    }

    /**
     * Sets the value of the commitDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCommitDate(XMLGregorianCalendar value) {
        this.commitDate = value;
    }

    /**
     * Gets the value of the commitPriorWarning property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCommitPriorWarning() {
        return commitPriorWarning;
    }

    /**
     * Sets the value of the commitPriorWarning property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCommitPriorWarning(XMLGregorianCalendar value) {
        this.commitPriorWarning = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Gets the value of the originatorLogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatorLogin() {
        return originatorLogin;
    }

    /**
     * Sets the value of the originatorLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatorLogin(String value) {
        this.originatorLogin = value;
    }

    /**
     * Gets the value of the escalationSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEscalationSource() {
        return escalationSource;
    }

    /**
     * Sets the value of the escalationSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEscalationSource(String value) {
        this.escalationSource = value;
    }

    /**
     * Gets the value of the escalationReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEscalationReason() {
        return escalationReason;
    }

    /**
     * Sets the value of the escalationReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEscalationReason(String value) {
        this.escalationReason = value;
    }

    /**
     * Gets the value of the escalationIssueCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEscalationIssueCode() {
        return escalationIssueCode;
    }

    /**
     * Sets the value of the escalationIssueCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEscalationIssueCode(String value) {
        this.escalationIssueCode = value;
    }

    /**
     * Gets the value of the escalationIssueDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEscalationIssueDesc() {
        return escalationIssueDesc;
    }

    /**
     * Sets the value of the escalationIssueDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEscalationIssueDesc(String value) {
        this.escalationIssueDesc = value;
    }

    /**
     * Gets the value of the parentCaseID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentCaseID() {
        return parentCaseID;
    }

    /**
     * Sets the value of the parentCaseID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentCaseID(String value) {
        this.parentCaseID = value;
    }

}
