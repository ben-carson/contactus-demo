
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchField" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SiteSearchField"/>
 *         &lt;element name="SearchValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CaseStatus" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}CaseStatus"/>
 *         &lt;element name="WithinLastDays" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SortBy" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}cbsSortBy"/>
 *         &lt;element name="Order" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SortOrder"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchField",
    "searchValue",
    "caseStatus",
    "withinLastDays",
    "sortBy",
    "order"
})
@XmlRootElement(name = "GetCasesBySite")
public class GetCasesBySite {

    @XmlElement(name = "SearchField", required = true)
    protected SiteSearchField searchField;
    @XmlElement(name = "SearchValue")
    protected String searchValue;
    @XmlElement(name = "CaseStatus", required = true, nillable = true)
    protected CaseStatus caseStatus;
    @XmlElement(name = "WithinLastDays")
    protected int withinLastDays;
    @XmlElement(name = "SortBy", required = true, nillable = true)
    protected CbsSortBy sortBy;
    @XmlElement(name = "Order", required = true, nillable = true)
    protected SortOrder order;

    /**
     * Gets the value of the searchField property.
     * 
     * @return
     *     possible object is
     *     {@link SiteSearchField }
     *     
     */
    public SiteSearchField getSearchField() {
        return searchField;
    }

    /**
     * Sets the value of the searchField property.
     * 
     * @param value
     *     allowed object is
     *     {@link SiteSearchField }
     *     
     */
    public void setSearchField(SiteSearchField value) {
        this.searchField = value;
    }

    /**
     * Gets the value of the searchValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchValue() {
        return searchValue;
    }

    /**
     * Sets the value of the searchValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchValue(String value) {
        this.searchValue = value;
    }

    /**
     * Gets the value of the caseStatus property.
     * 
     * @return
     *     possible object is
     *     {@link CaseStatus }
     *     
     */
    public CaseStatus getCaseStatus() {
        return caseStatus;
    }

    /**
     * Sets the value of the caseStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseStatus }
     *     
     */
    public void setCaseStatus(CaseStatus value) {
        this.caseStatus = value;
    }

    /**
     * Gets the value of the withinLastDays property.
     * 
     */
    public int getWithinLastDays() {
        return withinLastDays;
    }

    /**
     * Sets the value of the withinLastDays property.
     * 
     */
    public void setWithinLastDays(int value) {
        this.withinLastDays = value;
    }

    /**
     * Gets the value of the sortBy property.
     * 
     * @return
     *     possible object is
     *     {@link CbsSortBy }
     *     
     */
    public CbsSortBy getSortBy() {
        return sortBy;
    }

    /**
     * Sets the value of the sortBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link CbsSortBy }
     *     
     */
    public void setSortBy(CbsSortBy value) {
        this.sortBy = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link SortOrder }
     *     
     */
    public SortOrder getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortOrder }
     *     
     */
    public void setOrder(SortOrder value) {
        this.order = value;
    }

}
