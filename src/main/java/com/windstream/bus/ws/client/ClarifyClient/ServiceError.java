
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceError complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceError">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServiceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ServiceItemID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ErrorNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceError", propOrder = {
    "serviceID",
    "serviceItemID",
    "errorNumber",
    "errorMessage"
})
public class ServiceError {

    @XmlElement(name = "ServiceID")
    protected long serviceID;
    @XmlElement(name = "ServiceItemID")
    protected long serviceItemID;
    @XmlElement(name = "ErrorNumber")
    protected long errorNumber;
    @XmlElement(name = "ErrorMessage")
    protected String errorMessage;

    /**
     * Gets the value of the serviceID property.
     * 
     */
    public long getServiceID() {
        return serviceID;
    }

    /**
     * Sets the value of the serviceID property.
     * 
     */
    public void setServiceID(long value) {
        this.serviceID = value;
    }

    /**
     * Gets the value of the serviceItemID property.
     * 
     */
    public long getServiceItemID() {
        return serviceItemID;
    }

    /**
     * Sets the value of the serviceItemID property.
     * 
     */
    public void setServiceItemID(long value) {
        this.serviceItemID = value;
    }

    /**
     * Gets the value of the errorNumber property.
     * 
     */
    public long getErrorNumber() {
        return errorNumber;
    }

    /**
     * Sets the value of the errorNumber property.
     * 
     */
    public void setErrorNumber(long value) {
        this.errorNumber = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

}
