
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetWholesalerCasesResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}GetWholesalerCasesResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getWholesalerCasesResult"
})
@XmlRootElement(name = "GetWholesalerCasesResponse")
public class GetWholesalerCasesResponse {

    @XmlElement(name = "GetWholesalerCasesResult", required = true)
    protected GetWholesalerCasesResp getWholesalerCasesResult;

    /**
     * Gets the value of the getWholesalerCasesResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetWholesalerCasesResp }
     *     
     */
    public GetWholesalerCasesResp getGetWholesalerCasesResult() {
        return getWholesalerCasesResult;
    }

    /**
     * Sets the value of the getWholesalerCasesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetWholesalerCasesResp }
     *     
     */
    public void setGetWholesalerCasesResult(GetWholesalerCasesResp value) {
        this.getWholesalerCasesResult = value;
    }

}
