
package com.windstream.bus.ws.client.ClarifyClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSubcaseInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSubcaseInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubcaseInfo" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SubcaseInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSubcaseInfo", propOrder = {
    "subcaseInfo"
})
public class ArrayOfSubcaseInfo {

    @XmlElement(name = "SubcaseInfo")
    protected List<SubcaseInfo> subcaseInfo;

    /**
     * Gets the value of the subcaseInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subcaseInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubcaseInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubcaseInfo }
     * 
     * 
     */
    public List<SubcaseInfo> getSubcaseInfo() {
        if (subcaseInfo == null) {
            subcaseInfo = new ArrayList<SubcaseInfo>();
        }
        return this.subcaseInfo;
    }

}
