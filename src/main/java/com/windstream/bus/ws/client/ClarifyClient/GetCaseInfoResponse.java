
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCaseInfoResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}CaseInfoResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCaseInfoResult"
})
@XmlRootElement(name = "GetCaseInfoResponse")
public class GetCaseInfoResponse {

    @XmlElement(name = "GetCaseInfoResult", required = true)
    protected CaseInfoResp getCaseInfoResult;

    /**
     * Gets the value of the getCaseInfoResult property.
     * 
     * @return
     *     possible object is
     *     {@link CaseInfoResp }
     *     
     */
    public CaseInfoResp getGetCaseInfoResult() {
        return getCaseInfoResult;
    }

    /**
     * Sets the value of the getCaseInfoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseInfoResp }
     *     
     */
    public void setGetCaseInfoResult(CaseInfoResp value) {
        this.getCaseInfoResult = value;
    }

}
