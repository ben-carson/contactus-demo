
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WholesalerCase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WholesalerCase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ProfileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubProfileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CaseObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CaseID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CaseTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreationTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CreatedByLoginName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreatedByFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreatedByLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TroubleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TroubleSubtype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubtypeCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Market" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OwnerLoginName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Queue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConditionTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Closed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ClosedByLoginName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClosedByFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClosedByLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CloseDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CloseResolutionCode1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CloseResolutionCode2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CloseResolutionCode3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CloseResolutionCode4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OpenEscalation" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WholesalerCase", propOrder = {
    "siteID",
    "siteName",
    "accountNo",
    "profileName",
    "subProfileName",
    "caseObjid",
    "caseID",
    "caseTitle",
    "creationTime",
    "createdByLoginName",
    "createdByFirstName",
    "createdByLastName",
    "troubleType",
    "troubleSubtype",
    "subtypeCategory",
    "market",
    "priority",
    "ownerLoginName",
    "queue",
    "conditionTitle",
    "status",
    "closed",
    "closedByLoginName",
    "closedByFirstName",
    "closedByLastName",
    "closeDate",
    "closeResolutionCode1",
    "closeResolutionCode2",
    "closeResolutionCode3",
    "closeResolutionCode4",
    "openEscalation"
})
public class WholesalerCase {

    @XmlElement(name = "SiteID")
    protected String siteID;
    @XmlElement(name = "SiteName")
    protected String siteName;
    @XmlElement(name = "AccountNo")
    protected long accountNo;
    @XmlElement(name = "ProfileName")
    protected String profileName;
    @XmlElement(name = "SubProfileName")
    protected String subProfileName;
    @XmlElement(name = "CaseObjid")
    protected long caseObjid;
    @XmlElement(name = "CaseID")
    protected String caseID;
    @XmlElement(name = "CaseTitle")
    protected String caseTitle;
    @XmlElement(name = "CreationTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationTime;
    @XmlElement(name = "CreatedByLoginName")
    protected String createdByLoginName;
    @XmlElement(name = "CreatedByFirstName")
    protected String createdByFirstName;
    @XmlElement(name = "CreatedByLastName")
    protected String createdByLastName;
    @XmlElement(name = "TroubleType")
    protected String troubleType;
    @XmlElement(name = "TroubleSubtype")
    protected String troubleSubtype;
    @XmlElement(name = "SubtypeCategory")
    protected String subtypeCategory;
    @XmlElement(name = "Market")
    protected String market;
    @XmlElement(name = "Priority")
    protected String priority;
    @XmlElement(name = "OwnerLoginName")
    protected String ownerLoginName;
    @XmlElement(name = "Queue")
    protected String queue;
    @XmlElement(name = "ConditionTitle")
    protected String conditionTitle;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "Closed")
    protected boolean closed;
    @XmlElement(name = "ClosedByLoginName")
    protected String closedByLoginName;
    @XmlElement(name = "ClosedByFirstName")
    protected String closedByFirstName;
    @XmlElement(name = "ClosedByLastName")
    protected String closedByLastName;
    @XmlElement(name = "CloseDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar closeDate;
    @XmlElement(name = "CloseResolutionCode1")
    protected String closeResolutionCode1;
    @XmlElement(name = "CloseResolutionCode2")
    protected String closeResolutionCode2;
    @XmlElement(name = "CloseResolutionCode3")
    protected String closeResolutionCode3;
    @XmlElement(name = "CloseResolutionCode4")
    protected String closeResolutionCode4;
    @XmlElement(name = "OpenEscalation")
    protected long openEscalation;

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteID(String value) {
        this.siteID = value;
    }

    /**
     * Gets the value of the siteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * Sets the value of the siteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteName(String value) {
        this.siteName = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     */
    public long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     */
    public void setAccountNo(long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the profileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileName() {
        return profileName;
    }

    /**
     * Sets the value of the profileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileName(String value) {
        this.profileName = value;
    }

    /**
     * Gets the value of the subProfileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubProfileName() {
        return subProfileName;
    }

    /**
     * Sets the value of the subProfileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubProfileName(String value) {
        this.subProfileName = value;
    }

    /**
     * Gets the value of the caseObjid property.
     * 
     */
    public long getCaseObjid() {
        return caseObjid;
    }

    /**
     * Sets the value of the caseObjid property.
     * 
     */
    public void setCaseObjid(long value) {
        this.caseObjid = value;
    }

    /**
     * Gets the value of the caseID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseID() {
        return caseID;
    }

    /**
     * Sets the value of the caseID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseID(String value) {
        this.caseID = value;
    }

    /**
     * Gets the value of the caseTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTitle() {
        return caseTitle;
    }

    /**
     * Sets the value of the caseTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTitle(String value) {
        this.caseTitle = value;
    }

    /**
     * Gets the value of the creationTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationTime() {
        return creationTime;
    }

    /**
     * Sets the value of the creationTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationTime(XMLGregorianCalendar value) {
        this.creationTime = value;
    }

    /**
     * Gets the value of the createdByLoginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedByLoginName() {
        return createdByLoginName;
    }

    /**
     * Sets the value of the createdByLoginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedByLoginName(String value) {
        this.createdByLoginName = value;
    }

    /**
     * Gets the value of the createdByFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedByFirstName() {
        return createdByFirstName;
    }

    /**
     * Sets the value of the createdByFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedByFirstName(String value) {
        this.createdByFirstName = value;
    }

    /**
     * Gets the value of the createdByLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedByLastName() {
        return createdByLastName;
    }

    /**
     * Sets the value of the createdByLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedByLastName(String value) {
        this.createdByLastName = value;
    }

    /**
     * Gets the value of the troubleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTroubleType() {
        return troubleType;
    }

    /**
     * Sets the value of the troubleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTroubleType(String value) {
        this.troubleType = value;
    }

    /**
     * Gets the value of the troubleSubtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTroubleSubtype() {
        return troubleSubtype;
    }

    /**
     * Sets the value of the troubleSubtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTroubleSubtype(String value) {
        this.troubleSubtype = value;
    }

    /**
     * Gets the value of the subtypeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubtypeCategory() {
        return subtypeCategory;
    }

    /**
     * Sets the value of the subtypeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubtypeCategory(String value) {
        this.subtypeCategory = value;
    }

    /**
     * Gets the value of the market property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarket() {
        return market;
    }

    /**
     * Sets the value of the market property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarket(String value) {
        this.market = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Gets the value of the ownerLoginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerLoginName() {
        return ownerLoginName;
    }

    /**
     * Sets the value of the ownerLoginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerLoginName(String value) {
        this.ownerLoginName = value;
    }

    /**
     * Gets the value of the queue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueue() {
        return queue;
    }

    /**
     * Sets the value of the queue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueue(String value) {
        this.queue = value;
    }

    /**
     * Gets the value of the conditionTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConditionTitle() {
        return conditionTitle;
    }

    /**
     * Sets the value of the conditionTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConditionTitle(String value) {
        this.conditionTitle = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the closed property.
     * 
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * Sets the value of the closed property.
     * 
     */
    public void setClosed(boolean value) {
        this.closed = value;
    }

    /**
     * Gets the value of the closedByLoginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosedByLoginName() {
        return closedByLoginName;
    }

    /**
     * Sets the value of the closedByLoginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosedByLoginName(String value) {
        this.closedByLoginName = value;
    }

    /**
     * Gets the value of the closedByFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosedByFirstName() {
        return closedByFirstName;
    }

    /**
     * Sets the value of the closedByFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosedByFirstName(String value) {
        this.closedByFirstName = value;
    }

    /**
     * Gets the value of the closedByLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosedByLastName() {
        return closedByLastName;
    }

    /**
     * Sets the value of the closedByLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosedByLastName(String value) {
        this.closedByLastName = value;
    }

    /**
     * Gets the value of the closeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCloseDate() {
        return closeDate;
    }

    /**
     * Sets the value of the closeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCloseDate(XMLGregorianCalendar value) {
        this.closeDate = value;
    }

    /**
     * Gets the value of the closeResolutionCode1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseResolutionCode1() {
        return closeResolutionCode1;
    }

    /**
     * Sets the value of the closeResolutionCode1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseResolutionCode1(String value) {
        this.closeResolutionCode1 = value;
    }

    /**
     * Gets the value of the closeResolutionCode2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseResolutionCode2() {
        return closeResolutionCode2;
    }

    /**
     * Sets the value of the closeResolutionCode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseResolutionCode2(String value) {
        this.closeResolutionCode2 = value;
    }

    /**
     * Gets the value of the closeResolutionCode3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseResolutionCode3() {
        return closeResolutionCode3;
    }

    /**
     * Sets the value of the closeResolutionCode3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseResolutionCode3(String value) {
        this.closeResolutionCode3 = value;
    }

    /**
     * Gets the value of the closeResolutionCode4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseResolutionCode4() {
        return closeResolutionCode4;
    }

    /**
     * Sets the value of the closeResolutionCode4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseResolutionCode4(String value) {
        this.closeResolutionCode4 = value;
    }

    /**
     * Gets the value of the openEscalation property.
     * 
     */
    public long getOpenEscalation() {
        return openEscalation;
    }

    /**
     * Sets the value of the openEscalation property.
     * 
     */
    public void setOpenEscalation(long value) {
        this.openEscalation = value;
    }

}
