
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCaseSubcasesResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SubcaseResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCaseSubcasesResult"
})
@XmlRootElement(name = "GetCaseSubcasesResponse")
public class GetCaseSubcasesResponse {

    @XmlElement(name = "GetCaseSubcasesResult", required = true)
    protected SubcaseResp getCaseSubcasesResult;

    /**
     * Gets the value of the getCaseSubcasesResult property.
     * 
     * @return
     *     possible object is
     *     {@link SubcaseResp }
     *     
     */
    public SubcaseResp getGetCaseSubcasesResult() {
        return getCaseSubcasesResult;
    }

    /**
     * Sets the value of the getCaseSubcasesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubcaseResp }
     *     
     */
    public void setGetCaseSubcasesResult(SubcaseResp value) {
        this.getCaseSubcasesResult = value;
    }

}
