
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubcaseResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubcaseResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StandardResponse" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *         &lt;element name="subcases" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ArrayOfSubcaseInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubcaseResp", propOrder = {
    "standardResponse",
    "subcases"
})
public class SubcaseResp {

    @XmlElement(name = "StandardResponse", required = true)
    protected StandardResponse standardResponse;
    protected ArrayOfSubcaseInfo subcases;

    /**
     * Gets the value of the standardResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getStandardResponse() {
        return standardResponse;
    }

    /**
     * Sets the value of the standardResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setStandardResponse(StandardResponse value) {
        this.standardResponse = value;
    }

    /**
     * Gets the value of the subcases property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubcaseInfo }
     *     
     */
    public ArrayOfSubcaseInfo getSubcases() {
        return subcases;
    }

    /**
     * Sets the value of the subcases property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubcaseInfo }
     *     
     */
    public void setSubcases(ArrayOfSubcaseInfo value) {
        this.subcases = value;
    }

}
