
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetSiteInfoResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSiteInfoResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StandardResponse" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *         &lt;element name="SiteInfo" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SiteInformation"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSiteInfoResp", propOrder = {
    "standardResponse",
    "siteInfo"
})
public class GetSiteInfoResp {

    @XmlElement(name = "StandardResponse", required = true)
    protected StandardResponse standardResponse;
    @XmlElement(name = "SiteInfo", required = true)
    protected SiteInformation siteInfo;

    /**
     * Gets the value of the standardResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getStandardResponse() {
        return standardResponse;
    }

    /**
     * Sets the value of the standardResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setStandardResponse(StandardResponse value) {
        this.standardResponse = value;
    }

    /**
     * Gets the value of the siteInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SiteInformation }
     *     
     */
    public SiteInformation getSiteInfo() {
        return siteInfo;
    }

    /**
     * Sets the value of the siteInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SiteInformation }
     *     
     */
    public void setSiteInfo(SiteInformation value) {
        this.siteInfo = value;
    }

}
