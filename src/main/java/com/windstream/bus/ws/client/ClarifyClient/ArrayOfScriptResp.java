
package com.windstream.bus.ws.client.ClarifyClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfScriptResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfScriptResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ScriptResp" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ScriptResp" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfScriptResp", propOrder = {
    "scriptResp"
})
public class ArrayOfScriptResp {

    @XmlElement(name = "ScriptResp")
    protected List<ScriptResp> scriptResp;

    /**
     * Gets the value of the scriptResp property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scriptResp property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScriptResp().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScriptResp }
     * 
     * 
     */
    public List<ScriptResp> getScriptResp() {
        if (scriptResp == null) {
            scriptResp = new ArrayList<ScriptResp>();
        }
        return this.scriptResp;
    }

}
