
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ExtendedCaseAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtendedCaseAttribute">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TitleLocked" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ReporterEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendSurvey" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SrvcInstallDt" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="MSSDocno" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="Application" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DispatchCommitTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RIORequestNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RIODispatchDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtendedCaseAttribute", propOrder = {
    "titleLocked",
    "reporterEmail",
    "sendSurvey",
    "srvcInstallDt",
    "mssDocno",
    "application",
    "dispatchCommitTime",
    "rioRequestNo",
    "rioDispatchDate"
})
public class ExtendedCaseAttribute {

    @XmlElement(name = "TitleLocked")
    protected boolean titleLocked;
    @XmlElement(name = "ReporterEmail")
    protected String reporterEmail;
    @XmlElement(name = "SendSurvey")
    protected boolean sendSurvey;
    @XmlElement(name = "SrvcInstallDt", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar srvcInstallDt;
    @XmlElement(name = "MSSDocno")
    protected long mssDocno;
    @XmlElement(name = "Application")
    protected String application;
    @XmlElement(name = "DispatchCommitTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dispatchCommitTime;
    @XmlElement(name = "RIORequestNo")
    protected String rioRequestNo;
    @XmlElement(name = "RIODispatchDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rioDispatchDate;

    /**
     * Gets the value of the titleLocked property.
     * 
     */
    public boolean isTitleLocked() {
        return titleLocked;
    }

    /**
     * Sets the value of the titleLocked property.
     * 
     */
    public void setTitleLocked(boolean value) {
        this.titleLocked = value;
    }

    /**
     * Gets the value of the reporterEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporterEmail() {
        return reporterEmail;
    }

    /**
     * Sets the value of the reporterEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporterEmail(String value) {
        this.reporterEmail = value;
    }

    /**
     * Gets the value of the sendSurvey property.
     * 
     */
    public boolean isSendSurvey() {
        return sendSurvey;
    }

    /**
     * Sets the value of the sendSurvey property.
     * 
     */
    public void setSendSurvey(boolean value) {
        this.sendSurvey = value;
    }

    /**
     * Gets the value of the srvcInstallDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSrvcInstallDt() {
        return srvcInstallDt;
    }

    /**
     * Sets the value of the srvcInstallDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSrvcInstallDt(XMLGregorianCalendar value) {
        this.srvcInstallDt = value;
    }

    /**
     * Gets the value of the mssDocno property.
     * 
     */
    public long getMSSDocno() {
        return mssDocno;
    }

    /**
     * Sets the value of the mssDocno property.
     * 
     */
    public void setMSSDocno(long value) {
        this.mssDocno = value;
    }

    /**
     * Gets the value of the application property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplication() {
        return application;
    }

    /**
     * Sets the value of the application property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplication(String value) {
        this.application = value;
    }

    /**
     * Gets the value of the dispatchCommitTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDispatchCommitTime() {
        return dispatchCommitTime;
    }

    /**
     * Sets the value of the dispatchCommitTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDispatchCommitTime(XMLGregorianCalendar value) {
        this.dispatchCommitTime = value;
    }

    /**
     * Gets the value of the rioRequestNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRIORequestNo() {
        return rioRequestNo;
    }

    /**
     * Sets the value of the rioRequestNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRIORequestNo(String value) {
        this.rioRequestNo = value;
    }

    /**
     * Gets the value of the rioDispatchDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRIODispatchDate() {
        return rioDispatchDate;
    }

    /**
     * Sets the value of the rioDispatchDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRIODispatchDate(XMLGregorianCalendar value) {
        this.rioDispatchDate = value;
    }

}
