
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScriptQuestion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScriptQuestion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QuestionObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="SequenceNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="QuestionText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResponseType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsLastQuestion" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Response" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ArrayOfScriptResp" minOccurs="0"/>
 *         &lt;element name="TroubleTypes" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}TroubleTypes"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScriptQuestion", propOrder = {
    "questionObjid",
    "sequenceNo",
    "questionText",
    "responseType",
    "isRequired",
    "isLastQuestion",
    "response",
    "troubleTypes"
})
public class ScriptQuestion {

    @XmlElement(name = "QuestionObjid")
    protected long questionObjid;
    @XmlElement(name = "SequenceNo")
    protected long sequenceNo;
    @XmlElement(name = "QuestionText")
    protected String questionText;
    @XmlElement(name = "ResponseType")
    protected String responseType;
    @XmlElement(name = "IsRequired")
    protected boolean isRequired;
    @XmlElement(name = "IsLastQuestion")
    protected boolean isLastQuestion;
    @XmlElement(name = "Response")
    protected ArrayOfScriptResp response;
    @XmlElement(name = "TroubleTypes", required = true, nillable = true)
    protected TroubleTypes troubleTypes;

    /**
     * Gets the value of the questionObjid property.
     * 
     */
    public long getQuestionObjid() {
        return questionObjid;
    }

    /**
     * Sets the value of the questionObjid property.
     * 
     */
    public void setQuestionObjid(long value) {
        this.questionObjid = value;
    }

    /**
     * Gets the value of the sequenceNo property.
     * 
     */
    public long getSequenceNo() {
        return sequenceNo;
    }

    /**
     * Sets the value of the sequenceNo property.
     * 
     */
    public void setSequenceNo(long value) {
        this.sequenceNo = value;
    }

    /**
     * Gets the value of the questionText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionText() {
        return questionText;
    }

    /**
     * Sets the value of the questionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionText(String value) {
        this.questionText = value;
    }

    /**
     * Gets the value of the responseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseType() {
        return responseType;
    }

    /**
     * Sets the value of the responseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseType(String value) {
        this.responseType = value;
    }

    /**
     * Gets the value of the isRequired property.
     * 
     */
    public boolean isIsRequired() {
        return isRequired;
    }

    /**
     * Sets the value of the isRequired property.
     * 
     */
    public void setIsRequired(boolean value) {
        this.isRequired = value;
    }

    /**
     * Gets the value of the isLastQuestion property.
     * 
     */
    public boolean isIsLastQuestion() {
        return isLastQuestion;
    }

    /**
     * Sets the value of the isLastQuestion property.
     * 
     */
    public void setIsLastQuestion(boolean value) {
        this.isLastQuestion = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfScriptResp }
     *     
     */
    public ArrayOfScriptResp getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfScriptResp }
     *     
     */
    public void setResponse(ArrayOfScriptResp value) {
        this.response = value;
    }

    /**
     * Gets the value of the troubleTypes property.
     * 
     * @return
     *     possible object is
     *     {@link TroubleTypes }
     *     
     */
    public TroubleTypes getTroubleTypes() {
        return troubleTypes;
    }

    /**
     * Sets the value of the troubleTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link TroubleTypes }
     *     
     */
    public void setTroubleTypes(TroubleTypes value) {
        this.troubleTypes = value;
    }

}
