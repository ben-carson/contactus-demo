
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetScriptQuestionResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ScriptQuestionResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getScriptQuestionResult"
})
@XmlRootElement(name = "GetScriptQuestionResponse")
public class GetScriptQuestionResponse {

    @XmlElement(name = "GetScriptQuestionResult", required = true)
    protected ScriptQuestionResp getScriptQuestionResult;

    /**
     * Gets the value of the getScriptQuestionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ScriptQuestionResp }
     *     
     */
    public ScriptQuestionResp getGetScriptQuestionResult() {
        return getScriptQuestionResult;
    }

    /**
     * Sets the value of the getScriptQuestionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScriptQuestionResp }
     *     
     */
    public void setGetScriptQuestionResult(ScriptQuestionResp value) {
        this.getScriptQuestionResult = value;
    }

}
