
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CasesInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CasesInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CaseObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CaseID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CaseTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreationTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TroubleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TroubleSubtype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubtypeCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Market" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HangupTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ModifyStmp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="IsParentCase" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Keywords" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LecCktID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NvxCktID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CaseStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CasePriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CaseSeverity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CaseType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Condition" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ConditionTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrQueueTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrevQueueTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteObjid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactObjid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OwnerLoginName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrigLoginName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WipName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExtendedCaseAttribute" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ExtendedCaseAttribute"/>
 *         &lt;element name="TreatmentInformation" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}TreatmentInfo"/>
 *         &lt;element name="AlternateContactInformation" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}AlternateContactInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CasesInfo", propOrder = {
    "caseObjid",
    "caseID",
    "caseTitle",
    "creationTime",
    "troubleType",
    "troubleSubtype",
    "subtypeCategory",
    "market",
    "hangupTime",
    "modifyStmp",
    "isParentCase",
    "keywords",
    "phoneNum",
    "lecCktID",
    "nvxCktID",
    "caseStatus",
    "casePriority",
    "caseSeverity",
    "caseType",
    "condition",
    "conditionTitle",
    "currQueueTitle",
    "prevQueueTitle",
    "siteObjid",
    "siteID",
    "siteName",
    "contactObjid",
    "contactFirstName",
    "contactLastName",
    "contactPhone",
    "ownerLoginName",
    "origLoginName",
    "wipName",
    "extendedCaseAttribute",
    "treatmentInformation",
    "alternateContactInformation"
})
public class CasesInfo {

    @XmlElement(name = "CaseObjid")
    protected long caseObjid;
    @XmlElement(name = "CaseID")
    protected String caseID;
    @XmlElement(name = "CaseTitle")
    protected String caseTitle;
    @XmlElement(name = "CreationTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationTime;
    @XmlElement(name = "TroubleType")
    protected String troubleType;
    @XmlElement(name = "TroubleSubtype")
    protected String troubleSubtype;
    @XmlElement(name = "SubtypeCategory")
    protected String subtypeCategory;
    @XmlElement(name = "Market")
    protected String market;
    @XmlElement(name = "HangupTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar hangupTime;
    @XmlElement(name = "ModifyStmp", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modifyStmp;
    @XmlElement(name = "IsParentCase")
    protected boolean isParentCase;
    @XmlElement(name = "Keywords")
    protected String keywords;
    @XmlElement(name = "PhoneNum")
    protected String phoneNum;
    @XmlElement(name = "LecCktID")
    protected String lecCktID;
    @XmlElement(name = "NvxCktID")
    protected String nvxCktID;
    @XmlElement(name = "CaseStatus")
    protected String caseStatus;
    @XmlElement(name = "CasePriority")
    protected String casePriority;
    @XmlElement(name = "CaseSeverity")
    protected String caseSeverity;
    @XmlElement(name = "CaseType")
    protected String caseType;
    @XmlElement(name = "Condition")
    protected long condition;
    @XmlElement(name = "ConditionTitle")
    protected String conditionTitle;
    @XmlElement(name = "CurrQueueTitle")
    protected String currQueueTitle;
    @XmlElement(name = "PrevQueueTitle")
    protected String prevQueueTitle;
    @XmlElement(name = "SiteObjid")
    protected String siteObjid;
    @XmlElement(name = "SiteID")
    protected String siteID;
    @XmlElement(name = "SiteName")
    protected String siteName;
    @XmlElement(name = "ContactObjid")
    protected String contactObjid;
    @XmlElement(name = "ContactFirstName")
    protected String contactFirstName;
    @XmlElement(name = "ContactLastName")
    protected String contactLastName;
    @XmlElement(name = "ContactPhone")
    protected String contactPhone;
    @XmlElement(name = "OwnerLoginName")
    protected String ownerLoginName;
    @XmlElement(name = "OrigLoginName")
    protected String origLoginName;
    @XmlElement(name = "WipName")
    protected String wipName;
    @XmlElement(name = "ExtendedCaseAttribute", required = true)
    protected ExtendedCaseAttribute extendedCaseAttribute;
    @XmlElement(name = "TreatmentInformation", required = true)
    protected TreatmentInfo treatmentInformation;
    @XmlElement(name = "AlternateContactInformation", required = true)
    protected AlternateContactInfo alternateContactInformation;

    /**
     * Gets the value of the caseObjid property.
     * 
     */
    public long getCaseObjid() {
        return caseObjid;
    }

    /**
     * Sets the value of the caseObjid property.
     * 
     */
    public void setCaseObjid(long value) {
        this.caseObjid = value;
    }

    /**
     * Gets the value of the caseID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseID() {
        return caseID;
    }

    /**
     * Sets the value of the caseID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseID(String value) {
        this.caseID = value;
    }

    /**
     * Gets the value of the caseTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTitle() {
        return caseTitle;
    }

    /**
     * Sets the value of the caseTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTitle(String value) {
        this.caseTitle = value;
    }

    /**
     * Gets the value of the creationTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationTime() {
        return creationTime;
    }

    /**
     * Sets the value of the creationTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationTime(XMLGregorianCalendar value) {
        this.creationTime = value;
    }

    /**
     * Gets the value of the troubleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTroubleType() {
        return troubleType;
    }

    /**
     * Sets the value of the troubleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTroubleType(String value) {
        this.troubleType = value;
    }

    /**
     * Gets the value of the troubleSubtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTroubleSubtype() {
        return troubleSubtype;
    }

    /**
     * Sets the value of the troubleSubtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTroubleSubtype(String value) {
        this.troubleSubtype = value;
    }

    /**
     * Gets the value of the subtypeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubtypeCategory() {
        return subtypeCategory;
    }

    /**
     * Sets the value of the subtypeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubtypeCategory(String value) {
        this.subtypeCategory = value;
    }

    /**
     * Gets the value of the market property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarket() {
        return market;
    }

    /**
     * Sets the value of the market property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarket(String value) {
        this.market = value;
    }

    /**
     * Gets the value of the hangupTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHangupTime() {
        return hangupTime;
    }

    /**
     * Sets the value of the hangupTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHangupTime(XMLGregorianCalendar value) {
        this.hangupTime = value;
    }

    /**
     * Gets the value of the modifyStmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModifyStmp() {
        return modifyStmp;
    }

    /**
     * Sets the value of the modifyStmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModifyStmp(XMLGregorianCalendar value) {
        this.modifyStmp = value;
    }

    /**
     * Gets the value of the isParentCase property.
     * 
     */
    public boolean isIsParentCase() {
        return isParentCase;
    }

    /**
     * Sets the value of the isParentCase property.
     * 
     */
    public void setIsParentCase(boolean value) {
        this.isParentCase = value;
    }

    /**
     * Gets the value of the keywords property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * Sets the value of the keywords property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeywords(String value) {
        this.keywords = value;
    }

    /**
     * Gets the value of the phoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNum() {
        return phoneNum;
    }

    /**
     * Sets the value of the phoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNum(String value) {
        this.phoneNum = value;
    }

    /**
     * Gets the value of the lecCktID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLecCktID() {
        return lecCktID;
    }

    /**
     * Sets the value of the lecCktID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLecCktID(String value) {
        this.lecCktID = value;
    }

    /**
     * Gets the value of the nvxCktID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNvxCktID() {
        return nvxCktID;
    }

    /**
     * Sets the value of the nvxCktID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNvxCktID(String value) {
        this.nvxCktID = value;
    }

    /**
     * Gets the value of the caseStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseStatus() {
        return caseStatus;
    }

    /**
     * Sets the value of the caseStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseStatus(String value) {
        this.caseStatus = value;
    }

    /**
     * Gets the value of the casePriority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCasePriority() {
        return casePriority;
    }

    /**
     * Sets the value of the casePriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCasePriority(String value) {
        this.casePriority = value;
    }

    /**
     * Gets the value of the caseSeverity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseSeverity() {
        return caseSeverity;
    }

    /**
     * Sets the value of the caseSeverity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseSeverity(String value) {
        this.caseSeverity = value;
    }

    /**
     * Gets the value of the caseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseType() {
        return caseType;
    }

    /**
     * Sets the value of the caseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseType(String value) {
        this.caseType = value;
    }

    /**
     * Gets the value of the condition property.
     * 
     */
    public long getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     */
    public void setCondition(long value) {
        this.condition = value;
    }

    /**
     * Gets the value of the conditionTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConditionTitle() {
        return conditionTitle;
    }

    /**
     * Sets the value of the conditionTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConditionTitle(String value) {
        this.conditionTitle = value;
    }

    /**
     * Gets the value of the currQueueTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrQueueTitle() {
        return currQueueTitle;
    }

    /**
     * Sets the value of the currQueueTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrQueueTitle(String value) {
        this.currQueueTitle = value;
    }

    /**
     * Gets the value of the prevQueueTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevQueueTitle() {
        return prevQueueTitle;
    }

    /**
     * Sets the value of the prevQueueTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevQueueTitle(String value) {
        this.prevQueueTitle = value;
    }

    /**
     * Gets the value of the siteObjid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteObjid() {
        return siteObjid;
    }

    /**
     * Sets the value of the siteObjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteObjid(String value) {
        this.siteObjid = value;
    }

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteID(String value) {
        this.siteID = value;
    }

    /**
     * Gets the value of the siteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * Sets the value of the siteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteName(String value) {
        this.siteName = value;
    }

    /**
     * Gets the value of the contactObjid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactObjid() {
        return contactObjid;
    }

    /**
     * Sets the value of the contactObjid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactObjid(String value) {
        this.contactObjid = value;
    }

    /**
     * Gets the value of the contactFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactFirstName() {
        return contactFirstName;
    }

    /**
     * Sets the value of the contactFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactFirstName(String value) {
        this.contactFirstName = value;
    }

    /**
     * Gets the value of the contactLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactLastName() {
        return contactLastName;
    }

    /**
     * Sets the value of the contactLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactLastName(String value) {
        this.contactLastName = value;
    }

    /**
     * Gets the value of the contactPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * Sets the value of the contactPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPhone(String value) {
        this.contactPhone = value;
    }

    /**
     * Gets the value of the ownerLoginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerLoginName() {
        return ownerLoginName;
    }

    /**
     * Sets the value of the ownerLoginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerLoginName(String value) {
        this.ownerLoginName = value;
    }

    /**
     * Gets the value of the origLoginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigLoginName() {
        return origLoginName;
    }

    /**
     * Sets the value of the origLoginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigLoginName(String value) {
        this.origLoginName = value;
    }

    /**
     * Gets the value of the wipName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWipName() {
        return wipName;
    }

    /**
     * Sets the value of the wipName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWipName(String value) {
        this.wipName = value;
    }

    /**
     * Gets the value of the extendedCaseAttribute property.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedCaseAttribute }
     *     
     */
    public ExtendedCaseAttribute getExtendedCaseAttribute() {
        return extendedCaseAttribute;
    }

    /**
     * Sets the value of the extendedCaseAttribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedCaseAttribute }
     *     
     */
    public void setExtendedCaseAttribute(ExtendedCaseAttribute value) {
        this.extendedCaseAttribute = value;
    }

    /**
     * Gets the value of the treatmentInformation property.
     * 
     * @return
     *     possible object is
     *     {@link TreatmentInfo }
     *     
     */
    public TreatmentInfo getTreatmentInformation() {
        return treatmentInformation;
    }

    /**
     * Sets the value of the treatmentInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link TreatmentInfo }
     *     
     */
    public void setTreatmentInformation(TreatmentInfo value) {
        this.treatmentInformation = value;
    }

    /**
     * Gets the value of the alternateContactInformation property.
     * 
     * @return
     *     possible object is
     *     {@link AlternateContactInfo }
     *     
     */
    public AlternateContactInfo getAlternateContactInformation() {
        return alternateContactInformation;
    }

    /**
     * Sets the value of the alternateContactInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateContactInfo }
     *     
     */
    public void setAlternateContactInformation(AlternateContactInfo value) {
        this.alternateContactInformation = value;
    }

}
