
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WholesalerId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="WholesalerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="AccountName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CaseStatus" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}CaseStatus"/>
 *         &lt;element name="CreatedWithinLastDays" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ClosedWithinLastDays" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wholesalerId",
    "wholesalerName",
    "accountNo",
    "accountName",
    "caseStatus",
    "createdWithinLastDays",
    "closedWithinLastDays"
})
@XmlRootElement(name = "GetWholesalerCases")
public class GetWholesalerCases {

    @XmlElement(name = "WholesalerId")
    protected long wholesalerId;
    @XmlElement(name = "WholesalerName")
    protected String wholesalerName;
    @XmlElement(name = "AccountNo")
    protected long accountNo;
    @XmlElement(name = "AccountName")
    protected String accountName;
    @XmlElement(name = "CaseStatus", required = true, nillable = true)
    protected CaseStatus caseStatus;
    @XmlElement(name = "CreatedWithinLastDays")
    protected int createdWithinLastDays;
    @XmlElement(name = "ClosedWithinLastDays")
    protected int closedWithinLastDays;

    /**
     * Gets the value of the wholesalerId property.
     * 
     */
    public long getWholesalerId() {
        return wholesalerId;
    }

    /**
     * Sets the value of the wholesalerId property.
     * 
     */
    public void setWholesalerId(long value) {
        this.wholesalerId = value;
    }

    /**
     * Gets the value of the wholesalerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWholesalerName() {
        return wholesalerName;
    }

    /**
     * Sets the value of the wholesalerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWholesalerName(String value) {
        this.wholesalerName = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     */
    public long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     */
    public void setAccountNo(long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the accountName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Sets the value of the accountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountName(String value) {
        this.accountName = value;
    }

    /**
     * Gets the value of the caseStatus property.
     * 
     * @return
     *     possible object is
     *     {@link CaseStatus }
     *     
     */
    public CaseStatus getCaseStatus() {
        return caseStatus;
    }

    /**
     * Sets the value of the caseStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseStatus }
     *     
     */
    public void setCaseStatus(CaseStatus value) {
        this.caseStatus = value;
    }

    /**
     * Gets the value of the createdWithinLastDays property.
     * 
     */
    public int getCreatedWithinLastDays() {
        return createdWithinLastDays;
    }

    /**
     * Sets the value of the createdWithinLastDays property.
     * 
     */
    public void setCreatedWithinLastDays(int value) {
        this.createdWithinLastDays = value;
    }

    /**
     * Gets the value of the closedWithinLastDays property.
     * 
     */
    public int getClosedWithinLastDays() {
        return closedWithinLastDays;
    }

    /**
     * Sets the value of the closedWithinLastDays property.
     * 
     */
    public void setClosedWithinLastDays(int value) {
        this.closedWithinLastDays = value;
    }

}
