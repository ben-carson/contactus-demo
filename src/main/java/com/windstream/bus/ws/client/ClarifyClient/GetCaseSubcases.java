
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchField" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}CaseSearchField"/>
 *         &lt;element name="SearchValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}CaseStatus"/>
 *         &lt;element name="SubcaseType" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SubcaseTypes"/>
 *         &lt;element name="WithinLastDays" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="SortBy" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}cscSortBy"/>
 *         &lt;element name="Order" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SortOrder"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchField",
    "searchValue",
    "status",
    "subcaseType",
    "withinLastDays",
    "sortBy",
    "order"
})
@XmlRootElement(name = "GetCaseSubcases")
public class GetCaseSubcases {

    @XmlElement(name = "SearchField", required = true)
    protected CaseSearchField searchField;
    @XmlElement(name = "SearchValue")
    protected String searchValue;
    @XmlElement(name = "Status", required = true, nillable = true)
    protected CaseStatus status;
    @XmlElement(name = "SubcaseType", required = true, nillable = true)
    protected SubcaseTypes subcaseType;
    @XmlElement(name = "WithinLastDays", required = true, type = Long.class, nillable = true)
    protected Long withinLastDays;
    @XmlElement(name = "SortBy", required = true, nillable = true)
    protected CscSortBy sortBy;
    @XmlElement(name = "Order", required = true, nillable = true)
    protected SortOrder order;

    /**
     * Gets the value of the searchField property.
     * 
     * @return
     *     possible object is
     *     {@link CaseSearchField }
     *     
     */
    public CaseSearchField getSearchField() {
        return searchField;
    }

    /**
     * Sets the value of the searchField property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseSearchField }
     *     
     */
    public void setSearchField(CaseSearchField value) {
        this.searchField = value;
    }

    /**
     * Gets the value of the searchValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchValue() {
        return searchValue;
    }

    /**
     * Sets the value of the searchValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchValue(String value) {
        this.searchValue = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link CaseStatus }
     *     
     */
    public CaseStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseStatus }
     *     
     */
    public void setStatus(CaseStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the subcaseType property.
     * 
     * @return
     *     possible object is
     *     {@link SubcaseTypes }
     *     
     */
    public SubcaseTypes getSubcaseType() {
        return subcaseType;
    }

    /**
     * Sets the value of the subcaseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubcaseTypes }
     *     
     */
    public void setSubcaseType(SubcaseTypes value) {
        this.subcaseType = value;
    }

    /**
     * Gets the value of the withinLastDays property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getWithinLastDays() {
        return withinLastDays;
    }

    /**
     * Sets the value of the withinLastDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setWithinLastDays(Long value) {
        this.withinLastDays = value;
    }

    /**
     * Gets the value of the sortBy property.
     * 
     * @return
     *     possible object is
     *     {@link CscSortBy }
     *     
     */
    public CscSortBy getSortBy() {
        return sortBy;
    }

    /**
     * Sets the value of the sortBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link CscSortBy }
     *     
     */
    public void setSortBy(CscSortBy value) {
        this.sortBy = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link SortOrder }
     *     
     */
    public SortOrder getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortOrder }
     *     
     */
    public void setOrder(SortOrder value) {
        this.order = value;
    }

}
