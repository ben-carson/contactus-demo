
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContactObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="NewFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NewLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NewPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NewPrimaryRoleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Salutation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AfterHoursNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MailStop" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SiteStatus"/>
 *         &lt;element name="NetworkLogin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "contactObjid",
    "newFirstName",
    "newLastName",
    "newPhone",
    "newPrimaryRoleName",
    "salutation",
    "title",
    "fax",
    "email",
    "mobileNumber",
    "afterHoursNumber",
    "mailStop",
    "status",
    "networkLogin"
})
@XmlRootElement(name = "UpdateContact")
public class UpdateContact {

    @XmlElement(name = "ContactObjid")
    protected long contactObjid;
    @XmlElement(name = "NewFirstName")
    protected String newFirstName;
    @XmlElement(name = "NewLastName")
    protected String newLastName;
    @XmlElement(name = "NewPhone")
    protected String newPhone;
    @XmlElement(name = "NewPrimaryRoleName")
    protected String newPrimaryRoleName;
    @XmlElement(name = "Salutation")
    protected String salutation;
    @XmlElement(name = "Title")
    protected String title;
    @XmlElement(name = "Fax")
    protected String fax;
    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "MobileNumber")
    protected String mobileNumber;
    @XmlElement(name = "AfterHoursNumber")
    protected String afterHoursNumber;
    @XmlElement(name = "MailStop")
    protected String mailStop;
    @XmlElement(name = "Status", required = true, nillable = true)
    protected SiteStatus status;
    @XmlElement(name = "NetworkLogin")
    protected String networkLogin;

    /**
     * Gets the value of the contactObjid property.
     * 
     */
    public long getContactObjid() {
        return contactObjid;
    }

    /**
     * Sets the value of the contactObjid property.
     * 
     */
    public void setContactObjid(long value) {
        this.contactObjid = value;
    }

    /**
     * Gets the value of the newFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewFirstName() {
        return newFirstName;
    }

    /**
     * Sets the value of the newFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewFirstName(String value) {
        this.newFirstName = value;
    }

    /**
     * Gets the value of the newLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewLastName() {
        return newLastName;
    }

    /**
     * Sets the value of the newLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewLastName(String value) {
        this.newLastName = value;
    }

    /**
     * Gets the value of the newPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewPhone() {
        return newPhone;
    }

    /**
     * Sets the value of the newPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewPhone(String value) {
        this.newPhone = value;
    }

    /**
     * Gets the value of the newPrimaryRoleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewPrimaryRoleName() {
        return newPrimaryRoleName;
    }

    /**
     * Sets the value of the newPrimaryRoleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewPrimaryRoleName(String value) {
        this.newPrimaryRoleName = value;
    }

    /**
     * Gets the value of the salutation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalutation() {
        return salutation;
    }

    /**
     * Sets the value of the salutation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalutation(String value) {
        this.salutation = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the afterHoursNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAfterHoursNumber() {
        return afterHoursNumber;
    }

    /**
     * Sets the value of the afterHoursNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAfterHoursNumber(String value) {
        this.afterHoursNumber = value;
    }

    /**
     * Gets the value of the mailStop property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailStop() {
        return mailStop;
    }

    /**
     * Sets the value of the mailStop property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailStop(String value) {
        this.mailStop = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link SiteStatus }
     *     
     */
    public SiteStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link SiteStatus }
     *     
     */
    public void setStatus(SiteStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the networkLogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkLogin() {
        return networkLogin;
    }

    /**
     * Sets the value of the networkLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkLogin(String value) {
        this.networkLogin = value;
    }

}
