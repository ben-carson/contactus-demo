
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetWholesalerCasesResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetWholesalerCasesResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StandardResponse" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *         &lt;element name="WholesalerCases" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ArrayOfWholesalerCase" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetWholesalerCasesResp", propOrder = {
    "standardResponse",
    "wholesalerCases"
})
public class GetWholesalerCasesResp {

    @XmlElement(name = "StandardResponse", required = true)
    protected StandardResponse standardResponse;
    @XmlElement(name = "WholesalerCases")
    protected ArrayOfWholesalerCase wholesalerCases;

    /**
     * Gets the value of the standardResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getStandardResponse() {
        return standardResponse;
    }

    /**
     * Sets the value of the standardResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setStandardResponse(StandardResponse value) {
        this.standardResponse = value;
    }

    /**
     * Gets the value of the wholesalerCases property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWholesalerCase }
     *     
     */
    public ArrayOfWholesalerCase getWholesalerCases() {
        return wholesalerCases;
    }

    /**
     * Sets the value of the wholesalerCases property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWholesalerCase }
     *     
     */
    public void setWholesalerCases(ArrayOfWholesalerCase value) {
        this.wholesalerCases = value;
    }

}
