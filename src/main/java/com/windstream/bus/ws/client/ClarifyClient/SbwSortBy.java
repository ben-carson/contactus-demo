
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sbwSortBy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="sbwSortBy">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="AccountNo"/>
 *     &lt;enumeration value="AccountName"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "sbwSortBy")
@XmlEnum
public enum SbwSortBy {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("AccountNo")
    ACCOUNT_NO("AccountNo"),
    @XmlEnumValue("AccountName")
    ACCOUNT_NAME("AccountName");
    private final String value;

    SbwSortBy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SbwSortBy fromValue(String v) {
        for (SbwSortBy c: SbwSortBy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
