
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseScriptResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseScriptResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StandardResponse" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *         &lt;element name="CaseScripts" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ArrayOfCaseScriptElement" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseScriptResp", propOrder = {
    "standardResponse",
    "caseScripts"
})
public class CaseScriptResp {

    @XmlElement(name = "StandardResponse", required = true)
    protected StandardResponse standardResponse;
    @XmlElement(name = "CaseScripts")
    protected ArrayOfCaseScriptElement caseScripts;

    /**
     * Gets the value of the standardResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getStandardResponse() {
        return standardResponse;
    }

    /**
     * Sets the value of the standardResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setStandardResponse(StandardResponse value) {
        this.standardResponse = value;
    }

    /**
     * Gets the value of the caseScripts property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCaseScriptElement }
     *     
     */
    public ArrayOfCaseScriptElement getCaseScripts() {
        return caseScripts;
    }

    /**
     * Sets the value of the caseScripts property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCaseScriptElement }
     *     
     */
    public void setCaseScripts(ArrayOfCaseScriptElement value) {
        this.caseScripts = value;
    }

}
