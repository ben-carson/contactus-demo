
/*
 * 
 */

package com.windstream.bus.ws.client.XrefClient;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.2.7
 * Wed Nov 09 14:45:37 EST 2011
 * Generated source version: 2.2.7
 * 
 */


@WebServiceClient(name = "XrefAccess", 
                  wsdlLocation = "http://aluisupport.dev.nuvox.net/AluiTools/services/XrefAccess?wsdl",
                  targetNamespace = "http://beans.nuvox.com") 
public class XrefAccess extends Service {

    public final static URL WSDL_LOCATION;
    public final static QName SERVICE = new QName("http://beans.nuvox.com", "XrefAccess");
    public final static QName XrefAccessHttpPort = new QName("http://beans.nuvox.com", "XrefAccessHttpPort");
    static {
        URL url = null;
        try {
            url = new URL("http://aluisupport.dev.nuvox.net/AluiTools/services/XrefAccess?wsdl");
        } catch (MalformedURLException e) {
            System.err.println("Can not initialize the default wsdl from http://aluisupport.dev.nuvox.net/AluiTools/services/XrefAccess?wsdl");
            // e.printStackTrace();
        }
        WSDL_LOCATION = url;
    }

    public XrefAccess(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public XrefAccess(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public XrefAccess() {
        super(WSDL_LOCATION, SERVICE);
    }

    /**
     * 
     * @return
     *     returns XrefAccessPortType
     */
    @WebEndpoint(name = "XrefAccessHttpPort")
    public XrefAccessPortType getXrefAccessHttpPort() {
        return super.getPort(XrefAccessHttpPort, XrefAccessPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns XrefAccessPortType
     */
    @WebEndpoint(name = "XrefAccessHttpPort")
    public XrefAccessPortType getXrefAccessHttpPort(WebServiceFeature... features) {
        return super.getPort(XrefAccessHttpPort, XrefAccessPortType.class, features);
    }

}
