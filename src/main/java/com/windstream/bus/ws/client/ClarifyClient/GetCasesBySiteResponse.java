
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCasesBySiteResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}CasesBySiteResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCasesBySiteResult"
})
@XmlRootElement(name = "GetCasesBySiteResponse")
public class GetCasesBySiteResponse {

    @XmlElement(name = "GetCasesBySiteResult", required = true)
    protected CasesBySiteResp getCasesBySiteResult;

    /**
     * Gets the value of the getCasesBySiteResult property.
     * 
     * @return
     *     possible object is
     *     {@link CasesBySiteResp }
     *     
     */
    public CasesBySiteResp getGetCasesBySiteResult() {
        return getCasesBySiteResult;
    }

    /**
     * Sets the value of the getCasesBySiteResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CasesBySiteResp }
     *     
     */
    public void setGetCasesBySiteResult(CasesBySiteResp value) {
        this.getCasesBySiteResult = value;
    }

}
