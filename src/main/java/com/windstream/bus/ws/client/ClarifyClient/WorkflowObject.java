
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WorkflowObject.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WorkflowObject">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Case"/>
 *     &lt;enumeration value="SubCase"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "WorkflowObject")
@XmlEnum
public enum WorkflowObject {

    @XmlEnumValue("Case")
    CASE("Case"),
    @XmlEnumValue("SubCase")
    SUB_CASE("SubCase");
    private final String value;

    WorkflowObject(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WorkflowObject fromValue(String v) {
        for (WorkflowObject c: WorkflowObject.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
