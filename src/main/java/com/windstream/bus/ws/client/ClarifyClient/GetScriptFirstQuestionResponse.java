
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetScriptFirstQuestionResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ScriptQuestionResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getScriptFirstQuestionResult"
})
@XmlRootElement(name = "GetScriptFirstQuestionResponse")
public class GetScriptFirstQuestionResponse {

    @XmlElement(name = "GetScriptFirstQuestionResult", required = true)
    protected ScriptQuestionResp getScriptFirstQuestionResult;

    /**
     * Gets the value of the getScriptFirstQuestionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ScriptQuestionResp }
     *     
     */
    public ScriptQuestionResp getGetScriptFirstQuestionResult() {
        return getScriptFirstQuestionResult;
    }

    /**
     * Sets the value of the getScriptFirstQuestionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScriptQuestionResp }
     *     
     */
    public void setGetScriptFirstQuestionResult(ScriptQuestionResp value) {
        this.getScriptFirstQuestionResult = value;
    }

}
