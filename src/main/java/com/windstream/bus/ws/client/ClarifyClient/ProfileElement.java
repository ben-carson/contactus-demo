
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProfileElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfileElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProfileTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfileGlobalID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SubTypeTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubTypeGlobalID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileElement", propOrder = {
    "profileTitle",
    "profileGlobalID",
    "subTypeTitle",
    "subTypeGlobalID"
})
public class ProfileElement {

    @XmlElement(name = "ProfileTitle")
    protected String profileTitle;
    @XmlElement(name = "ProfileGlobalID")
    protected int profileGlobalID;
    @XmlElement(name = "SubTypeTitle")
    protected String subTypeTitle;
    @XmlElement(name = "SubTypeGlobalID")
    protected int subTypeGlobalID;

    /**
     * Gets the value of the profileTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileTitle() {
        return profileTitle;
    }

    /**
     * Sets the value of the profileTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileTitle(String value) {
        this.profileTitle = value;
    }

    /**
     * Gets the value of the profileGlobalID property.
     * 
     */
    public int getProfileGlobalID() {
        return profileGlobalID;
    }

    /**
     * Sets the value of the profileGlobalID property.
     * 
     */
    public void setProfileGlobalID(int value) {
        this.profileGlobalID = value;
    }

    /**
     * Gets the value of the subTypeTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubTypeTitle() {
        return subTypeTitle;
    }

    /**
     * Sets the value of the subTypeTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubTypeTitle(String value) {
        this.subTypeTitle = value;
    }

    /**
     * Gets the value of the subTypeGlobalID property.
     * 
     */
    public int getSubTypeGlobalID() {
        return subTypeGlobalID;
    }

    /**
     * Sets the value of the subTypeGlobalID property.
     * 
     */
    public void setSubTypeGlobalID(int value) {
        this.subTypeGlobalID = value;
    }

}
