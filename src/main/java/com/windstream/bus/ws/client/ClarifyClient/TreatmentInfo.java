
package com.windstream.bus.ws.client.ClarifyClient;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TreatmentInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TreatmentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TrmtObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="SubId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="Bal30to60" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Bal60to90" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BalOver90" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PastDueBal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="InvoiceAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CrdtCls" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="BillingCycle" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="MRC" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BillingSystem" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="InvoiceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TreatmentInfo", propOrder = {
    "trmtObjid",
    "subId",
    "bal30To60",
    "bal60To90",
    "balOver90",
    "pastDueBal",
    "invoiceAmount",
    "crdtCls",
    "billingCycle",
    "mrc",
    "billingSystem",
    "invoiceDate"
})
public class TreatmentInfo {

    @XmlElement(name = "TrmtObjid")
    protected long trmtObjid;
    @XmlElement(name = "SubId")
    protected long subId;
    @XmlElement(name = "Bal30to60", required = true)
    protected BigDecimal bal30To60;
    @XmlElement(name = "Bal60to90", required = true)
    protected BigDecimal bal60To90;
    @XmlElement(name = "BalOver90", required = true)
    protected BigDecimal balOver90;
    @XmlElement(name = "PastDueBal", required = true)
    protected BigDecimal pastDueBal;
    @XmlElement(name = "InvoiceAmount", required = true)
    protected BigDecimal invoiceAmount;
    @XmlElement(name = "CrdtCls")
    protected long crdtCls;
    @XmlElement(name = "BillingCycle")
    protected long billingCycle;
    @XmlElement(name = "MRC", required = true)
    protected BigDecimal mrc;
    @XmlElement(name = "BillingSystem")
    protected long billingSystem;
    @XmlElement(name = "InvoiceDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar invoiceDate;

    /**
     * Gets the value of the trmtObjid property.
     * 
     */
    public long getTrmtObjid() {
        return trmtObjid;
    }

    /**
     * Sets the value of the trmtObjid property.
     * 
     */
    public void setTrmtObjid(long value) {
        this.trmtObjid = value;
    }

    /**
     * Gets the value of the subId property.
     * 
     */
    public long getSubId() {
        return subId;
    }

    /**
     * Sets the value of the subId property.
     * 
     */
    public void setSubId(long value) {
        this.subId = value;
    }

    /**
     * Gets the value of the bal30To60 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBal30To60() {
        return bal30To60;
    }

    /**
     * Sets the value of the bal30To60 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBal30To60(BigDecimal value) {
        this.bal30To60 = value;
    }

    /**
     * Gets the value of the bal60To90 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBal60To90() {
        return bal60To90;
    }

    /**
     * Sets the value of the bal60To90 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBal60To90(BigDecimal value) {
        this.bal60To90 = value;
    }

    /**
     * Gets the value of the balOver90 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBalOver90() {
        return balOver90;
    }

    /**
     * Sets the value of the balOver90 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBalOver90(BigDecimal value) {
        this.balOver90 = value;
    }

    /**
     * Gets the value of the pastDueBal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPastDueBal() {
        return pastDueBal;
    }

    /**
     * Sets the value of the pastDueBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPastDueBal(BigDecimal value) {
        this.pastDueBal = value;
    }

    /**
     * Gets the value of the invoiceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInvoiceAmount() {
        return invoiceAmount;
    }

    /**
     * Sets the value of the invoiceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInvoiceAmount(BigDecimal value) {
        this.invoiceAmount = value;
    }

    /**
     * Gets the value of the crdtCls property.
     * 
     */
    public long getCrdtCls() {
        return crdtCls;
    }

    /**
     * Sets the value of the crdtCls property.
     * 
     */
    public void setCrdtCls(long value) {
        this.crdtCls = value;
    }

    /**
     * Gets the value of the billingCycle property.
     * 
     */
    public long getBillingCycle() {
        return billingCycle;
    }

    /**
     * Sets the value of the billingCycle property.
     * 
     */
    public void setBillingCycle(long value) {
        this.billingCycle = value;
    }

    /**
     * Gets the value of the mrc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMRC() {
        return mrc;
    }

    /**
     * Sets the value of the mrc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMRC(BigDecimal value) {
        this.mrc = value;
    }

    /**
     * Gets the value of the billingSystem property.
     * 
     */
    public long getBillingSystem() {
        return billingSystem;
    }

    /**
     * Sets the value of the billingSystem property.
     * 
     */
    public void setBillingSystem(long value) {
        this.billingSystem = value;
    }

    /**
     * Gets the value of the invoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Sets the value of the invoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInvoiceDate(XMLGregorianCalendar value) {
        this.invoiceDate = value;
    }

}
