
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PopupListName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SortBy" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}pllSortBy"/>
 *         &lt;element name="Order" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SortOrder"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "popupListName",
    "sortBy",
    "order"
})
@XmlRootElement(name = "GetPopupListLevel1")
public class GetPopupListLevel1 {

    @XmlElement(name = "PopupListName")
    protected String popupListName;
    @XmlElement(name = "SortBy", required = true, nillable = true)
    protected PllSortBy sortBy;
    @XmlElement(name = "Order", required = true, nillable = true)
    protected SortOrder order;

    /**
     * Gets the value of the popupListName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPopupListName() {
        return popupListName;
    }

    /**
     * Sets the value of the popupListName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPopupListName(String value) {
        this.popupListName = value;
    }

    /**
     * Gets the value of the sortBy property.
     * 
     * @return
     *     possible object is
     *     {@link PllSortBy }
     *     
     */
    public PllSortBy getSortBy() {
        return sortBy;
    }

    /**
     * Sets the value of the sortBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PllSortBy }
     *     
     */
    public void setSortBy(PllSortBy value) {
        this.sortBy = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link SortOrder }
     *     
     */
    public SortOrder getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortOrder }
     *     
     */
    public void setOrder(SortOrder value) {
        this.order = value;
    }

}
