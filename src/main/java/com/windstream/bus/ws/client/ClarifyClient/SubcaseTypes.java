
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubcaseTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SubcaseTypes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="General"/>
 *     &lt;enumeration value="Administrative"/>
 *     &lt;enumeration value="Escalation"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SubcaseTypes")
@XmlEnum
public enum SubcaseTypes {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("General")
    GENERAL("General"),
    @XmlEnumValue("Administrative")
    ADMINISTRATIVE("Administrative"),
    @XmlEnumValue("Escalation")
    ESCALATION("Escalation");
    private final String value;

    SubcaseTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SubcaseTypes fromValue(String v) {
        for (SubcaseTypes c: SubcaseTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
