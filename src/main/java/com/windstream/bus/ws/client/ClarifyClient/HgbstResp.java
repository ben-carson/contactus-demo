
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HgbstResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HgbstResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StandardResponse" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *         &lt;element name="HgbstElements" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ArrayOfHgbstElement" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HgbstResp", propOrder = {
    "standardResponse",
    "hgbstElements"
})
public class HgbstResp {

    @XmlElement(name = "StandardResponse", required = true)
    protected StandardResponse standardResponse;
    @XmlElement(name = "HgbstElements")
    protected ArrayOfHgbstElement hgbstElements;

    /**
     * Gets the value of the standardResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getStandardResponse() {
        return standardResponse;
    }

    /**
     * Sets the value of the standardResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setStandardResponse(StandardResponse value) {
        this.standardResponse = value;
    }

    /**
     * Gets the value of the hgbstElements property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfHgbstElement }
     *     
     */
    public ArrayOfHgbstElement getHgbstElements() {
        return hgbstElements;
    }

    /**
     * Sets the value of the hgbstElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfHgbstElement }
     *     
     */
    public void setHgbstElements(ArrayOfHgbstElement value) {
        this.hgbstElements = value;
    }

}
