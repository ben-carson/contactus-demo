
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubcaseSearchField.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SubcaseSearchField">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Objid"/>
 *     &lt;enumeration value="SubcaseID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SubcaseSearchField")
@XmlEnum
public enum SubcaseSearchField {

    @XmlEnumValue("Objid")
    OBJID("Objid"),
    @XmlEnumValue("SubcaseID")
    SUBCASE_ID("SubcaseID");
    private final String value;

    SubcaseSearchField(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SubcaseSearchField fromValue(String v) {
        for (SubcaseSearchField c: SubcaseSearchField.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
