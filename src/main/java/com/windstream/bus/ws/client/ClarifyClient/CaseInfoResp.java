
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseInfoResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseInfoResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StandardResponse" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *         &lt;element name="Cases" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}CasesInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseInfoResp", propOrder = {
    "standardResponse",
    "cases"
})
public class CaseInfoResp {

    @XmlElement(name = "StandardResponse", required = true)
    protected StandardResponse standardResponse;
    @XmlElement(name = "Cases", required = true)
    protected CasesInfo cases;

    /**
     * Gets the value of the standardResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getStandardResponse() {
        return standardResponse;
    }

    /**
     * Sets the value of the standardResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setStandardResponse(StandardResponse value) {
        this.standardResponse = value;
    }

    /**
     * Gets the value of the cases property.
     * 
     * @return
     *     possible object is
     *     {@link CasesInfo }
     *     
     */
    public CasesInfo getCases() {
        return cases;
    }

    /**
     * Sets the value of the cases property.
     * 
     * @param value
     *     allowed object is
     *     {@link CasesInfo }
     *     
     */
    public void setCases(CasesInfo value) {
        this.cases = value;
    }

}
