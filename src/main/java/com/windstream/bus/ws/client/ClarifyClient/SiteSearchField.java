
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SiteSearchField.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SiteSearchField">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Objid"/>
 *     &lt;enumeration value="SiteID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SiteSearchField")
@XmlEnum
public enum SiteSearchField {

    @XmlEnumValue("Objid")
    OBJID("Objid"),
    @XmlEnumValue("SiteID")
    SITE_ID("SiteID");
    private final String value;

    SiteSearchField(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SiteSearchField fromValue(String v) {
        for (SiteSearchField c: SiteSearchField.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
