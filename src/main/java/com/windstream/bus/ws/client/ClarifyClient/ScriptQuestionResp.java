
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScriptQuestionResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScriptQuestionResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StandardResponse" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *         &lt;element name="Question" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ScriptQuestion"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScriptQuestionResp", propOrder = {
    "standardResponse",
    "question"
})
public class ScriptQuestionResp {

    @XmlElement(name = "StandardResponse", required = true)
    protected StandardResponse standardResponse;
    @XmlElement(name = "Question", required = true, nillable = true)
    protected ScriptQuestion question;

    /**
     * Gets the value of the standardResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getStandardResponse() {
        return standardResponse;
    }

    /**
     * Sets the value of the standardResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setStandardResponse(StandardResponse value) {
        this.standardResponse = value;
    }

    /**
     * Gets the value of the question property.
     * 
     * @return
     *     possible object is
     *     {@link ScriptQuestion }
     *     
     */
    public ScriptQuestion getQuestion() {
        return question;
    }

    /**
     * Sets the value of the question property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScriptQuestion }
     *     
     */
    public void setQuestion(ScriptQuestion value) {
        this.question = value;
    }

}
