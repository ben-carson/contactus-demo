
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CloseCaseResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "closeCaseResult"
})
@XmlRootElement(name = "CloseCaseResponse")
public class CloseCaseResponse {

    @XmlElement(name = "CloseCaseResult", required = true)
    protected StandardResponse closeCaseResult;

    /**
     * Gets the value of the closeCaseResult property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getCloseCaseResult() {
        return closeCaseResult;
    }

    /**
     * Sets the value of the closeCaseResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setCloseCaseResult(StandardResponse value) {
        this.closeCaseResult = value;
    }

}
