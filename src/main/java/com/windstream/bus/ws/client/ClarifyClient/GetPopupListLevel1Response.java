
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetPopupListLevel1Result" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}PopupListLevel1Resp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPopupListLevel1Result"
})
@XmlRootElement(name = "GetPopupListLevel1Response")
public class GetPopupListLevel1Response {

    @XmlElement(name = "GetPopupListLevel1Result", required = true)
    protected PopupListLevel1Resp getPopupListLevel1Result;

    /**
     * Gets the value of the getPopupListLevel1Result property.
     * 
     * @return
     *     possible object is
     *     {@link PopupListLevel1Resp }
     *     
     */
    public PopupListLevel1Resp getGetPopupListLevel1Result() {
        return getPopupListLevel1Result;
    }

    /**
     * Sets the value of the getPopupListLevel1Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link PopupListLevel1Resp }
     *     
     */
    public void setGetPopupListLevel1Result(PopupListLevel1Resp value) {
        this.getPopupListLevel1Result = value;
    }

}
