
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateClosed" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CloseStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResolutionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Summary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CloseCode1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CloseCode2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CloseCode3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CloseCode4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idNumber",
    "userName",
    "dateClosed",
    "closeStatus",
    "resolutionCode",
    "summary",
    "closeCode1",
    "closeCode2",
    "closeCode3",
    "closeCode4"
})
@XmlRootElement(name = "CloseCase")
public class CloseCase {

    @XmlElement(name = "IDNumber")
    protected String idNumber;
    @XmlElement(name = "UserName")
    protected String userName;
    @XmlElement(name = "DateClosed", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateClosed;
    @XmlElement(name = "CloseStatus")
    protected String closeStatus;
    @XmlElement(name = "ResolutionCode")
    protected String resolutionCode;
    @XmlElement(name = "Summary")
    protected String summary;
    @XmlElement(name = "CloseCode1")
    protected String closeCode1;
    @XmlElement(name = "CloseCode2")
    protected String closeCode2;
    @XmlElement(name = "CloseCode3")
    protected String closeCode3;
    @XmlElement(name = "CloseCode4")
    protected String closeCode4;

    /**
     * Gets the value of the idNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDNumber() {
        return idNumber;
    }

    /**
     * Sets the value of the idNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDNumber(String value) {
        this.idNumber = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the dateClosed property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateClosed() {
        return dateClosed;
    }

    /**
     * Sets the value of the dateClosed property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateClosed(XMLGregorianCalendar value) {
        this.dateClosed = value;
    }

    /**
     * Gets the value of the closeStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseStatus() {
        return closeStatus;
    }

    /**
     * Sets the value of the closeStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseStatus(String value) {
        this.closeStatus = value;
    }

    /**
     * Gets the value of the resolutionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResolutionCode() {
        return resolutionCode;
    }

    /**
     * Sets the value of the resolutionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResolutionCode(String value) {
        this.resolutionCode = value;
    }

    /**
     * Gets the value of the summary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Sets the value of the summary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummary(String value) {
        this.summary = value;
    }

    /**
     * Gets the value of the closeCode1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseCode1() {
        return closeCode1;
    }

    /**
     * Sets the value of the closeCode1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseCode1(String value) {
        this.closeCode1 = value;
    }

    /**
     * Gets the value of the closeCode2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseCode2() {
        return closeCode2;
    }

    /**
     * Sets the value of the closeCode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseCode2(String value) {
        this.closeCode2 = value;
    }

    /**
     * Gets the value of the closeCode3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseCode3() {
        return closeCode3;
    }

    /**
     * Sets the value of the closeCode3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseCode3(String value) {
        this.closeCode3 = value;
    }

    /**
     * Gets the value of the closeCode4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseCode4() {
        return closeCode4;
    }

    /**
     * Sets the value of the closeCode4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseCode4(String value) {
        this.closeCode4 = value;
    }

}
