
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetContactInfoByObjidResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}GetContactInfoResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getContactInfoByObjidResult"
})
@XmlRootElement(name = "GetContactInfoByObjidResponse")
public class GetContactInfoByObjidResponse {

    @XmlElement(name = "GetContactInfoByObjidResult", required = true)
    protected GetContactInfoResp getContactInfoByObjidResult;

    /**
     * Gets the value of the getContactInfoByObjidResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetContactInfoResp }
     *     
     */
    public GetContactInfoResp getGetContactInfoByObjidResult() {
        return getContactInfoByObjidResult;
    }

    /**
     * Sets the value of the getContactInfoByObjidResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetContactInfoResp }
     *     
     */
    public void setGetContactInfoByObjidResult(GetContactInfoResp value) {
        this.getContactInfoByObjidResult = value;
    }

}
