
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCaseScriptResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}CaseScriptResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCaseScriptResult"
})
@XmlRootElement(name = "GetCaseScriptResponse")
public class GetCaseScriptResponse {

    @XmlElement(name = "GetCaseScriptResult", required = true)
    protected CaseScriptResp getCaseScriptResult;

    /**
     * Gets the value of the getCaseScriptResult property.
     * 
     * @return
     *     possible object is
     *     {@link CaseScriptResp }
     *     
     */
    public CaseScriptResp getGetCaseScriptResult() {
        return getCaseScriptResult;
    }

    /**
     * Sets the value of the getCaseScriptResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseScriptResp }
     *     
     */
    public void setGetCaseScriptResult(CaseScriptResp value) {
        this.getCaseScriptResult = value;
    }

}
