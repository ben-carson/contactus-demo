
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetContactInfoByNameResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}GetContactInfoResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getContactInfoByNameResult"
})
@XmlRootElement(name = "GetContactInfoByNameResponse")
public class GetContactInfoByNameResponse {

    @XmlElement(name = "GetContactInfoByNameResult", required = true)
    protected GetContactInfoResp getContactInfoByNameResult;

    /**
     * Gets the value of the getContactInfoByNameResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetContactInfoResp }
     *     
     */
    public GetContactInfoResp getGetContactInfoByNameResult() {
        return getContactInfoByNameResult;
    }

    /**
     * Sets the value of the getContactInfoByNameResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetContactInfoResp }
     *     
     */
    public void setGetContactInfoByNameResult(GetContactInfoResp value) {
        this.getContactInfoByNameResult = value;
    }

}
