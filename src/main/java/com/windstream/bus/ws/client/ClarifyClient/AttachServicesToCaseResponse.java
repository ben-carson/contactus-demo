
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AttachServicesToCaseResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}AttachServicesToCaseResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "attachServicesToCaseResult"
})
@XmlRootElement(name = "AttachServicesToCaseResponse")
public class AttachServicesToCaseResponse {

    @XmlElement(name = "AttachServicesToCaseResult", required = true)
    protected AttachServicesToCaseResp attachServicesToCaseResult;

    /**
     * Gets the value of the attachServicesToCaseResult property.
     * 
     * @return
     *     possible object is
     *     {@link AttachServicesToCaseResp }
     *     
     */
    public AttachServicesToCaseResp getAttachServicesToCaseResult() {
        return attachServicesToCaseResult;
    }

    /**
     * Sets the value of the attachServicesToCaseResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachServicesToCaseResp }
     *     
     */
    public void setAttachServicesToCaseResult(AttachServicesToCaseResp value) {
        this.attachServicesToCaseResult = value;
    }

}
