
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WholesalerId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="WholesalerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="AccountName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IncludeInactive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SortBy" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}sbwSortBy"/>
 *         &lt;element name="Order" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SortOrder"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wholesalerId",
    "wholesalerName",
    "accountNo",
    "accountName",
    "includeInactive",
    "sortBy",
    "order"
})
@XmlRootElement(name = "SearchByWholesaler")
public class SearchByWholesaler {

    @XmlElement(name = "WholesalerId")
    protected long wholesalerId;
    @XmlElement(name = "WholesalerName")
    protected String wholesalerName;
    @XmlElement(name = "AccountNo")
    protected long accountNo;
    @XmlElement(name = "AccountName")
    protected String accountName;
    @XmlElement(name = "IncludeInactive", required = true, type = Boolean.class, nillable = true)
    protected Boolean includeInactive;
    @XmlElement(name = "SortBy", required = true, nillable = true)
    protected SbwSortBy sortBy;
    @XmlElement(name = "Order", required = true, nillable = true)
    protected SortOrder order;

    /**
     * Gets the value of the wholesalerId property.
     * 
     */
    public long getWholesalerId() {
        return wholesalerId;
    }

    /**
     * Sets the value of the wholesalerId property.
     * 
     */
    public void setWholesalerId(long value) {
        this.wholesalerId = value;
    }

    /**
     * Gets the value of the wholesalerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWholesalerName() {
        return wholesalerName;
    }

    /**
     * Sets the value of the wholesalerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWholesalerName(String value) {
        this.wholesalerName = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     */
    public long getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     */
    public void setAccountNo(long value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the accountName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Sets the value of the accountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountName(String value) {
        this.accountName = value;
    }

    /**
     * Gets the value of the includeInactive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeInactive() {
        return includeInactive;
    }

    /**
     * Sets the value of the includeInactive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeInactive(Boolean value) {
        this.includeInactive = value;
    }

    /**
     * Gets the value of the sortBy property.
     * 
     * @return
     *     possible object is
     *     {@link SbwSortBy }
     *     
     */
    public SbwSortBy getSortBy() {
        return sortBy;
    }

    /**
     * Sets the value of the sortBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link SbwSortBy }
     *     
     */
    public void setSortBy(SbwSortBy value) {
        this.sortBy = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link SortOrder }
     *     
     */
    public SortOrder getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortOrder }
     *     
     */
    public void setOrder(SortOrder value) {
        this.order = value;
    }

}
