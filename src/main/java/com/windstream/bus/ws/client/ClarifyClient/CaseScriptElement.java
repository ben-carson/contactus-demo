
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseScriptElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseScriptElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Question" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Required" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Rank" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ResponseType" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ScriptResponseType"/>
 *         &lt;element name="ResponseList" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}ArrayOfHgbstElement" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseScriptElement", propOrder = {
    "question",
    "required",
    "rank",
    "responseType",
    "responseList"
})
public class CaseScriptElement {

    @XmlElement(name = "Question")
    protected String question;
    @XmlElement(name = "Required")
    protected boolean required;
    @XmlElement(name = "Rank")
    protected int rank;
    @XmlElement(name = "ResponseType", required = true)
    protected ScriptResponseType responseType;
    @XmlElement(name = "ResponseList")
    protected ArrayOfHgbstElement responseList;

    /**
     * Gets the value of the question property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Sets the value of the question property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestion(String value) {
        this.question = value;
    }

    /**
     * Gets the value of the required property.
     * 
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * Sets the value of the required property.
     * 
     */
    public void setRequired(boolean value) {
        this.required = value;
    }

    /**
     * Gets the value of the rank property.
     * 
     */
    public int getRank() {
        return rank;
    }

    /**
     * Sets the value of the rank property.
     * 
     */
    public void setRank(int value) {
        this.rank = value;
    }

    /**
     * Gets the value of the responseType property.
     * 
     * @return
     *     possible object is
     *     {@link ScriptResponseType }
     *     
     */
    public ScriptResponseType getResponseType() {
        return responseType;
    }

    /**
     * Sets the value of the responseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScriptResponseType }
     *     
     */
    public void setResponseType(ScriptResponseType value) {
        this.responseType = value;
    }

    /**
     * Gets the value of the responseList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfHgbstElement }
     *     
     */
    public ArrayOfHgbstElement getResponseList() {
        return responseList;
    }

    /**
     * Sets the value of the responseList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfHgbstElement }
     *     
     */
    public void setResponseList(ArrayOfHgbstElement value) {
        this.responseList = value;
    }

}
