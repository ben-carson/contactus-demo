
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AlternateContactInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AlternateContactInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AltContactObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="AltContactFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AltContactLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AltContactPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlternateContactInfo", propOrder = {
    "altContactObjid",
    "altContactFirstName",
    "altContactLastName",
    "altContactPhone"
})
public class AlternateContactInfo {

    @XmlElement(name = "AltContactObjid")
    protected long altContactObjid;
    @XmlElement(name = "AltContactFirstName")
    protected String altContactFirstName;
    @XmlElement(name = "AltContactLastName")
    protected String altContactLastName;
    @XmlElement(name = "AltContactPhone")
    protected String altContactPhone;

    /**
     * Gets the value of the altContactObjid property.
     * 
     */
    public long getAltContactObjid() {
        return altContactObjid;
    }

    /**
     * Sets the value of the altContactObjid property.
     * 
     */
    public void setAltContactObjid(long value) {
        this.altContactObjid = value;
    }

    /**
     * Gets the value of the altContactFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltContactFirstName() {
        return altContactFirstName;
    }

    /**
     * Sets the value of the altContactFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltContactFirstName(String value) {
        this.altContactFirstName = value;
    }

    /**
     * Gets the value of the altContactLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltContactLastName() {
        return altContactLastName;
    }

    /**
     * Sets the value of the altContactLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltContactLastName(String value) {
        this.altContactLastName = value;
    }

    /**
     * Gets the value of the altContactPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltContactPhone() {
        return altContactPhone;
    }

    /**
     * Sets the value of the altContactPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltContactPhone(String value) {
        this.altContactPhone = value;
    }

}
