
package com.windstream.bus.ws.client.ClarifyClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSiteContacts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSiteContacts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SiteContacts" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SiteContacts" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSiteContacts", propOrder = {
    "siteContacts"
})
public class ArrayOfSiteContacts {

    @XmlElement(name = "SiteContacts")
    protected List<SiteContacts> siteContacts;

    /**
     * Gets the value of the siteContacts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the siteContacts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSiteContacts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SiteContacts }
     * 
     * 
     */
    public List<SiteContacts> getSiteContacts() {
        if (siteContacts == null) {
            siteContacts = new ArrayList<SiteContacts>();
        }
        return this.siteContacts;
    }

}
