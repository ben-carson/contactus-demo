
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.windstream.bus.ws.client.ClarifyClient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.windstream.bus.ws.client.ClarifyClient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CasesBySite }
     * 
     */
    public CasesBySite createCasesBySite() {
        return new CasesBySite();
    }

    /**
     * Create an instance of {@link ScriptQuestion }
     * 
     */
    public ScriptQuestion createScriptQuestion() {
        return new ScriptQuestion();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link ArrayOfSiteElement }
     * 
     */
    public ArrayOfSiteElement createArrayOfSiteElement() {
        return new ArrayOfSiteElement();
    }

    /**
     * Create an instance of {@link ArrayOfProfileElement }
     * 
     */
    public ArrayOfProfileElement createArrayOfProfileElement() {
        return new ArrayOfProfileElement();
    }

    /**
     * Create an instance of {@link ArrayOfServiceItem }
     * 
     */
    public ArrayOfServiceItem createArrayOfServiceItem() {
        return new ArrayOfServiceItem();
    }

    /**
     * Create an instance of {@link GetCaseInfo }
     * 
     */
    public GetCaseInfo createGetCaseInfo() {
        return new GetCaseInfo();
    }

    /**
     * Create an instance of {@link CreateSubcaseResponse }
     * 
     */
    public CreateSubcaseResponse createCreateSubcaseResponse() {
        return new CreateSubcaseResponse();
    }

    /**
     * Create an instance of {@link GetCaseSubcasesResponse }
     * 
     */
    public GetCaseSubcasesResponse createGetCaseSubcasesResponse() {
        return new GetCaseSubcasesResponse();
    }

    /**
     * Create an instance of {@link GetSubcaseInfo }
     * 
     */
    public GetSubcaseInfo createGetSubcaseInfo() {
        return new GetSubcaseInfo();
    }

    /**
     * Create an instance of {@link CaseScriptResp }
     * 
     */
    public CaseScriptResp createCaseScriptResp() {
        return new CaseScriptResp();
    }

    /**
     * Create an instance of {@link CreateCaseResponse }
     * 
     */
    public CreateCaseResponse createCreateCaseResponse() {
        return new CreateCaseResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPopupList }
     * 
     */
    public ArrayOfPopupList createArrayOfPopupList() {
        return new ArrayOfPopupList();
    }

    /**
     * Create an instance of {@link Assign }
     * 
     */
    public Assign createAssign() {
        return new Assign();
    }

    /**
     * Create an instance of {@link GetSiteContacts }
     * 
     */
    public GetSiteContacts createGetSiteContacts() {
        return new GetSiteContacts();
    }

    /**
     * Create an instance of {@link CasesInfo }
     * 
     */
    public CasesInfo createCasesInfo() {
        return new CasesInfo();
    }

    /**
     * Create an instance of {@link AttachServicesToCaseResp }
     * 
     */
    public AttachServicesToCaseResp createAttachServicesToCaseResp() {
        return new AttachServicesToCaseResp();
    }

    /**
     * Create an instance of {@link GetScriptQuestionResponse }
     * 
     */
    public GetScriptQuestionResponse createGetScriptQuestionResponse() {
        return new GetScriptQuestionResponse();
    }

    /**
     * Create an instance of {@link AttachServicesToCaseResponse }
     * 
     */
    public AttachServicesToCaseResponse createAttachServicesToCaseResponse() {
        return new AttachServicesToCaseResponse();
    }

    /**
     * Create an instance of {@link UpdateContactResponse }
     * 
     */
    public UpdateContactResponse createUpdateContactResponse() {
        return new UpdateContactResponse();
    }

    /**
     * Create an instance of {@link EscalationElement }
     * 
     */
    public EscalationElement createEscalationElement() {
        return new EscalationElement();
    }

    /**
     * Create an instance of {@link CloseSubCaseResponse }
     * 
     */
    public CloseSubCaseResponse createCloseSubCaseResponse() {
        return new CloseSubCaseResponse();
    }

    /**
     * Create an instance of {@link ScriptQuestionResp }
     * 
     */
    public ScriptQuestionResp createScriptQuestionResp() {
        return new ScriptQuestionResp();
    }

    /**
     * Create an instance of {@link OtherSiteElement }
     * 
     */
    public OtherSiteElement createOtherSiteElement() {
        return new OtherSiteElement();
    }

    /**
     * Create an instance of {@link GetScriptFirstQuestion }
     * 
     */
    public GetScriptFirstQuestion createGetScriptFirstQuestion() {
        return new GetScriptFirstQuestion();
    }

    /**
     * Create an instance of {@link UpdateSite }
     * 
     */
    public UpdateSite createUpdateSite() {
        return new UpdateSite();
    }

    /**
     * Create an instance of {@link WholesalerCase }
     * 
     */
    public WholesalerCase createWholesalerCase() {
        return new WholesalerCase();
    }

    /**
     * Create an instance of {@link RemoveContactRoleResponse }
     * 
     */
    public RemoveContactRoleResponse createRemoveContactRoleResponse() {
        return new RemoveContactRoleResponse();
    }

    /**
     * Create an instance of {@link GetPopupListLevel1 }
     * 
     */
    public GetPopupListLevel1 createGetPopupListLevel1() {
        return new GetPopupListLevel1();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link TreatmentElement }
     * 
     */
    public TreatmentElement createTreatmentElement() {
        return new TreatmentElement();
    }

    /**
     * Create an instance of {@link CreateContactRole }
     * 
     */
    public CreateContactRole createCreateContactRole() {
        return new CreateContactRole();
    }

    /**
     * Create an instance of {@link CreateContactResponse }
     * 
     */
    public CreateContactResponse createCreateContactResponse() {
        return new CreateContactResponse();
    }

    /**
     * Create an instance of {@link LogNotesResponse }
     * 
     */
    public LogNotesResponse createLogNotesResponse() {
        return new LogNotesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfOtherSiteElement }
     * 
     */
    public ArrayOfOtherSiteElement createArrayOfOtherSiteElement() {
        return new ArrayOfOtherSiteElement();
    }

    /**
     * Create an instance of {@link LogEmail }
     * 
     */
    public LogEmail createLogEmail() {
        return new LogEmail();
    }

    /**
     * Create an instance of {@link ScriptResp }
     * 
     */
    public ScriptResp createScriptResp() {
        return new ScriptResp();
    }

    /**
     * Create an instance of {@link UpdateContact }
     * 
     */
    public UpdateContact createUpdateContact() {
        return new UpdateContact();
    }

    /**
     * Create an instance of {@link GetScriptFirstQuestionResponse }
     * 
     */
    public GetScriptFirstQuestionResponse createGetScriptFirstQuestionResponse() {
        return new GetScriptFirstQuestionResponse();
    }

    /**
     * Create an instance of {@link GetCaseCountResponse }
     * 
     */
    public GetCaseCountResponse createGetCaseCountResponse() {
        return new GetCaseCountResponse();
    }

    /**
     * Create an instance of {@link CaseScriptElement }
     * 
     */
    public CaseScriptElement createCaseScriptElement() {
        return new CaseScriptElement();
    }

    /**
     * Create an instance of {@link LogEmailResponse }
     * 
     */
    public LogEmailResponse createLogEmailResponse() {
        return new LogEmailResponse();
    }

    /**
     * Create an instance of {@link DispatchResponse }
     * 
     */
    public DispatchResponse createDispatchResponse() {
        return new DispatchResponse();
    }

    /**
     * Create an instance of {@link ProfileElement }
     * 
     */
    public ProfileElement createProfileElement() {
        return new ProfileElement();
    }

    /**
     * Create an instance of {@link GetCasesBySite }
     * 
     */
    public GetCasesBySite createGetCasesBySite() {
        return new GetCasesBySite();
    }

    /**
     * Create an instance of {@link GetWholesalerSiteContactsResponse }
     * 
     */
    public GetWholesalerSiteContactsResponse createGetWholesalerSiteContactsResponse() {
        return new GetWholesalerSiteContactsResponse();
    }

    /**
     * Create an instance of {@link SubcaseResp }
     * 
     */
    public SubcaseResp createSubcaseResp() {
        return new SubcaseResp();
    }

    /**
     * Create an instance of {@link ArrayOfWholesalerCase }
     * 
     */
    public ArrayOfWholesalerCase createArrayOfWholesalerCase() {
        return new ArrayOfWholesalerCase();
    }

    /**
     * Create an instance of {@link ArrayOfScriptResp }
     * 
     */
    public ArrayOfScriptResp createArrayOfScriptResp() {
        return new ArrayOfScriptResp();
    }

    /**
     * Create an instance of {@link ServiceError }
     * 
     */
    public ServiceError createServiceError() {
        return new ServiceError();
    }

    /**
     * Create an instance of {@link ServiceItem }
     * 
     */
    public ServiceItem createServiceItem() {
        return new ServiceItem();
    }

    /**
     * Create an instance of {@link GetWholesalerCasesResponse }
     * 
     */
    public GetWholesalerCasesResponse createGetWholesalerCasesResponse() {
        return new GetWholesalerCasesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfServiceHeader }
     * 
     */
    public ArrayOfServiceHeader createArrayOfServiceHeader() {
        return new ArrayOfServiceHeader();
    }

    /**
     * Create an instance of {@link Dispatch }
     * 
     */
    public Dispatch createDispatch() {
        return new Dispatch();
    }

    /**
     * Create an instance of {@link ArrayOfServiceError }
     * 
     */
    public ArrayOfServiceError createArrayOfServiceError() {
        return new ArrayOfServiceError();
    }

    /**
     * Create an instance of {@link GetCaseLogs }
     * 
     */
    public GetCaseLogs createGetCaseLogs() {
        return new GetCaseLogs();
    }

    /**
     * Create an instance of {@link GetCaseCount }
     * 
     */
    public GetCaseCount createGetCaseCount() {
        return new GetCaseCount();
    }

    /**
     * Create an instance of {@link GetPopupListLevel1Response }
     * 
     */
    public GetPopupListLevel1Response createGetPopupListLevel1Response() {
        return new GetPopupListLevel1Response();
    }

    /**
     * Create an instance of {@link CreateSite }
     * 
     */
    public CreateSite createCreateSite() {
        return new CreateSite();
    }

    /**
     * Create an instance of {@link GetCaseSubcases }
     * 
     */
    public GetCaseSubcases createGetCaseSubcases() {
        return new GetCaseSubcases();
    }

    /**
     * Create an instance of {@link GetCaseLogResp }
     * 
     */
    public GetCaseLogResp createGetCaseLogResp() {
        return new GetCaseLogResp();
    }

    /**
     * Create an instance of {@link EscalationIssueResp }
     * 
     */
    public EscalationIssueResp createEscalationIssueResp() {
        return new EscalationIssueResp();
    }

    /**
     * Create an instance of {@link TreatmentInfo }
     * 
     */
    public TreatmentInfo createTreatmentInfo() {
        return new TreatmentInfo();
    }

    /**
     * Create an instance of {@link GetScriptHeaderByType }
     * 
     */
    public GetScriptHeaderByType createGetScriptHeaderByType() {
        return new GetScriptHeaderByType();
    }

    /**
     * Create an instance of {@link CaseCount }
     * 
     */
    public CaseCount createCaseCount() {
        return new CaseCount();
    }

    /**
     * Create an instance of {@link GetSiteInfo }
     * 
     */
    public GetSiteInfo createGetSiteInfo() {
        return new GetSiteInfo();
    }

    /**
     * Create an instance of {@link CaseLog }
     * 
     */
    public CaseLog createCaseLog() {
        return new CaseLog();
    }

    /**
     * Create an instance of {@link CaseInfoResp }
     * 
     */
    public CaseInfoResp createCaseInfoResp() {
        return new CaseInfoResp();
    }

    /**
     * Create an instance of {@link AttachServicesToCase }
     * 
     */
    public AttachServicesToCase createAttachServicesToCase() {
        return new AttachServicesToCase();
    }

    /**
     * Create an instance of {@link GetScriptHeaderByTypeResponse }
     * 
     */
    public GetScriptHeaderByTypeResponse createGetScriptHeaderByTypeResponse() {
        return new GetScriptHeaderByTypeResponse();
    }

    /**
     * Create an instance of {@link Yank }
     * 
     */
    public Yank createYank() {
        return new Yank();
    }

    /**
     * Create an instance of {@link GetWholesalerSiteContacts }
     * 
     */
    public GetWholesalerSiteContacts createGetWholesalerSiteContacts() {
        return new GetWholesalerSiteContacts();
    }

    /**
     * Create an instance of {@link ChangeStatusResponse }
     * 
     */
    public ChangeStatusResponse createChangeStatusResponse() {
        return new ChangeStatusResponse();
    }

    /**
     * Create an instance of {@link ChangeStatus }
     * 
     */
    public ChangeStatus createChangeStatus() {
        return new ChangeStatus();
    }

    /**
     * Create an instance of {@link GetSiteInfoResp }
     * 
     */
    public GetSiteInfoResp createGetSiteInfoResp() {
        return new GetSiteInfoResp();
    }

    /**
     * Create an instance of {@link SearchByWholesalerResp }
     * 
     */
    public SearchByWholesalerResp createSearchByWholesalerResp() {
        return new SearchByWholesalerResp();
    }

    /**
     * Create an instance of {@link ArrayOfWholesalerAccounts }
     * 
     */
    public ArrayOfWholesalerAccounts createArrayOfWholesalerAccounts() {
        return new ArrayOfWholesalerAccounts();
    }

    /**
     * Create an instance of {@link UpdateContactRoleResponse }
     * 
     */
    public UpdateContactRoleResponse createUpdateContactRoleResponse() {
        return new UpdateContactRoleResponse();
    }

    /**
     * Create an instance of {@link GetScriptNextQuestionResponse }
     * 
     */
    public GetScriptNextQuestionResponse createGetScriptNextQuestionResponse() {
        return new GetScriptNextQuestionResponse();
    }

    /**
     * Create an instance of {@link CloseSubCase }
     * 
     */
    public CloseSubCase createCloseSubCase() {
        return new CloseSubCase();
    }

    /**
     * Create an instance of {@link UpdateContactRole }
     * 
     */
    public UpdateContactRole createUpdateContactRole() {
        return new UpdateContactRole();
    }

    /**
     * Create an instance of {@link ServiceHeader }
     * 
     */
    public ServiceHeader createServiceHeader() {
        return new ServiceHeader();
    }

    /**
     * Create an instance of {@link ArrayOfCaseScriptElement }
     * 
     */
    public ArrayOfCaseScriptElement createArrayOfCaseScriptElement() {
        return new ArrayOfCaseScriptElement();
    }

    /**
     * Create an instance of {@link TroubleTypes }
     * 
     */
    public TroubleTypes createTroubleTypes() {
        return new TroubleTypes();
    }

    /**
     * Create an instance of {@link GetContactInfoByNameResponse }
     * 
     */
    public GetContactInfoByNameResponse createGetContactInfoByNameResponse() {
        return new GetContactInfoByNameResponse();
    }

    /**
     * Create an instance of {@link CasesBySiteResp }
     * 
     */
    public CasesBySiteResp createCasesBySiteResp() {
        return new CasesBySiteResp();
    }

    /**
     * Create an instance of {@link SearchByWholesalerResponse }
     * 
     */
    public SearchByWholesalerResponse createSearchByWholesalerResponse() {
        return new SearchByWholesalerResponse();
    }

    /**
     * Create an instance of {@link GetContactInfoResp }
     * 
     */
    public GetContactInfoResp createGetContactInfoResp() {
        return new GetContactInfoResp();
    }

    /**
     * Create an instance of {@link GetWholesalerCasesResp }
     * 
     */
    public GetWholesalerCasesResp createGetWholesalerCasesResp() {
        return new GetWholesalerCasesResp();
    }

    /**
     * Create an instance of {@link LogNotes }
     * 
     */
    public LogNotes createLogNotes() {
        return new LogNotes();
    }

    /**
     * Create an instance of {@link GetEscalationIssue }
     * 
     */
    public GetEscalationIssue createGetEscalationIssue() {
        return new GetEscalationIssue();
    }

    /**
     * Create an instance of {@link PopupList }
     * 
     */
    public PopupList createPopupList() {
        return new PopupList();
    }

    /**
     * Create an instance of {@link YankResponse }
     * 
     */
    public YankResponse createYankResponse() {
        return new YankResponse();
    }

    /**
     * Create an instance of {@link ScriptHeader }
     * 
     */
    public ScriptHeader createScriptHeader() {
        return new ScriptHeader();
    }

    /**
     * Create an instance of {@link CreateSiteResponse }
     * 
     */
    public CreateSiteResponse createCreateSiteResponse() {
        return new CreateSiteResponse();
    }

    /**
     * Create an instance of {@link PopupListLevel1Resp }
     * 
     */
    public PopupListLevel1Resp createPopupListLevel1Resp() {
        return new PopupListLevel1Resp();
    }

    /**
     * Create an instance of {@link Issue }
     * 
     */
    public Issue createIssue() {
        return new Issue();
    }

    /**
     * Create an instance of {@link CloseCase }
     * 
     */
    public CloseCase createCloseCase() {
        return new CloseCase();
    }

    /**
     * Create an instance of {@link LogPhone }
     * 
     */
    public LogPhone createLogPhone() {
        return new LogPhone();
    }

    /**
     * Create an instance of {@link StandardResponse }
     * 
     */
    public StandardResponse createStandardResponse() {
        return new StandardResponse();
    }

    /**
     * Create an instance of {@link CreateSubcase }
     * 
     */
    public CreateSubcase createCreateSubcase() {
        return new CreateSubcase();
    }

    /**
     * Create an instance of {@link WholesalerAccounts }
     * 
     */
    public WholesalerAccounts createWholesalerAccounts() {
        return new WholesalerAccounts();
    }

    /**
     * Create an instance of {@link GetContactInfoByObjid }
     * 
     */
    public GetContactInfoByObjid createGetContactInfoByObjid() {
        return new GetContactInfoByObjid();
    }

    /**
     * Create an instance of {@link GetContactInfoByObjidResponse }
     * 
     */
    public GetContactInfoByObjidResponse createGetContactInfoByObjidResponse() {
        return new GetContactInfoByObjidResponse();
    }

    /**
     * Create an instance of {@link GetEscalationIssueResponse }
     * 
     */
    public GetEscalationIssueResponse createGetEscalationIssueResponse() {
        return new GetEscalationIssueResponse();
    }

    /**
     * Create an instance of {@link CreateInteractionResponse }
     * 
     */
    public CreateInteractionResponse createCreateInteractionResponse() {
        return new CreateInteractionResponse();
    }

    /**
     * Create an instance of {@link CreateContact }
     * 
     */
    public CreateContact createCreateContact() {
        return new CreateContact();
    }

    /**
     * Create an instance of {@link SubcaseInfo }
     * 
     */
    public SubcaseInfo createSubcaseInfo() {
        return new SubcaseInfo();
    }

    /**
     * Create an instance of {@link GetCaseInfoResponse }
     * 
     */
    public GetCaseInfoResponse createGetCaseInfoResponse() {
        return new GetCaseInfoResponse();
    }

    /**
     * Create an instance of {@link HgbstResp }
     * 
     */
    public HgbstResp createHgbstResp() {
        return new HgbstResp();
    }

    /**
     * Create an instance of {@link SiteElement }
     * 
     */
    public SiteElement createSiteElement() {
        return new SiteElement();
    }

    /**
     * Create an instance of {@link GetCaseScriptResponse }
     * 
     */
    public GetCaseScriptResponse createGetCaseScriptResponse() {
        return new GetCaseScriptResponse();
    }

    /**
     * Create an instance of {@link LogPhoneResponse }
     * 
     */
    public LogPhoneResponse createLogPhoneResponse() {
        return new LogPhoneResponse();
    }

    /**
     * Create an instance of {@link GetCaseLogsResponse }
     * 
     */
    public GetCaseLogsResponse createGetCaseLogsResponse() {
        return new GetCaseLogsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCasesBySite }
     * 
     */
    public ArrayOfCasesBySite createArrayOfCasesBySite() {
        return new ArrayOfCasesBySite();
    }

    /**
     * Create an instance of {@link ArrayOfCaseLog }
     * 
     */
    public ArrayOfCaseLog createArrayOfCaseLog() {
        return new ArrayOfCaseLog();
    }

    /**
     * Create an instance of {@link RemoveContactRole }
     * 
     */
    public RemoveContactRole createRemoveContactRole() {
        return new RemoveContactRole();
    }

    /**
     * Create an instance of {@link ScriptHeaderByTypeResp }
     * 
     */
    public ScriptHeaderByTypeResp createScriptHeaderByTypeResp() {
        return new ScriptHeaderByTypeResp();
    }

    /**
     * Create an instance of {@link GetCaseScript }
     * 
     */
    public GetCaseScript createGetCaseScript() {
        return new GetCaseScript();
    }

    /**
     * Create an instance of {@link SearchByWholesaler }
     * 
     */
    public SearchByWholesaler createSearchByWholesaler() {
        return new SearchByWholesaler();
    }

    /**
     * Create an instance of {@link ContactInformation }
     * 
     */
    public ContactInformation createContactInformation() {
        return new ContactInformation();
    }

    /**
     * Create an instance of {@link GetSiteInfoResponse }
     * 
     */
    public GetSiteInfoResponse createGetSiteInfoResponse() {
        return new GetSiteInfoResponse();
    }

    /**
     * Create an instance of {@link GetSubcaseInfoResponse }
     * 
     */
    public GetSubcaseInfoResponse createGetSubcaseInfoResponse() {
        return new GetSubcaseInfoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSubcaseInfo }
     * 
     */
    public ArrayOfSubcaseInfo createArrayOfSubcaseInfo() {
        return new ArrayOfSubcaseInfo();
    }

    /**
     * Create an instance of {@link SiteInformation }
     * 
     */
    public SiteInformation createSiteInformation() {
        return new SiteInformation();
    }

    /**
     * Create an instance of {@link CreateInteraction }
     * 
     */
    public CreateInteraction createCreateInteraction() {
        return new CreateInteraction();
    }

    /**
     * Create an instance of {@link GetSiteContactsResponse }
     * 
     */
    public GetSiteContactsResponse createGetSiteContactsResponse() {
        return new GetSiteContactsResponse();
    }

    /**
     * Create an instance of {@link CreateCase }
     * 
     */
    public CreateCase createCreateCase() {
        return new CreateCase();
    }

    /**
     * Create an instance of {@link ArrayOfHgbstElement }
     * 
     */
    public ArrayOfHgbstElement createArrayOfHgbstElement() {
        return new ArrayOfHgbstElement();
    }

    /**
     * Create an instance of {@link ArrayOfIssue }
     * 
     */
    public ArrayOfIssue createArrayOfIssue() {
        return new ArrayOfIssue();
    }

    /**
     * Create an instance of {@link GetWholesalerCases }
     * 
     */
    public GetWholesalerCases createGetWholesalerCases() {
        return new GetWholesalerCases();
    }

    /**
     * Create an instance of {@link CloseCaseResponse }
     * 
     */
    public CloseCaseResponse createCloseCaseResponse() {
        return new CloseCaseResponse();
    }

    /**
     * Create an instance of {@link GetHgbstListResponse }
     * 
     */
    public GetHgbstListResponse createGetHgbstListResponse() {
        return new GetHgbstListResponse();
    }

    /**
     * Create an instance of {@link GetCasesBySiteResponse }
     * 
     */
    public GetCasesBySiteResponse createGetCasesBySiteResponse() {
        return new GetCasesBySiteResponse();
    }

    /**
     * Create an instance of {@link CreateContactRoleResponse }
     * 
     */
    public CreateContactRoleResponse createCreateContactRoleResponse() {
        return new CreateContactRoleResponse();
    }

    /**
     * Create an instance of {@link ExtendedCaseAttribute }
     * 
     */
    public ExtendedCaseAttribute createExtendedCaseAttribute() {
        return new ExtendedCaseAttribute();
    }

    /**
     * Create an instance of {@link AlternateContactInfo }
     * 
     */
    public AlternateContactInfo createAlternateContactInfo() {
        return new AlternateContactInfo();
    }

    /**
     * Create an instance of {@link UpdateSiteResponse }
     * 
     */
    public UpdateSiteResponse createUpdateSiteResponse() {
        return new UpdateSiteResponse();
    }

    /**
     * Create an instance of {@link GetHgbstList }
     * 
     */
    public GetHgbstList createGetHgbstList() {
        return new GetHgbstList();
    }

    /**
     * Create an instance of {@link AssignResponse }
     * 
     */
    public AssignResponse createAssignResponse() {
        return new AssignResponse();
    }

    /**
     * Create an instance of {@link CaseCountResp }
     * 
     */
    public CaseCountResp createCaseCountResp() {
        return new CaseCountResp();
    }

    /**
     * Create an instance of {@link AddressElement }
     * 
     */
    public AddressElement createAddressElement() {
        return new AddressElement();
    }

    /**
     * Create an instance of {@link HgbstElement }
     * 
     */
    public HgbstElement createHgbstElement() {
        return new HgbstElement();
    }

    /**
     * Create an instance of {@link ArrayOfSiteContacts }
     * 
     */
    public ArrayOfSiteContacts createArrayOfSiteContacts() {
        return new ArrayOfSiteContacts();
    }

    /**
     * Create an instance of {@link SiteContactsResp }
     * 
     */
    public SiteContactsResp createSiteContactsResp() {
        return new SiteContactsResp();
    }

    /**
     * Create an instance of {@link SiteContacts }
     * 
     */
    public SiteContacts createSiteContacts() {
        return new SiteContacts();
    }

    /**
     * Create an instance of {@link GetContactInfoByName }
     * 
     */
    public GetContactInfoByName createGetContactInfoByName() {
        return new GetContactInfoByName();
    }

    /**
     * Create an instance of {@link GetScriptQuestion }
     * 
     */
    public GetScriptQuestion createGetScriptQuestion() {
        return new GetScriptQuestion();
    }

    /**
     * Create an instance of {@link GetScriptNextQuestion }
     * 
     */
    public GetScriptNextQuestion createGetScriptNextQuestion() {
        return new GetScriptNextQuestion();
    }

}
