
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetEscalationIssueResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}EscalationIssueResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getEscalationIssueResult"
})
@XmlRootElement(name = "GetEscalationIssueResponse")
public class GetEscalationIssueResponse {

    @XmlElement(name = "GetEscalationIssueResult", required = true)
    protected EscalationIssueResp getEscalationIssueResult;

    /**
     * Gets the value of the getEscalationIssueResult property.
     * 
     * @return
     *     possible object is
     *     {@link EscalationIssueResp }
     *     
     */
    public EscalationIssueResp getGetEscalationIssueResult() {
        return getEscalationIssueResult;
    }

    /**
     * Sets the value of the getEscalationIssueResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link EscalationIssueResp }
     *     
     */
    public void setGetEscalationIssueResult(EscalationIssueResp value) {
        this.getEscalationIssueResult = value;
    }

}
