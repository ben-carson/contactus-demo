
package com.windstream.bus.ws.client.ClarifyClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWholesalerCase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWholesalerCase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WholesalerCase" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}WholesalerCase" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWholesalerCase", propOrder = {
    "wholesalerCase"
})
public class ArrayOfWholesalerCase {

    @XmlElement(name = "WholesalerCase")
    protected List<WholesalerCase> wholesalerCase;

    /**
     * Gets the value of the wholesalerCase property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wholesalerCase property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWholesalerCase().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WholesalerCase }
     * 
     * 
     */
    public List<WholesalerCase> getWholesalerCase() {
        if (wholesalerCase == null) {
            wholesalerCase = new ArrayList<WholesalerCase>();
        }
        return this.wholesalerCase;
    }

}
