
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScriptResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScriptResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseObjid" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="SequenceNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ResponseText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Score" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="IsDefault" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsEditable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScriptResp", propOrder = {
    "responseObjid",
    "sequenceNo",
    "responseText",
    "score",
    "isDefault",
    "isEditable"
})
public class ScriptResp {

    @XmlElement(name = "ResponseObjid")
    protected long responseObjid;
    @XmlElement(name = "SequenceNo")
    protected long sequenceNo;
    @XmlElement(name = "ResponseText")
    protected String responseText;
    @XmlElement(name = "Score")
    protected long score;
    @XmlElement(name = "IsDefault")
    protected boolean isDefault;
    @XmlElement(name = "IsEditable")
    protected boolean isEditable;

    /**
     * Gets the value of the responseObjid property.
     * 
     */
    public long getResponseObjid() {
        return responseObjid;
    }

    /**
     * Sets the value of the responseObjid property.
     * 
     */
    public void setResponseObjid(long value) {
        this.responseObjid = value;
    }

    /**
     * Gets the value of the sequenceNo property.
     * 
     */
    public long getSequenceNo() {
        return sequenceNo;
    }

    /**
     * Sets the value of the sequenceNo property.
     * 
     */
    public void setSequenceNo(long value) {
        this.sequenceNo = value;
    }

    /**
     * Gets the value of the responseText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * Sets the value of the responseText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseText(String value) {
        this.responseText = value;
    }

    /**
     * Gets the value of the score property.
     * 
     */
    public long getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     * 
     */
    public void setScore(long value) {
        this.score = value;
    }

    /**
     * Gets the value of the isDefault property.
     * 
     */
    public boolean isIsDefault() {
        return isDefault;
    }

    /**
     * Sets the value of the isDefault property.
     * 
     */
    public void setIsDefault(boolean value) {
        this.isDefault = value;
    }

    /**
     * Gets the value of the isEditable property.
     * 
     */
    public boolean isIsEditable() {
        return isEditable;
    }

    /**
     * Sets the value of the isEditable property.
     * 
     */
    public void setIsEditable(boolean value) {
        this.isEditable = value;
    }

}
