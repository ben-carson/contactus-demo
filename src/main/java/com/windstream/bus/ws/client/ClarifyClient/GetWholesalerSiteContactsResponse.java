
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetWholesalerSiteContactsResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}SiteContactsResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getWholesalerSiteContactsResult"
})
@XmlRootElement(name = "GetWholesalerSiteContactsResponse")
public class GetWholesalerSiteContactsResponse {

    @XmlElement(name = "GetWholesalerSiteContactsResult", required = true)
    protected SiteContactsResp getWholesalerSiteContactsResult;

    /**
     * Gets the value of the getWholesalerSiteContactsResult property.
     * 
     * @return
     *     possible object is
     *     {@link SiteContactsResp }
     *     
     */
    public SiteContactsResp getGetWholesalerSiteContactsResult() {
        return getWholesalerSiteContactsResult;
    }

    /**
     * Sets the value of the getWholesalerSiteContactsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SiteContactsResp }
     *     
     */
    public void setGetWholesalerSiteContactsResult(SiteContactsResp value) {
        this.getWholesalerSiteContactsResult = value;
    }

}
