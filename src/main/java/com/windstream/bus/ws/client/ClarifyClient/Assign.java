
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ObjectName" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}WorkflowObject"/>
 *         &lt;element name="AssignedToUserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssignedByUserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WipBin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idNumber",
    "objectName",
    "assignedToUserName",
    "assignedByUserName",
    "wipBin"
})
@XmlRootElement(name = "Assign")
public class Assign {

    @XmlElement(name = "IDNumber")
    protected String idNumber;
    @XmlElement(name = "ObjectName", required = true)
    protected WorkflowObject objectName;
    @XmlElement(name = "AssignedToUserName")
    protected String assignedToUserName;
    @XmlElement(name = "AssignedByUserName")
    protected String assignedByUserName;
    @XmlElement(name = "WipBin")
    protected String wipBin;

    /**
     * Gets the value of the idNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDNumber() {
        return idNumber;
    }

    /**
     * Sets the value of the idNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDNumber(String value) {
        this.idNumber = value;
    }

    /**
     * Gets the value of the objectName property.
     * 
     * @return
     *     possible object is
     *     {@link WorkflowObject }
     *     
     */
    public WorkflowObject getObjectName() {
        return objectName;
    }

    /**
     * Sets the value of the objectName property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkflowObject }
     *     
     */
    public void setObjectName(WorkflowObject value) {
        this.objectName = value;
    }

    /**
     * Gets the value of the assignedToUserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedToUserName() {
        return assignedToUserName;
    }

    /**
     * Sets the value of the assignedToUserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedToUserName(String value) {
        this.assignedToUserName = value;
    }

    /**
     * Gets the value of the assignedByUserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedByUserName() {
        return assignedByUserName;
    }

    /**
     * Sets the value of the assignedByUserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedByUserName(String value) {
        this.assignedByUserName = value;
    }

    /**
     * Gets the value of the wipBin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWipBin() {
        return wipBin;
    }

    /**
     * Sets the value of the wipBin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWipBin(String value) {
        this.wipBin = value;
    }

}
