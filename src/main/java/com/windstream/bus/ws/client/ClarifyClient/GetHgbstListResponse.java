
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetHgbstListResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}HgbstResp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getHgbstListResult"
})
@XmlRootElement(name = "GetHgbstListResponse")
public class GetHgbstListResponse {

    @XmlElement(name = "GetHgbstListResult", required = true)
    protected HgbstResp getHgbstListResult;

    /**
     * Gets the value of the getHgbstListResult property.
     * 
     * @return
     *     possible object is
     *     {@link HgbstResp }
     *     
     */
    public HgbstResp getGetHgbstListResult() {
        return getHgbstListResult;
    }

    /**
     * Sets the value of the getHgbstListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link HgbstResp }
     *     
     */
    public void setGetHgbstListResult(HgbstResp value) {
        this.getHgbstListResult = value;
    }

}
