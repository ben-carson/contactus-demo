
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdateSiteResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateSiteResult"
})
@XmlRootElement(name = "UpdateSiteResponse")
public class UpdateSiteResponse {

    @XmlElement(name = "UpdateSiteResult", required = true)
    protected StandardResponse updateSiteResult;

    /**
     * Gets the value of the updateSiteResult property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getUpdateSiteResult() {
        return updateSiteResult;
    }

    /**
     * Sets the value of the updateSiteResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setUpdateSiteResult(StandardResponse value) {
        this.updateSiteResult = value;
    }

}
