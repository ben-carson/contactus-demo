
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeStatusResult" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}StandardResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "changeStatusResult"
})
@XmlRootElement(name = "ChangeStatusResponse")
public class ChangeStatusResponse {

    @XmlElement(name = "ChangeStatusResult", required = true)
    protected StandardResponse changeStatusResult;

    /**
     * Gets the value of the changeStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link StandardResponse }
     *     
     */
    public StandardResponse getChangeStatusResult() {
        return changeStatusResult;
    }

    /**
     * Sets the value of the changeStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardResponse }
     *     
     */
    public void setChangeStatusResult(StandardResponse value) {
        this.changeStatusResult = value;
    }

}
