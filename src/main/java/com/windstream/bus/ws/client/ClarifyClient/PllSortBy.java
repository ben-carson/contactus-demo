
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pllSortBy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="pllSortBy">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Rank"/>
 *     &lt;enumeration value="Title"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "pllSortBy")
@XmlEnum
public enum PllSortBy {

    @XmlEnumValue("Rank")
    RANK("Rank"),
    @XmlEnumValue("Title")
    TITLE("Title");
    private final String value;

    PllSortBy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PllSortBy fromValue(String v) {
        for (PllSortBy c: PllSortBy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
