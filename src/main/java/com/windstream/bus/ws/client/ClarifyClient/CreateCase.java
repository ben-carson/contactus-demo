
package com.windstream.bus.ws.client.ClarifyClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TroubleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TroubleSubType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TroubleSubTypeCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FSMarket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Severity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CaseType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhoneLog" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Queue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreatorLogin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LockTitle" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="AllowEmailSurvey" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SurveyEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowEmailStatusChange" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="StatusChangeEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TreatmentInfo" type="{http://clarifyservice.windstream.com/ClarifyServiceManager/}TreatmentElement"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "siteID",
    "contactFirstName",
    "contactLastName",
    "contactPhone",
    "title",
    "troubleType",
    "troubleSubType",
    "troubleSubTypeCategory",
    "fsMarket",
    "priority",
    "severity",
    "status",
    "caseType",
    "phoneLog",
    "queue",
    "creatorLogin",
    "lockTitle",
    "allowEmailSurvey",
    "surveyEmail",
    "allowEmailStatusChange",
    "statusChangeEmail",
    "treatmentInfo"
})
@XmlRootElement(name = "CreateCase")
public class CreateCase {

    @XmlElement(name = "SiteID")
    protected String siteID;
    @XmlElement(name = "ContactFirstName")
    protected String contactFirstName;
    @XmlElement(name = "ContactLastName")
    protected String contactLastName;
    @XmlElement(name = "ContactPhone")
    protected String contactPhone;
    @XmlElement(name = "Title")
    protected String title;
    @XmlElement(name = "TroubleType")
    protected String troubleType;
    @XmlElement(name = "TroubleSubType")
    protected String troubleSubType;
    @XmlElement(name = "TroubleSubTypeCategory")
    protected String troubleSubTypeCategory;
    @XmlElement(name = "FSMarket")
    protected String fsMarket;
    @XmlElement(name = "Priority")
    protected String priority;
    @XmlElement(name = "Severity")
    protected String severity;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "CaseType")
    protected String caseType;
    @XmlElement(name = "PhoneLog")
    protected String phoneLog;
    @XmlElement(name = "Queue")
    protected String queue;
    @XmlElement(name = "CreatorLogin")
    protected String creatorLogin;
    @XmlElement(name = "LockTitle", required = true, type = Boolean.class, nillable = true)
    protected Boolean lockTitle;
    @XmlElement(name = "AllowEmailSurvey", required = true, type = Boolean.class, nillable = true)
    protected Boolean allowEmailSurvey;
    @XmlElement(name = "SurveyEmail")
    protected String surveyEmail;
    @XmlElement(name = "AllowEmailStatusChange", required = true, type = Boolean.class, nillable = true)
    protected Boolean allowEmailStatusChange;
    @XmlElement(name = "StatusChangeEmail")
    protected String statusChangeEmail;
    @XmlElement(name = "TreatmentInfo", required = true, nillable = true)
    protected TreatmentElement treatmentInfo;

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteID(String value) {
        this.siteID = value;
    }

    /**
     * Gets the value of the contactFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactFirstName() {
        return contactFirstName;
    }

    /**
     * Sets the value of the contactFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactFirstName(String value) {
        this.contactFirstName = value;
    }

    /**
     * Gets the value of the contactLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactLastName() {
        return contactLastName;
    }

    /**
     * Sets the value of the contactLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactLastName(String value) {
        this.contactLastName = value;
    }

    /**
     * Gets the value of the contactPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * Sets the value of the contactPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPhone(String value) {
        this.contactPhone = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the troubleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTroubleType() {
        return troubleType;
    }

    /**
     * Sets the value of the troubleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTroubleType(String value) {
        this.troubleType = value;
    }

    /**
     * Gets the value of the troubleSubType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTroubleSubType() {
        return troubleSubType;
    }

    /**
     * Sets the value of the troubleSubType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTroubleSubType(String value) {
        this.troubleSubType = value;
    }

    /**
     * Gets the value of the troubleSubTypeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTroubleSubTypeCategory() {
        return troubleSubTypeCategory;
    }

    /**
     * Sets the value of the troubleSubTypeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTroubleSubTypeCategory(String value) {
        this.troubleSubTypeCategory = value;
    }

    /**
     * Gets the value of the fsMarket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFSMarket() {
        return fsMarket;
    }

    /**
     * Sets the value of the fsMarket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFSMarket(String value) {
        this.fsMarket = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Gets the value of the severity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * Sets the value of the severity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeverity(String value) {
        this.severity = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the caseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseType() {
        return caseType;
    }

    /**
     * Sets the value of the caseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseType(String value) {
        this.caseType = value;
    }

    /**
     * Gets the value of the phoneLog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneLog() {
        return phoneLog;
    }

    /**
     * Sets the value of the phoneLog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneLog(String value) {
        this.phoneLog = value;
    }

    /**
     * Gets the value of the queue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueue() {
        return queue;
    }

    /**
     * Sets the value of the queue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueue(String value) {
        this.queue = value;
    }

    /**
     * Gets the value of the creatorLogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatorLogin() {
        return creatorLogin;
    }

    /**
     * Sets the value of the creatorLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatorLogin(String value) {
        this.creatorLogin = value;
    }

    /**
     * Gets the value of the lockTitle property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLockTitle() {
        return lockTitle;
    }

    /**
     * Sets the value of the lockTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLockTitle(Boolean value) {
        this.lockTitle = value;
    }

    /**
     * Gets the value of the allowEmailSurvey property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowEmailSurvey() {
        return allowEmailSurvey;
    }

    /**
     * Sets the value of the allowEmailSurvey property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowEmailSurvey(Boolean value) {
        this.allowEmailSurvey = value;
    }

    /**
     * Gets the value of the surveyEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurveyEmail() {
        return surveyEmail;
    }

    /**
     * Sets the value of the surveyEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurveyEmail(String value) {
        this.surveyEmail = value;
    }

    /**
     * Gets the value of the allowEmailStatusChange property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowEmailStatusChange() {
        return allowEmailStatusChange;
    }

    /**
     * Sets the value of the allowEmailStatusChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowEmailStatusChange(Boolean value) {
        this.allowEmailStatusChange = value;
    }

    /**
     * Gets the value of the statusChangeEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusChangeEmail() {
        return statusChangeEmail;
    }

    /**
     * Sets the value of the statusChangeEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusChangeEmail(String value) {
        this.statusChangeEmail = value;
    }

    /**
     * Gets the value of the treatmentInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TreatmentElement }
     *     
     */
    public TreatmentElement getTreatmentInfo() {
        return treatmentInfo;
    }

    /**
     * Sets the value of the treatmentInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TreatmentElement }
     *     
     */
    public void setTreatmentInfo(TreatmentElement value) {
        this.treatmentInfo = value;
    }

}
