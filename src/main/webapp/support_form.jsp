<pt:namespace pt:token="$PORTLET_ID$" xmlns:pt='http://www.plumtree.com/xmlschemas/ptui/'/>
<%@ page language="java" import="java.util.*" import="java.util.regex.*" pageEncoding="ISO-8859-1"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Contact Us</title>
		<script type="text/javascript" language="JavaScript">
			function show_mac() {
				// Make sure the element exists before calling it's properties
				if ((document.getElementById("macTbl") != null)&&
						(document.getElementById("billTbl") != null)&&
						(document.getElementById("techTbl") != null)&&
						(document.getElementById("nonTbl") != null)) {
					document.getElementById("macTbl").style.display = 'block';
					document.getElementById("billTbl").style.display = 'none';
					document.getElementById("techTbl").style.display = 'none';
					document.getElementById("nonTbl").style.display = 'none';
				}
			}
			function show_bill() {
				// Make sure the element exists before calling it's properties
				if ((document.getElementById("macTbl") != null)&&
				   	    (document.getElementById("billTbl") != null)&&
				   	    (document.getElementById("techTbl") != null)&&
				   	    (document.getElementById("nonTbl") != null)) {
					document.getElementById("macTbl").style.display = 'none';
					document.getElementById("billTbl").style.display = 'block';
					document.getElementById("techTbl").style.display = 'none';
					document.getElementById("nonTbl").style.display = 'none';
				}
			}
			function show_tech() {
				// Make sure the element exists before calling it's properties
				if ((document.getElementById("macTbl") != null)&&
						(document.getElementById("billTbl") != null)&&
						(document.getElementById("techTbl") != null)&&
						(document.getElementById("nonTbl") != null)) {
					document.getElementById("macTbl").style.display = 'none';
					document.getElementById("billTbl").style.display = 'none';
					document.getElementById("techTbl").style.display = 'block';
					document.getElementById("nonTbl").style.display = 'none';
				}
			}
		  
			function show_non() {
				// Make sure the element exists before calling it's properties
				if ((document.getElementById("macTbl") != null)&&
						(document.getElementById("billTbl") != null)&&
						(document.getElementById("techTbl") != null)&&
						(document.getElementById("nonTbl") != null)) {
			    	document.getElementById("macTbl").style.display = 'none';
					document.getElementById("billTbl").style.display = 'none';
					document.getElementById("techTbl").style.display = 'none';
					document.getElementById("nonTbl").style.display = 'block';
				}
			}
		  
			function validate_time() {
				var today = new Date();
				HR = today.getHours();
				Min = today.getMinutes();
				Day = today.getDay();
				
				if (HR == 17 && Min < 31 && Day != 0 && Day != 6) {
					document.getElementById("Chat").style.visibility = "visible";
					document.getElementById("Chat2").style.visibility = "visible";
				} else if (HR > 7 && HR < 17 && Day != 0 && Day != 6) {
					document.getElementById("Chat").style.visibility = "visible";
					document.getElementById("Chat2").style.visibility = "visible";
				}
			}
		
			function validate_form(thisform) {
				with (thisform) {
				  	if (validate_required(nv_accountnum,"Account Number must be filled out!")==false) {
				  	  	nv_accountnum.focus();return false;
				  	} else if (validate_account(nv_accountnum,"Account Number must be numbers only!")==false) {
				  	  	nv_accountnum.focus();return false;
				  	} else if (validate_required(aicEscDisplayName,"Contact Name must be filled out!")==false) {
				  	  	aicEscDisplayName.focus();return false;
				  	} else if (validate_name(aicEscDisplayName,"Contact Name must be in letters only!")==false) {
				  	  	aicEscDisplayName.focus();return false;
				  	} else if (validate_required(aicEscContact,"Contact Email must be filled out!")==false) {
				  	  	aicEscContact.focus();return false;
				  	} else if (validate_email(aicEscContact,"Not a valid e-mail address!")==false) {
				  	  	aicEscContact.focus();return false;
				  	} else if (validate_required(phone,"Contact Phone Number must be filled out!")==false) {
				  	  	phone.focus();return false;
				  	} else if (validate_phone(phone,"Please enter a phone number with the format xxx-xxx-xxxx.")==false) {
				  	  	phone.focus();return false;
				  	} else if (validate_required(aicEscQuestion,"Please enter what you need assistance with.")==false) {
				  		aicEscQuestion.focus();return false;
				  	}
				}
		   	}
			
			function validate_required(field,alerttxt) {
				with (field) {
			  		if (value==null||value=="") {
				    	alert(alerttxt);return false;
				    } else {
			    		return true;
			    	}
			    }
		    }
		 	   	
		    function validate_account(field,alerttxt) {
				with (field) {
			  		var namevalid=/^[+]?\d*$/;
			  		if(value.search(namevalid)==-1)	{
				  		alert(alerttxt);
				  		return false;
			  		} else {
			  	  		return true;
			  	  	}
		  		}
			}
		    	
		    function validate_name(field,alerttxt) {
				with (field) {
			  		var namevalid=/^[a-zA-Z ]+$/;
			  		if(value.search(namevalid)==-1) {
			  			alert(alerttxt);
			  			return false;
			  		}else{
				  		return true;
				  	}
			
		  		}
			}
		    
		    function validate_email(field,alerttxt)	{
				with (field) {
			  		apos=value.indexOf("@");
			  		dotpos=value.lastIndexOf(".");
			  		if (apos<1||dotpos-apos<2) {
				  		alert(alerttxt);
				  		return false;
				  	} else {
					  	return true;
					}
		  		}
			}
				
			function validate_phone(field,alerttxt) {
				with (field) {
			  		var phonevalid=/^(\d{3})[- ]?(\d{3})[- ]?(\d{4})$/;
			  		if(value.search(phonevalid)==-1) {
				  		alert(alerttxt);
				  		return false;
			  		} else {
				  		return true;
				  	}
				}
			}
		</script>
	</head>

	<body onload="validate_time()">
	<div id="container">
		<!--header-->
		<div id="content_base">
			<div id="center_content">
				<div class="content">
					<p>We are a communications company and we make contacting us easy! For our customers, we've listed the most frequently called groups below. </p>
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
				        <tr>
							<td width="9%"><input name="action" type="radio" value="mac" onClick="show_mac();"  /></td>
							<td width="91%"><strong>Making changes to your current account and services</strong></td>
				        </tr>
				        <tr>
							<td><input type="radio" name="action" id="billing" value="billing"  onClick="show_bill();" /></td>
							<td><strong>Billing questions regarding your account</strong></td>
				        </tr>
				        <tr>
							<td><input type="radio" name="action" id="tech" value="tech"  onClick="show_tech();" /></td>
							<td><strong>Technical support for your services</strong></td>
				        </tr>
				        <tr>
							<td><input type="radio" name="action" value="noncrit"  onClick="show_non();" /></td>
							<td><strong>Non-critical issues</strong><em> (Please allow 48 hours for a response)</em></td>
				        </tr>
					</table>
	      			<br />
					<!-- MAC -->
					<form action="" onSubmit="return validate_form(this);" method="get" name="frm$PORTLET_ID$"  id="macInfo">
						<!--mac table -->
						<table width="100%" border="0" cellspacing="0" cellpadding="3" id="macTbl" style='display:none;'>
							<tr>
								<td colspan="2"><h2>Making Changes</h2>
								<input name="nv_tickettype" type="hidden" id="nv_tickettype" value="MAC" maxlength="10" class="formStyle" />
								</td>
							</tr>
							<tr>
								<td width="50%" align="right"><strong>Fields marked with * are required.</strong></td>
							</tr>
							<tr>
								<td width="36%" align="right"><strong>Account Number*:</strong></td>
								<td width="64%"><input name="nv_accountnum" type="text" id="nv_accountnum" value="" class="formStyle" /></td>
							</tr>
							<tr>
								<td align="right"><strong>Contact First and Last Name*:</strong></td>
								<td><input name="aicEscDisplayName" type="text" id="aicEscDisplayName" value="" class="formStyle" /></td>
							</tr>
					        <tr>
								<td align="right"><strong>Contact Email*:</strong></td>
								<td>
									<span id="sprytextfield1">
										<input type="text" name="aicEscContact" id="aicEscContact" value="" class="formStyle" />
										<span class="textfieldInvalidFormatMsg"></span>
								 	</span>
								</td>
					        </tr>
							<tr> 
								<td align="right"><strong>Contact Phone No.*:</strong></td>
								<td>
									<input name="phone" type="text" id="phone" value="" maxlength="12" class="formStyle" />
									<strong>(format xxx-xxx-xxxx)</strong>
								</td>
							</tr>
							<tr>
								<td align="right"><strong>Ticket Number:</strong></td>
								<td><input name="nv_ticketnum" type="text" id="nv_ticketnum" value="" maxlength="20" class="formStyle" /></td>
							</tr>
					        <tr>
								<td align="right"><strong>PSR Number:</strong></td>
								<td><input name="nv_psrnum" type="text" id="nv_psrnum" value="" maxlength="20" class="formStyle" /></td>
					        </tr>
					        <tr>
								<td align="right"><strong>ICare Order Number:</strong></td>
								<td><input name="nv_icareordernum" type="text" id="nv_icareordernum" value="" maxlength="20" class="formStyle" /></td>
					        </tr>
					        <tr>
								<td align="right" valign="top"><strong>I need assistance with*:</strong></td>
								<td><textarea name="aicEscQuestion" id="aicEscQuestion" cols="45" rows="5"></textarea></td>
					        </tr>
							<tr>
								<td colspan="2" align="center">
									<input type="submit"  name="Email" id="Email" value="Email"  onClick="this.form.action='servlet/support_email'"  />
									<input type="submit" style="visibility:hidden" name="Chat" id="Chat" value="Chat" onClick="this.form.action='servlet/support_link'"  />
								</td>
							</tr>
						</table>
					</form>
					<!-- BILLING -->
					<form action=""  onSubmit="return validate_form(this);" method="get" name="frm$PORTLET_ID$"  id="billInfo">
						<!--billing table -->
						<table width="100%" border="0" cellspacing="0" cellpadding="3" id="billTbl" style='display:none;'>
							<tr>
								<td colspan="2">
									<h2>Billing Inquiries</h2>
									<input name="nv_tickettype" type="hidden" id="nv_tickettype" value="Billing" maxlength="10" class="formStyle" />
								</td>
							</tr>
							<tr>
								<td width="50%" align="right"><strong> Fields marked with * are required.</strong></td>
							</tr>
							<tr>
								<td width="36%" align="right"><strong>Account Number*:</strong></td>
								<td width="64%"><input name="nv_accountnum" type="text" id="nv_accountnum" value="" class="formStyle" /></td>
							</tr>
							<tr>
								<td align="right"><strong>Contact First and Last Name*:</strong></td>
								<td><input name="aicEscDisplayName" type="text" id="aicEscDisplayName" value="" class="formStyle" /></td>
							</tr>
							<tr>
								<td align="right"><strong>Contact Email*:</strong></td>
								<td>
									<span id="sprytextfield1">
									<input type="text" name="aicEscContact" id="aicEscContact" value="" class="formStyle"><span class="textfieldInvalidFormatMsg"></span></span>
								</td>
							</tr>
							<tr>
								<td align="right"><strong>Contact Phone No.*:</strong></td>
								<td>
									<input name="phone" type="text" id="phone" value="" maxlength="12" class="formStyle" />
									<strong>(format xxx-xxx-xxxx)</strong>
								</td>
							</tr>
							<tr>
								<td align="right"><strong>Ticket Number:</strong></td>
								<td>
									<input name="nv_ticketnum" type="text" id="nv_ticketnum" value="" maxlength="20" class="formStyle" />
								</td>
							</tr>
							<tr>
								<td align="right"><strong>PSR Number:</strong></td>
								<td>
									<input name="nv_psrnum" type="text" id="nv_psrnum" value="" maxlength="20" class="formStyle" />
								</td>
							</tr>
							<tr>
								<td align="right"><strong>ICare Order Number:</strong></td>
								<td>
									<input name="nv_icareordernum" type="text" id="nv_icareordernum" value="" maxlength="20" class="formStyle" />
								</td>
							</tr>
							<tr>
								<td align="right" valign="top"><strong>I need assistance with*:</strong></td>
								<td><textarea name="aicEscQuestion" id="aicEscQuestion" cols="45" rows="5"></textarea></td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="submit" name="Billing" id="Billing" value="Email"  onClick="this.form.action='servlet/support_email'"  />
									<input type="submit" style="visibility:hidden"  name="Billing" id="Chat2" value="Chat"  onClick="this.form.action='servlet/support_link'"/>
								</td>
							</tr>
						</table>
					</form>
				
					<!--tech table -->
					<table width="100%" border="0" name="frm$PORTLET_ID$" cellspacing="0" cellpadding="3" id="techTbl" style='display:none;'>
						<tr>
							<td><h2>Technical Support</h2></td>
						</tr>
						<tr>
							<td width="36%">
								<p>For the fastest service, please visit our <a href="https://my.nuvox.net/portal/server.pt" target="_blank">&quot;I'm a Customer&quot;</a> site and submit an <a href="http://my.nuvox.net/" target="_blank">eTicket.</a></p>            <p><strong>Or call</strong>:<br />
								T: 800.600.5050<br />
								Hours: 24 x 7 </p>
							</td>
						</tr>
					</table>
	        	
					<!--non crit table -->
					<table width="100%" border="0" name="frm$PORTLET_ID$" cellspacing="0" cellpadding="3" id="nonTbl" style='display:none;'>
						<tr>
							<td><h2>Non-critical Issues</h2></td>
						</tr>
				        <tr>
							<td width="36%">
								<p>If you have a question that is not critical, you may choose to email Windstream.<br />
				            	<a href="mailto:customercare@nuvox.com?subject=Customer Care Inquiry from www.nuvox.com">Click to Email. </a>
				            	</p>
				            	<p>(<strong>Please allow 48 hours for a response.</strong>)</p>
							</td>
				        </tr>
				    </table>
					<br />
	    		</div> <!-- end 'content' div -->
	    	</div> <!-- end 'center_content' div -->
		</div> <!-- end 'content_base' div -->
	</div> <!-- end 'container' div -->
	</body>
</html>


