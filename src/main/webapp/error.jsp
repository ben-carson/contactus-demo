<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Error Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
</head>

<body>
<div id="amResults">
<h3>ERROR</h3>
<dl>
	<dt>ActionError:</dt>
	<dd><s:actionerror name="errors"></s:actionerror></dd>
	<dt>Exception Name:</dt>
	<dd>&nbsp;<s:property value="exception" /></dd>

	<dt>What you did wrong:</dt>
	<dd><s:if test="exceptionStack.length>0">
		<s:iterator id="next" value="exceptionStack" status="stat">
			<span><s:property value="exceptionStack[#stat.index]" /></span>
			<br />
		</s:iterator>
	</s:if> <s:else>
		<s:text name="No exception stack to display." />
	</s:else></dd>
</dl>
</div>
</body>
</html>
