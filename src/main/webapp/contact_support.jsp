<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
	<head>
		<link href="./css/clarifyStyles.css" rel="stylesheet" type="text/css"></link>
		<script type="text/javascript" src="./js/LAB.min.js"></script>
		<script type="text/javascript">
			$LAB
				.script("http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js").wait()
				.script("./js/json2.js").wait()
				.script("./js/jquery.form.js")
				.script("./js/jquery-select-functions.js")
				.script("./js/dev/cufon-yui.js").wait()
				.script("./js/dev/ie-fixes.js").wait()
				.script("./js/dev/VistaSans_500-VistaSans_700.font.js").wait()
				.script("./js/jquery-controls.js");
		</script>
		<title>Contact Windstream</title>
	</head>
	<body>
		<div id="mainContent">
			<h3 id="introDiv">
				We are a communications company and we make contacting us easy!
			</h3>
			<div id="formStatus"></div>
			<div id="clarifyCaseFormDiv">
				<s:form id="clarifyCaseForm" method="post" action="#" enctype="multipart/form-data">
					<fieldset id="ticketCreator">
						<legend>Contact Windstream</legend>
						<%--== ACCOUNT NUMBER Section ==--%>
						<div
							id="acctNumSection"
							class="section">
							<input
								type="text"
								id="caseDto.siteId"
								name="caseDto.siteId"
								value=""></input>
							<input
								type="button"
								id="acctNumSubmit"
								value="Validate Account"
								class="submitButton"></input>
							<div id="acctNumStatus"></div>
						</div>
						<%--== CONTACT Section ==--%>
						<div id="contactSection" class="section">
							<label
								 id="contactLabel" 
								 for="caseDto.contactInfo">Select Contact
							</label>
							<select
								 id="caseDto.contactInfo"
								 name="caseDto.contactInfo"
								 disabled="disabled">
								<option value="">Select a contact</option>
							</select>
							<span id="contactsStatus"></span>
						</div>
						<div id="requestSection" class="section">
							<%--== PROBLEM Section ==--%>
							<div id="lvl1">
								<label id="requestLabel" for="caseDto.requestType">How can we help?</label>
								<select 
									id="caseDto.requestType"
									name="caseDto.requestType" 
									disabled="disabled">
									<option value="">Choose a request type</option>
								</select>
								<span id="lvl1Status"></span>
							</div>
							<div id="acctManagerSection">
								<div
									id="managerSection"
									class="floatL">
									<span class="floatL">
										<label
											id="acctManagerLabel"
											for="caseDto.manager">
											Are you a Windstream Account Manager?
										</label>
									</span>
									<span class="floatL">
										<input
											type="checkbox"
											id="caseDto.manager"
											name="caseDto.manager"/>
									</span>
								</div>
								<div
									id="acctManagerDetailsSection"
									class="floatR">
									<label
										id="acctManagerNameLabel"
										for="caseDto.managerName">
										Account Manager Name:
									</label>
									<input
										type="text"
										id="caseDto.managerName"
										name="caseDto.managerName" />
									<label
										id="acctManagerEmail"
										for="caseDto.managerEmail">
										Account Manager Email:
									</label>
									<input
										type="text"
										id="caseDto.managerEmail"
										name="caseDto.managerEmail" />
									<label
										id="acctManagerAttachLabel"
										for="caseDto.acctMngrAttachment">
										Attachment:
									</label>
									<input
										type="file"
										id="caseDto.acctMngrAttachment"
										name="caseDto.acctMngrAttachment" />
								</div>
							</div>
							<%--== SUBTYPE Section ==--%>
							<div id="lvl2">
								<label id="subTypeLabel" for="caseDto.requestSubType">Request SubType:</label>
								<select
									id="caseDto.requestSubType"
									name="caseDto.requestSubType" 
									disabled="disabled">
									<option value="">Choose a subtype</option>
								</select>
								<span id="lvl2Status"></span>
							</div>
							<%--== SUBTYPE CATEGORY Section ==--%>
							<div id="lvl3">
								<label id="subTypeCatLabel" for="caseDto.requestSubTypeCategory">SubType Category:</label>
								<select
									id="caseDto.requestSubTypeCategory"
									name="caseDto.requestSubTypeCategory"
									disabled="disabled">
									<option value="">Choose a category</option>
								</select>
								<span id="lvl3Status"></span>
							</div>
						</div>
						<%--== PRIORITY Section ==--%>
						<div
							id="prioritySection"
							class="section">
							<select
								id="caseDto.priority"
								name="caseDto.priority"
								disabled="disabled">
								<option value="">Select a priority</option>
								<option value="Low">Low</option>
								<option value="Medium">Medium</option>
								<option value="High">High</option>
							</select>
						</div>
						<%--== DESCRIPTION Section ==--%>
						<%--TODO: I need to encode this field to prevent XSS attacks --%>
						<div id="descriptionSection" class="section">
							<label id="descLabel" for="descField">Please provide details about your request:</label>
							<textarea
								id="caseDto.phoneLog"
								name="caseDto.phoneLog"
								rows="6"
								cols="80"
								disabled="disabled"></textarea>
						</div>
						<%--== SUBMIT BUTTON Section --%>
						<div id="buttonSection" class="section">
							<input
								type="button"
								id="ticketSubmit"
								value="Submit Ticket"
								disabled="disabled"
								class="submitButton"></input>
						</div>
					</fieldset>
				</s:form>
			</div>
		</div>
	</body>
</html>

