/**
 * Author: Ben Carson
 * Date Created: 03.04.2011
 * Notes: Javascript source file for the Contact Us page
 * Written with jQuery
 */

//actions to perform once the page is loaded and the DOM is registered.
$(document).ready( function() {
	//overlay the Windstream fonts
	Cufon.replace("#introDiv");
	Cufon.now();

	//jQuery Form plugin stuff
	$("#clarifyCaseForm").ajaxForm(validateAndSubmit);
	//------------------------
//	$("#clarifyCaseForm").submit(validateAndSubmit);
	$("#acctNumSubmit").click(validateAcctNum);
	$("#contactSection select[id*=contact]").change(refreshLvl1Dropdown);
	$("#lvl1>select")
		.change(refreshLvl2Dropdown)
		.change(refreshAcctManagerArea);
	$("#acctManagerSection").hide();
	$("#acctManagerDetailsSection").hide();
	$("#acctManagerSection input[id*=manager]").click(refreshAcctManagerArea);
	$("#lvl2>select").change(refreshLvl3Dropdown);
	$("#lvl3>select").change(refreshPriority);
	$("#prioritySection>select").change(refreshDescription);
	$("#descField").keyup(refreshSubmit);
	$("#ticketSubmit").click(validateAndSubmit);
});

function validateAcctNum() {
	//I can't explicitly call #caseDto.siteId because jQuery interprets this
	//as "find all elements with an id of 'caseDto' that have a class 'siteId'"
	var siteIdData = $("#acctNumSection>input[id*=siteId]");
	var subButton = $("#acctNumSubmit");
	var acctNumStatus = $("#acctNumStatus");

	$.ajax({
		type: "post",
		url: "acctNumAction.action",
		data: siteIdData.serialize(),
		dataType: "text",
		beforeSend: function() {
			subButton
				.prop("disabled",true)
				.hide();
			acctNumStatus
				.text('')
				.removeClass('colorMessage badMessage goodMessage')
				.addClass('loading');
		},
		success: function(returnedText,status, xhr) {
			//determine if there was a server-side error or not
			if(returnedText.indexOf('Error') > -1) {
				acctNumStatus
					.removeClass('loading')
					.text(returnedText)
					.addClass('colorMessage badMessage');
				subButton
					.prop("disabled",false)
					.show();
			} else {
				//no error, so put EAN in the input field
				siteIdData
					.prop("value",returnedText)
					.prop("disabled",true);
				//remove 'badMessage' class, just in case this is a retry of the account code
				acctNumStatus
					.removeClass('loading badMessage')
					.addClass('colorMessage goodMessage')
					.text('Valid Account!');
			}
		},
		error: function(xhr, status, error) {
			acctNumStatus
				.hide()
				.removeClass('loading')
				.text("An error occurred: " + error)
				.addClass('colorMessage badMessage')
				.fadeIn();
			subButton
				.prop("disabled",false)
				.show();
		},
		complete: function() {
			refreshContacts();		//trigger the refresh cascade...
		}
	});
}

function refreshContacts() {
	var siteIdField = $("#acctNumSection input[id*=siteId]");
	var siteIdId = siteIdField.attr('id');
	var siteIdValue = siteIdField.val();
	var contactData = $("#contactSection select[id*=contact]");
	//if account was successfully retrieved.
	if(siteIdField.is(':disabled') && $("#acctNumStatus").hasClass('goodMessage')) {
		$("#contactsStatus").addClass("loading");
		$.getJSON(
			'contactAction.action',
			{"caseDto.siteId" : siteIdValue},
			function(json) {
				$.each(json, function(index,jsonValue) {
					var option = $('<option></option>');
					option.prop('value',JSON.stringify(json[index],null,0))
						.html(jsonValue.fName + " " + jsonValue.lName)
						.appendTo(contactData);
				});
				$("#contactsStatus").removeClass("loading");
				contactData.prop("disabled",false);
			}
		)
		.error( function () {
			$("#contactsStatus")
				.removeClass("loading")
				.addClass('colorMessage badMessage')
				.load("./includes/error.jsp", function(text,status,xhr) {
					//after the error page loads, check if there is any data there
					//if not, give the user a generic error
					var t = text;
					var s = status;
					var x = xhr;
					var cleanString = $.trim($("#contactsStatus").text()); 
					if(cleanString.length <= 0) {
						$("#contactsStatus").text("There was an error retrieving contacts for account "+siteIdValue);
					}
				});
		});
	} else {
		contactData
			.prop("disabled",true)
			.emptySelect()
			.html("<option value=''>Select a contact</option>");
	}
	refreshLvl1Dropdown();		//trigger the cascade refresh...
}

function refreshLvl1Dropdown() {
	var contactField = $("#contactSection select[id*=contact]");
	var lvl1Field = $("#lvl1 select[id*=request]");
	var lvl1Status = $("#lvl1Status");
	lvl1Status.addClass("loading");
	lvl1Field
		.prop("disabled",true)
		.emptySelect()
		.html("<option value=''>Choose a request type</option>");
	if(!contactField.is(":disabled") &&
			contactField.val() != null && contactField.val() != '') {
		$.getJSON(
			'level1Action.action',
			function(jsonArray) {
				$.each(jsonArray, function(index, jsonObj) {
					var option = $('<option></option>');
					option
						.prop('value',jsonObj.reqCode)
						.html(jsonObj.reqDesc)
						.appendTo(lvl1Field);
				});
			}
		)
		.complete( function() {
			lvl1Status.removeClass('loading');
		})
		.error(function () {
			lvl1Field.prop("disabled",true);
			lvl1Status
				.text("There was an error loading Request dropdown.")
				.addClass('badMessage');
		})
		.success( function() {
			//activate the element
			lvl1Field.prop("disabled",false);
		});
	} else {
		lvl1Status.removeClass('loading');
		lvl1Field
			.prop("disabled",true)
			.emptySelect()
			.html("<option value=''>Choose a request type</option>");
		
	}
	refreshLvl2Dropdown();		//trigger the cascade refresh...
}

function refreshAcctManagerArea() {
    var lvl1Field = $("#lvl1>select");
    var acctMngrSection = $("#acctManagerSection");
    if (lvl1Field.val() == 'MACD') {
        acctMngrSection.show();
    } else {
        acctMngrSection.hide();
    }
    refreshAcctManagerDetailsArea(); //cascade refresh

}
function refreshAcctManagerDetailsArea() {
    var isAcctMngrField = $("#acctManagerSection input[id*=manager]");
    var detailsSection = $("#acctManagerDetailsSection");
    if (isAcctMngrField.is(":checked")) {
        detailsSection.show();
    } else {
        detailsSection.hide();
    }
}

function refreshLvl2Dropdown() {
	var lvl1Field = $("#lvl1>select");
	var lvl1Status = $("#lvl1Status");
	var lvl2Field = $("#lvl2>select");
	var lvl2Status = $("#lvl2Status");
	lvl2Field.prop("disabled",true); //for some reason, can't chain this with the rest
	lvl2Field
		.emptySelect()
		.html("<option value=''>Choose a subtype</option>");
	lvl2Status.addClass("loading");

	if(lvl1Field.val() == 'Tech') {
		//display the non-dynamic entries.
		lvl1Status
			.load('./includes/techSupport.html')
			.addClass('staticMessage');
		lvl2Status.removeClass("loading");
/*		lvl2Field
			.prop("disabled",true)
			.emptySelect()
			.html("<option value=''>Choose a subtype</option>");*/
	} else if (lvl1Field.val() == 'NonCrit') {
		lvl1Status
			.load('./includes/nonCritical.html')
			.addClass('staticMessage');
		lvl2Status.removeClass("loading");
/*		lvl2Field
			.prop("disabled",true)
			.emptySelect()
			.html("<option value=''>Choose a subtype</option>");*/
	} else {
		lvl1Status
			.removeClass('staticMessage')
			.text(''); //clear out the status if not needed.
		if(!lvl1Field.is(":disabled") &&
				(lvl1Field.val() == 'MACD' || lvl1Field.val() == 'Billing')) {
			$.getJSON(
				'level2Action.action',
				{"caseDto.requestType" : lvl1Field.val()},
				function(jsonArray) {
					$.each(jsonArray, function(index, lvl2OptionStr) {
						var option = $('<option></option>');
						option
							.prop('value',lvl2OptionStr)
							.html(lvl2OptionStr)
							.appendTo(lvl2Field);
					});
				}
			)
			.error( function() {
				lvl2Field
					.prop("disabled",true);
				lvl2Status
					.text("There was an error loading Request SubType dropdown.")
					.addClass('badMessage');
			})
			.success( function() {
				lvl2Field.prop("disabled",false);
			})
			.complete( function() {
				lvl2Status.removeClass('loading');
			});
		} else {
			lvl2Status.removeClass("loading");
			lvl2Field
				.prop("disabled",true)
				.emptySelect()
				.html("<option value=''>Choose a subtype</option>");
		}
	}
	refreshLvl3Dropdown();		//trigger the cascade refresh...
}
function refreshLvl3Dropdown() {
	var lvl1Value = $("#lvl1>select").val();
	var lvl2Field = $("#lvl2>select");
	var lvl3Field = $("#lvl3>select");
	var lvl3Status = $("#lvl3Status");
	lvl3Status.addClass("loading");
	lvl3Field
		.prop("disabled",true)
		.emptySelect()
		.html("<option value=''>Choose a category</option>");
	if(!lvl2Field.is(":disabled") &&
			lvl1Value != null && lvl1Value != '' &&
			lvl2Field.val() != null && lvl2Field.val() != '') {
		$.getJSON(
			'level3Action.action',
			{"caseDto.requestType" : lvl1Value,"caseDto.requestSubType" : lvl2Field.val()},
			function(jsonArray) {
				//sometimes there are no options for level 3
				if(jsonArray != null && !jQuery.isEmptyObject(jsonArray)) {
					$.each(jsonArray, function(index, lvl3OptionStr) {
						var option = $('<option></option>');
						option
							.prop('value',lvl3OptionStr)
							.html(lvl3OptionStr)
							.appendTo(lvl3Field);
					});
				} else {
					lvl3Field
						.emptySelect()
						.html("<option value='noOpt'>No categories available</option>");
				}
			}
		)
		.error( function() {
			lvl3Field.prop("disabled",true);
			lvl3Status
				.load('error.jsp')
				.addClass('badMessage');
		})
		.success( function() {
			lvl3Field.prop("disabled",false);
		})
		.complete( function() {
			lvl3Status.removeClass('loading');
			refreshPriority(); //trigger the cascade refresh...
		});
	} else {
		lvl3Status.removeClass('loading');
		lvl3Field
			.prop("disabled",true)
			.emptySelect()
			.html("<option value=''>Choose a category</option>");
		refreshPriority(); //trigger the cascade refresh...
	}
}
function refreshPriority() {
	var lvl3Value = $("#lvl3>select").val();
	var priorityField = $("#prioritySection select[id*=priority]");
	if(lvl3Value != null && lvl3Value != '') {
		priorityField.prop("disabled",false);
	} else {
		priorityField.prop("disabled",true);
	}
	refreshDescription();	//trigger the cascade refresh...
}
function refreshDescription() {
	var priorityField = $("#prioritySection select[id*=priority]");
	var priorityValue = priorityField.val();
	var descField = $("#descriptionSection textarea[id*=phoneLog]");
	if(!priorityField.is(":disabled") && 
			priorityValue != null && priorityValue != '') {
		descField.keypress( function() {
			if(descField.val()!= null && descField.val() != '') {
				refreshSubmit();
			}
		});
		descField.prop("disabled",false);
	} else {
		descField.val("");
		descField.prop("disabled",true);
	}
	refreshSubmit();		//trigger the cascade refresh...
}
function refreshSubmit() {
	var descValue = $("#descriptionSection textarea[id*=phoneLog]").val();
	var subButton = $("#ticketSubmit");
	if(descValue != null && descValue != '') {
		subButton.prop("disabled",false);
	} else {
		subButton.prop("disabled",true);
	}
}
function validateAndSubmit() {
	//prevent double-submits
	$("#ticketSubmit").prop('disabled',true);
	$("#acctNumSubmit").prop('disabled',true);
	
	
	var acctNumButton = $("#acctNumSubmit");
	//if account number button is visible, then the form submit needs to validate just the account Number
	if(acctNumButton.is(':visible')) {
		acctNumButton.click();
	} else if(validateForm() && 
			!$("#contactSection select[id*=contact]").prop('disabled')) {
		//If form validates and contact dropdown is enabled, submit the entire form 
		//(the above 'disabled' check works because I'm setting 'disabled' as T or F,
		//if the element's visibility were altered by setting "display: none", this wouldn't work
		
		var formDataString = buildSerialString("#clarifyCaseForm");
		
		$.ajax({
			type: "post",
			url: "createTicketAction.action",
			data: formDataString,
			dataType: "html",
			beforeSend: function() {
				$("#formStatus").hide();
				//Hide form
				//     stop() method explanation:
				//     bool arg1 - if false, queued animation are not cleared
				//     bool arg2 - if true, CSS props should be set to final state regardless of animation completion
				$("#clarifyCaseFormDiv")
					.stop(false,true)
					.slideUp()
					.delay(500);
			},
			success: function(returnedHtml) {
				//determine if there was a server-side error or not
				if(returnedHtml.indexOf('problem') > -1) {
					$("#formStatus")
						.stop(false,true)
						.delay(1000)
						.removeClass('loading')
						.fadeOut(1000)
						.addClass('colorMessage badMessage')
						.html(returnedHtml)
						.fadeIn(1000);
					//Redisplay form
					$("#clarifyCaseFormDiv")
						.stop(false, true)
						.slideDown();
					//reactivate the submit button
					$("#ticketSubmit").prop('disabled',false);
				} else {
					//no error
					//remove 'badMessage' class, just in case this is a retry
					$("#formStatus")
						.stop(false,true)
						.delay(1000)
						.fadeOut(1000)
						.removeClass('loading badMessage')
						.addClass('colorMessage caseSuccessMessage')
						.html(returnedHtml)
						.fadeIn(1000);
				}
			},
			error: function(xhr, status, error) {
				$("#formStatus")
					.stop()
					.fadeOut(1000)
					.addClass('colorMessage badMessage')
					.html(error)
					.fadeIn(1000);
				//Redisplay form
				$("#clarifyCaseFormDiv")
					.stop(false,true)
					.slideDown();
				//reactivate the submit button
				$("#ticketSubmit").prop('disabled',false);
			}
		});
	}
		
	//returning false so that the actual submit doesn't go through. jQuery takes care of the submit.
	return false;
}
function validateForm() {
	var acctNumButton = $("#acctNumSubmit");
	var theForm = $("form");
	//otherwise, validate the entire form
	//TODO: add HTML encoder to prevent XSS attacks.
	if(true) {
		return true;
	} else {
		return false;
	}
}

function buildSerialString() {
	var serializedString;
	
	//manually serialize the account number because .serialize won't work.
	//since the field is disabled, it is not a HTML spec-compliant "successful" element 
	var siteIdField = $("#acctNumSection>input[id*=siteId]");
	var siteIdSerialized = siteIdField.attr('id')+"="+siteIdField.val();
	//manually serialize the contact info, as its a JSON object
	var contactSerialized = $("#contactSection select[id*=contact]").attr('id')+"="+
		$("#contactSection select[id*=contact]").val();
	var lvl1 = $("#lvl1>select").serialize();
	var lvl2 = $("#lvl2>select").serialize();
	var lvl3 = $("#lvl3>select");
	//blank out the category if there is nothing in the dropdown ('No categories available')
	if(lvl3.val() =='noOpt') {
		lvl3.val('');
	}
	var lvl3Serialized = lvl3.serialize();
	var priority = $("#prioritySection select[id*=priority]").serialize();
	var phoneLog = $("#descriptionSection textarea[id*=phoneLog]").serialize();
	serializedString = 
		siteIdSerialized+"&"+
		contactSerialized+"&"+
		lvl1+"&"+
		lvl2+"&"+
		lvl3Serialized+"&"+
		priority+"&"+
		phoneLog;

	var managerField = $("#acctManagerSection input[id$=manager]");
	if(managerField.is(':checked')) {
		//assuming that the values exist, as this is occurring AFTER validation is performed.
		var isAcctManagerSerialized = "caseDto.manager=true"; //hard coding, its gotta be true here and 'serialize' set it to 'on', not 'true'
		var acctManagerNameSerialized = $("#acctManagerSection input[id$=managerName]").serialize();
		var acctManagerEmailSerialized = $("#acctManagerSection input[id$=managerEmail]").serialize();
		var acctManagerAttachSerialized = $("#acctManagerSection input[id$=acctMngrAttachment]").serialize();
		serializedString +=	"&"+
			isAcctManagerSerialized+"&"+
			acctManagerNameSerialized+"&"+
			acctManagerEmailSerialized+"&"+
			acctManagerAttachSerialized;

	}
	return serializedString;
}

