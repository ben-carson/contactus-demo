/**
 * Taken from jQuery in Action, Ch. 8
 */

//clear out all option of a select box
(function($) {
	$.fn.emptySelect = function() {
		return this.each(function() {
			if (this.tagName=='SELECT') {
				this.options.length = 0;
			}
		});
	};
})(jQuery);
