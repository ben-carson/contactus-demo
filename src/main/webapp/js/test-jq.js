/**
 * 
 */

$(document).ready( function() {
	$("#acctNumSection").load('./test-acctNum.jsp', function() {
		//load the event handler after the page, otherwise jQuery doesn't know what this id is 
		$("#jqSubmit").click(testSubmit);
		$("#siteIdStatus").hide();
	});
	$("#contactSection select[id*=contact]").change(refreshLvl1Dropdown);
	$("#lvl1>select").change(refreshLvl2Dropdown);
	$("#lvl2>select").change(refreshLvl3Dropdown);
	$("#lvl3>select").change(refreshDescription);
	$("#descField").keyup(refreshSubmit);
	$("#ticketSubmit").click(validateAndSubmit);
});

function testSubmit() {
	//get the input field from form that contains the word 'siteId' in its id attribute
	var siteIdData = $("#jqTestForm input[id*=siteId]");
//	var siteIdId = siteIdData.attr('id');
//	var siteIdValue = siteIdData.val();
	var jqXHRObj = $.ajax({
		type: "post",
		url: "testAction.action",
		data: siteIdData.serialize(),
//		data: siteIdId+"="+siteIdValue,
		dataType: "html",
		beforeSend: function() {
			var subButton = $("#acctNumSection input[type=button]");
			subButton
				.attr("disabled",true)
				.hide();
			$("#siteIdStatus")
				.addClass('loading')
				.show();
		},
		success: function(html) {
			var siteStatus = $("#siteIdStatus");
			siteStatus.removeClass('loading');
			$("#acctNumSection").html(html);
			siteStatus.addClass('goodMessage');
			siteStatus.text('Valid Account!');
			refreshContacts();		//trigger the cascade refresh...
		},
		error: function(error) {
			$("#siteIdStatus").removeClass('loading');
			$("#siteIdStatus").text("An error occurred:" + error);
			$("#siteIdStatus").addClass('badMessage');
			$("#acctNumSection input").show(fast);
		}
	});
}

function refreshContacts() {
	var siteIdData = $("#acctNumSection input[id*=siteId]");
	var siteIdId = siteIdData.attr('id');
	var siteIdValue = siteIdData.val();
	var contactData = $("#contactSection select[id*=contact]");
	if(siteIdData.is(':disabled')) {  //account was successfully retrieved.
		$("#contactsStatus").addClass("loading");
		$("#contactsStatus").show("slow");
		$.getJSON(
			'testContactAction.action',
			{"caseDto.siteId" : siteIdValue},
			function(json) {
				$.each(json, function(index,jsonValue) {
					var option = $('<option></option>');
					option.attr('value',JSON.stringify(json[index],null,0))
						.html(jsonValue.fName + " " + jsonValue.lName)
						.appendTo(contactData);
				});
				refreshLvl1Dropdown();
			}
		)
		.error( function () {
			$("#contactStatus").removeClass("loading");
			$("#contactSection").html("<p class='badMessage'>There was an error retrieving contacts for account "+siteIdValue+"</p>");
		})
		.complete( function () {
			$("#contactStatus").removeClass("loading");
			if(contactData != null && contactData != '') {
				//activate the dropdown, if an error hasn't overwritten the selectbox
				contactData.attr("disabled",false);
			}
		});
	} else {
		contactData
			.attr("disabled",true)
			.emptySelect()
			.html("<option value=''>Select a contact</option>");
	}
	refreshLvl1Dropdown();		//trigger the cascade refresh...
}

function refreshLvl1Dropdown() {
	var contactValue = $("#jqTestForm select[id*=contact]").val();
	var lvl1Field = $("#lvl1 select[id*=request]");
	var lvl1Status = $("#lvl1Status");
	lvl1Status.addClass("loading");
	lvl1Status.show();
	if(contactValue != null && contactValue != '') {
		lvl1Field.attr("disabled",false);
		$.getJSON(
			'testLvl1Action.action',
			function(jsonArray) {
				var select = $('#lvl1 select');
				$.each(jsonArray, function(index, jsonObj) {
					var option = $('<option></option>');
					option.attr('value',jsonObj.reqCode)
						.html(jsonObj.reqDesc)
						.appendTo(select);
				});
			}
		).error(function () {
			lvl1Field.attr("disabled",true);
			lvl1Field.removeClass("loading", function() {
				lvl1Status.fadeOut("fast");
			});
		});
	} else {
		lvl1Field.attr("disabled",true);
		lvl1Field.emptySelect();
		
	}
	refreshLvl2Dropdown();		//trigger the cascade refresh...
}
function refreshLvl2Dropdown() {
	var lvl1Value = $("#lvl1>select").val();
	var lvl2Field = $("#lvl2>select");
	if(lvl1Value != null && lvl1Value != '') {
		alert("refreshLvl2Dropdown not yet implemented!");
		lvl2Field.attr("disabled",false);
	} else {
		lvl2Field.attr("disabled",true);
		lvl2Field.emptySelect();
	}
	refreshLvl3Dropdown();		//trigger the cascade refresh...
}
function refreshLvl3Dropdown() {
	var lvl2Value = $("#lvl2>select").val();
	var lvl3Field = $("#lvl3>select");
	if(lvl2Value != null && lvl2Value != '') {
		alert("refreshLvl3Dropdown not yet implemented!");
		lvl2Field.attr("disabled",false);
	} else {
		lvl3Field.attr("disabled",true);
		lvl3Field.emptySelect();
	}
	refreshDescription();		//trigger the cascade refresh...
}
function refreshDescription() {
	var lvl3Value = $("#lvl3>select").val();
	var descField = $("#descField");
	if(lvl3Value != null && lvl3Value != '') {
		alert("refreshDescription not yet implemented!");
		lvl3Field.attr("disabled",false);
	} else {
		descField.attr("disabled",true);
	}
	refreshSubmit();		//trigger the cascade refresh...
}
function refreshSubmit() {
	var descValue = $("#descField").val();
	var subButton = $("#ticketSubmit");
	if(descValue != null && descValue != '') {
		alert("refreshSubmit not yet implemented!");
		subButton.attr("disabled",false);
	} else {
		subButton.attr("disabled",true);
	}
}
function validateAndSubmit() {
	alert("validateAndSubmit not yet implemented!");
}

function validateAcctNum() {
	var jqXHRObj = $.ajax({
		type: "get",
		url: "acctNumAction.action",
		//data: "caseDto_siteId="+$('input[id*="caseDto"]').val(),
		dataType: "html",
		beforeSend: function() {
			$("#siteIdStatus").toggleClass('loading');
		},
		success: function(html) {
			$("#siteIdStatus").toggleClass('loading');
			$("#contactSection").html(html);
		},
		error: function(error) {
			$("#siteIdStatus").toggleClass('loading');
			$("#acctNumSection").html("An error occurred:" + error);
		}
	});
	return false;
}