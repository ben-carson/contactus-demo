<%@ taglib prefix="s" uri="/struts-tags" %>
<s:if test="%{caseDto.siteId!=null && caseDto.siteId!=0 && caseDto.siteId!=''}">
	<s:property value="displayDto.contacts" escapeJavaScript="false" escape="false" default="[{'':'No Contacts Available'}]"/>
</s:if>
<s:else>
	[{"":"There are no contacts to list"}]
</s:else>