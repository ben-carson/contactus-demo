<%@taglib prefix="s" uri="/struts-tags"%>
<s:if test="%{caseDto.returnCode == 'Error'}">
	<p>
		There was a problem with creating your request:<br/>
		<s:property value="caseDto.returnCode"/>: 
		<span class="emphasizeText"><s:property value="caseDto.responseMessage"/></span>
	</p>
</s:if>
<s:elseif test="%{caseDto.returnCode == 'Success'}">
	<p>
		Your request was successfully submitted.<br />
		A case id of <span class="emphasizeText"><s:property value="caseDto.referenceValue"/></span> has been created.
	</p>
	<p>
		Please have this id available when contacting Windstream in the future about your issue.<br />
		Thank you!
	</p>
</s:elseif>
<s:else>
	<p>
		There was a problem with creating your request:<br/>
		The server returned an unrecognized status.
	</p>
</s:else>
