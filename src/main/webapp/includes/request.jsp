<%@taglib prefix="s" uri="/struts-tags"%>
<s:property
	value="displayDto.requestList"
	escapeJavaScript="false"
	escape="false"
	default="[{'':'No request types available'}]"/>
