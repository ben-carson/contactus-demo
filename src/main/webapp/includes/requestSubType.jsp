<%@taglib prefix="s" uri="/struts-tags" %>
<s:property
	value="displayDto.requestSubTypeList"
	escapeJavaScript="false"
	escape="false"
	default="[{'':'No request types available'}]"/>
