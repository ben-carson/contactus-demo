<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%--display errors and messages--%>
<s:if test="hasActionErrors()">
	<div id="appErrors"><s:actionerror/></div>
</s:if>
<s:if test="hasActionMessages()">
	<div id="appMessages"><s:actionmessage/></div>
</s:if>
