<%@taglib prefix="s" uri="/struts-tags" %>
<s:property
	value="displayDto.reqSubTypeCatList"
	escapeJavaScript="false"
	escape="false"
	default="[{'':'No subtype categories available'}]"/>
